package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "pow", description = "Returns the value of the first argument raised to the power of the second argument")
public class PowSupplier implements Supplier<Double> {

    private Supplier<Double> base;
    private Supplier<Double> exponent;

    public PowSupplier(@Desc("base") Supplier<Double> base, @Desc("exponent") Supplier<Double> exponent) {
        this.base = base;
        this.exponent = exponent;
    }

    @Override
    public Double get() {
        return Math.pow(base.get(), exponent.get());
    }
}
