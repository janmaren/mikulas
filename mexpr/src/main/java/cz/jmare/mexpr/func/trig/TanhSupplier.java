package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "tanh", description = "Returns the hyperbolic tangent of a double value.")
public class TanhSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public TanhSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.tanh(supplier.get());
    }
}
