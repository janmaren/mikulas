package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "avg", description = "Returns the average value of arguments.")
public class AvgSupplier implements Supplier<Double> {
    private Supplier<Double> arg1;

    private Supplier<Double>[] args;

    @SafeVarargs
    public AvgSupplier(Supplier<Double> arg1, Supplier<Double>... args) {
        this.arg1 = arg1;
        this.args = args;
    }

    @Override
    public Double get() {
        double sum = arg1.get();
        for (Supplier<Double> supplier : args) {
            sum += supplier.get();
        }
        return sum / (args.length + 1);
    }
}
