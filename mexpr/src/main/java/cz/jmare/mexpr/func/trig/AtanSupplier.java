package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "atan", description = "Returns the arc tangent of a value; the returned angle is in the range -pi/2 through pi/2.")
public class AtanSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public AtanSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.atan(supplier.get());
    }
}
