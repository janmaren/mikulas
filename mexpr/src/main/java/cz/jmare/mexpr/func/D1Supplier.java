package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "d1", description = "Factory for VectorD1 type. VectorD1 is an one dimensional array of doubles.")
public class D1Supplier implements Supplier<VectorD1> {
    private Supplier<Double> supplier;
    private Supplier<Double>[] suppliers;

    @SuppressWarnings("unchecked")
    public D1Supplier() {
        this.supplier = null;
        this.suppliers = new Supplier[0];
    }

    @SuppressWarnings("unchecked")
    public D1Supplier(Supplier<Double> supplier) {
        this.supplier = supplier;
        this.suppliers = new Supplier[0];
    }

    @SafeVarargs
    public D1Supplier(Supplier<Double> supplier, Supplier<Double>... suppliers) {
        this.supplier = supplier;
        this.suppliers = suppliers;
    }

    @Override
    public VectorD1 get() {
        if (supplier == null) {
            return new VectorD1Impl(new double[0]);
        }
        double[] doubles = new double[(suppliers == null ? 0 : suppliers.length) + 1];
        doubles[0] = supplier.get();
        for (int i = 0; i < suppliers.length; i++) {
            doubles[i + 1] = suppliers[i].get();
        }
        return new VectorD1Impl(doubles);
    }
}
