package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "log", description = "Returns the natural logarithm (base e) or logarithm with custom base of a double value.")
public class LogSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    private Supplier<Double> base;

    public LogSupplier(@Desc("value") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    public LogSupplier(@Desc("value") Supplier<Double> supplier, @Desc("base") Supplier<Double> base) {
        this.supplier = supplier;
        this.base = base;
    }

    @Override
    public Double get() {
        if (base != null) {
            return Math.log(supplier.get()) / Math.log(base.get());
        }
        return (double) Math.log(supplier.get());
    }
}
