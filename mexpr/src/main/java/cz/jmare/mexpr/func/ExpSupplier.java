package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "exp", description = "Returns Euler's number e raised to the power of value")
public class ExpSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    public ExpSupplier(@Desc("the exponent to raise e to") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.exp(supplier.get());
    }
}
