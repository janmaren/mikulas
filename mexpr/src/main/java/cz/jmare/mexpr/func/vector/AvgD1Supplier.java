package cz.jmare.mexpr.func.vector;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "avg", description = "Returns the average value of arguments.")
public class AvgD1Supplier implements Supplier<Double> {

    private Supplier<VectorD1> vectorD1;

    public AvgD1Supplier(Supplier<VectorD1> vectorD1) {
        this.vectorD1 = vectorD1;
    }

    @Override
    public Double get() {
        double sum = 0;
        VectorD1 vector = vectorD1.get();
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            sum += vector.get(i);
        }

        return sum / size;
    }
}
