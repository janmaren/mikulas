package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.DoubleHolder;

public class ParserArgTest {
    @Test
    public void parseTest() {
        ExpressionParser expressionParser = new ExpressionParser("sin(pi) + pyth(2, 2) + c",
                Map.of("pyth(a, b)", "sqrt(a * a + b * b)", "c", new DoubleHolder(5)));
        double evaluateDouble = expressionParser.evaluateDouble();
        assertEquals(7.828, evaluateDouble, 0.001);
    }
}
