package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "cosh", description = "Returns the hyperbolic cosine of a double value.")
public class CoshSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CoshSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.cosh(supplier.get());
    }
}
