package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sinh", description = "Returns the hyperbolic sine of a double value.")
public class SinhSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public SinhSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.sinh(supplier.get());
    }
}
