package cz.jmare.mexpr.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.DefsGetter;
import cz.jmare.mexpr.ExpressionParser;

/**
 * This represents one call of function, like f(x, y, ...). It passes argument suppliers into function and return result
 */
public class DefinedFuncBindSupplier implements Supplier<Object> {
    private Supplier<?> resultSupplier;

    public DefinedFuncBindSupplier(List<String> argumentNames, List<Supplier<?>> argumentSuppliers, DefinedFunction definedFunction, DefsGetter defsGetter, Config config) throws IllegalArgumentException {
        super();
        if (argumentNames.size() != argumentSuppliers.size()) {
            throw new IllegalArgumentException("Function " + definedFunction.getFunctionName() + " has arguments " + argumentNames + " but there is a call with different number of parameters: " + argumentSuppliers.size());
        }

        // protection against overcycling
        Map<String, Object> argSuppliers = new HashMap<>(defsGetter.getDefsMap());
        Iterator<Entry<String, Object>> iterator = argSuppliers.entrySet().iterator();
        while (iterator.hasNext()) {
        	Entry<String, Object> next = iterator.next();
        	String key = next.getKey();
        	if (definedFunction.hasSameSignature(key)) {
        		iterator.remove();
        	}
        }
        
        // arguments (overlaping global variables)
        for (int i = 0; i < argumentNames.size(); i++) {
            argSuppliers.put(argumentNames.get(i), argumentSuppliers.get(i));
        }

        ExpressionParser expressionParser = new ExpressionParser(definedFunction.getExpression(), argSuppliers, config);
        resultSupplier = expressionParser.getResultSupplier();
    }

    @Override
    public Object get() {
        return resultSupplier.get();
    }

    public Supplier<?> getResultSupplier() {
        return resultSupplier;
    }
}
