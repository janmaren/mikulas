package cz.jmare.mexpr.util.classutil;

import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Supplier;

import cz.jmare.mexpr.util.DefinedFuncBindSupplier;
import cz.jmare.mexpr.util.InstanceFuncBindSupplier;
import cz.jmare.mexpr.util.UnknownTypeSupplier;
import cz.jmare.mexpr.util.type.ObjectTypeInformer;

public class SupplierTypeUtil {
    public static Class<?> getFirstClassFromGeneric(ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (actualTypeArguments.length != 1) {
            throw new IllegalStateException("Not the right Supplier with just one generic type");
        }
        Type type0 = actualTypeArguments[0];
        if (type0 instanceof Class<?>) {
            return (Class<?>) type0;
        }
        return null;
    }

    public static Class<?> getGenericTypeOfSupplier(Supplier<?> supplier) {
    	if (supplier instanceof ObjectTypeInformer) {
    		ObjectTypeInformer objectTypeInformer = (ObjectTypeInformer) supplier;
    		return objectTypeInformer.getObjectClass();
    	} else if (supplier instanceof DefinedFuncBindSupplier) {
            DefinedFuncBindSupplier definedFuncBindSupplier = (DefinedFuncBindSupplier) supplier;
            supplier = definedFuncBindSupplier.getResultSupplier();
        } else if (supplier instanceof InstanceFuncBindSupplier) {
            InstanceFuncBindSupplier instanceFuncBindSupplier = (InstanceFuncBindSupplier) supplier;
            return instanceFuncBindSupplier.getReturnClass();
        }
        Class<?> supplierClass = supplier.getClass();
        return getGenericTypeOfSupplierClass(supplierClass);
    }

    public static Class<?> getGenericTypeOfSupplierClass(Class<?> supplierClass) {
        Type[] genericInterfaces = supplierClass.getGenericInterfaces();
        if (genericInterfaces.length == 0) {
        	Class<?> superclass = supplierClass.getSuperclass();
        	if (superclass != null) {
        		return getGenericTypeOfSupplierClass(superclass);
        	}
            return Object.class;
        }
        for (Type type : genericInterfaces) {
            if (!(type instanceof ParameterizedType)) {
                if (supplierClass.getCanonicalName().contains("$Lambda$")) {
                    throw new RuntimeException("Supplier musn't be defined as a lambda function because couldn't determine result type");
                }
                throw new RuntimeException("Unable to determine generic argument T of Supplier<T>");
            }
            Type rawType = ((ParameterizedType)type).getRawType();
            Class<?> clazz = (Class<?>) rawType;
            if (Supplier.class.isAssignableFrom(clazz)) {
                return getFirstClassFromGeneric((ParameterizedType) genericInterfaces[0]);
            }
        }
        throw new RuntimeException("Not found supplier for " + supplierClass);
    }

    /**
     * From parameter like Supplier&lt;Double&gt; supplier get the generic type, like Double
     * @param supplierParameter
     * @return
     */
    public static Class<?> getParameterSupplierGenType(Parameter supplierParameter) {
        Type[] genArgs = ((java.lang.reflect.ParameterizedType)supplierParameter.getParameterizedType()).getActualTypeArguments();
        if (genArgs.length == 0) {
            return null;
        }
        if (genArgs[0] instanceof WildcardType) {
            WildcardType wildcardType = (WildcardType) genArgs[0];
            return (Class<?>) wildcardType.getUpperBounds()[0];
        }
        return (Class<?>) genArgs[0];
    }

    public static boolean allSuppliersOfType(Supplier<?> supplier, List<Supplier<?>> suppliers, Class<?> clazz) {
        if (!clazz.isAssignableFrom(getGenericTypeOfSupplier(supplier))) {
            return false;
        }
        return allSuppliersOfType(suppliers, clazz);
    }

    public static boolean allSuppliersOfType(List<Supplier<?>> suppliers, Class<?> clazz) {
        for (Supplier<?> supplier : suppliers) {
            Class<?> genericTypeOfSupplier = getGenericTypeOfSupplier(supplier);
            if (!clazz.isAssignableFrom(genericTypeOfSupplier)) {
                return false;
            }
        }
        return true;
    }

    public static boolean anySupplierOfType(List<Supplier<?>> suppliers, Class<?> clazz) {
        for (Supplier<?> supplier : suppliers) {
            Class<?> genericTypeOfSupplier = getGenericTypeOfSupplier(supplier);
            if (clazz.isAssignableFrom(genericTypeOfSupplier)) {
                return true;
            }
        }
        return false;
    }

    public static boolean existsUnknownTypeSupplier(Supplier<?> supplier, List<Supplier<?>> suppliers) {
        return existsUnknownTypeSupplier(List.of(supplier)) || existsUnknownTypeSupplier(suppliers);
    }

    public static boolean existsUnknownTypeSupplier(List<Supplier<?>> suppliers) {
        for (Supplier<?> supplier : suppliers) {
            if (supplier instanceof UnknownTypeSupplier) {
                return true;
            }
        }
        return false;
    }

    public static boolean isUnknownTypeSupplier(Supplier<?> supplier) {
        return existsUnknownTypeSupplier(List.of(supplier));
    }

    public static boolean supplierOfType(Supplier<?> supplier, Class<?> clazz) {
        Class<?> genericTypeOfSupplier = getGenericTypeOfSupplier(supplier);
        if (!clazz.isAssignableFrom(genericTypeOfSupplier)) {
            return false;
        }
        return true;
    }

    public static String txtSuppliers(List<Supplier<?>> suppliers) {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (Supplier<?> supplier : suppliers) {
            stringJoiner.add(getGenericTypeOfSupplier(supplier).getSimpleName());
        }
        return stringJoiner.toString();
    }
}
