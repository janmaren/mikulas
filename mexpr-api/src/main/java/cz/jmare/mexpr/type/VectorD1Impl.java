package cz.jmare.mexpr.type;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Type for 1D
 */
public class VectorD1Impl implements VectorD1 {
    private double[] values;

    public VectorD1Impl(double[] values) {
        this.values = values;
    }

    @Override
    public double get(int index) {
        return values[index];
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public String toString() {
        List<Double> values = new ArrayList<>();
        for (double d : this.values) {
            values.add(d);
        }
        return values.stream().map(d -> String.valueOf(d)).collect(Collectors.joining(", ", "d1(", ")"));
    }
}
