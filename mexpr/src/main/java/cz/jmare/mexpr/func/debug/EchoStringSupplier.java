package cz.jmare.mexpr.func.debug;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "echo", description = "Output string to stdout and return as value.")
public class EchoStringSupplier implements Supplier<String> {
    private Supplier<String> argSup;

    public EchoStringSupplier(Supplier<String> arg1) {
        this.argSup = arg1;
    }

    @Override
    public String get() {
        String str = argSup.get();
        System.out.println(str);
        return str;
    }
}
