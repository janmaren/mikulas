package cz.jmare.mexpr.util.classutil;

import java.util.List;

public class FunctionalSupplierClassInfo implements Comparable<FunctionalSupplierClassInfo> {
    public Class<?> supplierClass;

    public String functionName;

    public String functionDescription;

    public List<ConstructorInfo> constructorInfos;

    public String returnType;

    /**
     * True when such class is present in classpath (not in external jar file)
     */
    public boolean classpathClass;

    @Override
    public int compareTo(FunctionalSupplierClassInfo o) {
        return functionName.compareTo(o.functionName);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(functionName);
        sb.append(": ").append(returnType);
        sb.append(" - ").append(functionDescription).append(System.lineSeparator());
        sb.append(getSyntaxesString());
        return sb.toString();
    }

    public String getSyntaxesString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < constructorInfos.size(); i++) {
            ConstructorInfo constructorInfo = constructorInfos.get(i);
            sb.append("  ");
            sb.append(functionName).append(constructorInfo.toString());
            if (i < constructorInfos.size() - 1) {
                sb.append(System.lineSeparator());
            }
        }
        return sb.toString() + ": " + returnType;
    }
}
