package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "rand", description = "Random number 0<=n<1 when no parameter used. Or parameters from (incl.) and to (excl.) provided, then number will be from such interval.")
public class RandSupplier implements Supplier<Double> {
    private Supplier<Double> fromIncludingSupplier;
    private Supplier<Double> toExcludingSupplier;


    public RandSupplier() {
    }

    public RandSupplier(@Desc("fromIncl") Supplier<Double> fromIncludingSupplier, @Desc("toExcl") Supplier<Double> toExcludingSupplier) {
        this.fromIncludingSupplier = fromIncludingSupplier;
        this.toExcludingSupplier = toExcludingSupplier;
    }

    @Override
    public Double get() {
        if (fromIncludingSupplier == null) {
            return Math.random();
        } else {
            Double fromValue = fromIncludingSupplier.get();
            return fromValue + (Math.random() * (toExcludingSupplier.get() - fromIncludingSupplier.get()));
        }
    }
}
