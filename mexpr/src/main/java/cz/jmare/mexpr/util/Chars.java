package cz.jmare.mexpr.util;

import java.util.Objects;

public class Chars implements CharSequence {
    private final String wholeString;

    private final int offset;

    private final String string;

    public Chars(String wholeString) {
        this.wholeString = wholeString;
        this.offset = 0;
        this.string = wholeString;
    }

    public Chars(String wholeString, int offset, int length) {
        this.wholeString = wholeString;
        this.offset = offset;
        this.string = wholeString.substring(offset, offset + length);
    }

    public Chars(CharSequence wholeString, int offset, int length) {
        this.wholeString = new StringBuilder(wholeString).toString();
        this.offset = offset;
        this.string = this.wholeString.substring(offset, offset + length);
    }

    @Override
    public int length() {
        return string.length();
    }

    @Override
    public char charAt(int index) {
        return string.charAt(index);
    }

    @Override
    public Chars subSequence(int start, int end) {
        return new Chars(wholeString, offset + start, end - start);
    }

    @Override
    public String toString() {
        return string;
    }

    public String getWholeString() {
        return wholeString;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chars chars = (Chars) o;
        return Objects.equals(string, chars.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string);
    }
}
