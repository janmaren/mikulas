package mexpr;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.VectorD1;

public class FuncVectorTest {
    @Test
    public void vecTest() {
        ExpressionParser expressionParser = new ExpressionParser("d1(1, 2, 3, 4, 5)");
        VectorD1 vectorD1 = (VectorD1) expressionParser.getResultSupplier().get();
        assertEquals(1, vectorD1.get(0), 0.1);
        assertEquals(2, vectorD1.get(1), 0.1);
        assertEquals(3, vectorD1.get(2), 0.1);
        assertEquals(4, vectorD1.get(3), 0.1);
        assertEquals(5, vectorD1.get(4), 0.1);
    }

    @Test
    public void avgTest() {
        ExpressionParser expressionParser = new ExpressionParser("avg(d1(1, 2, 3, 4, 5))");
        double avg = expressionParser.evaluateDouble();
        assertEquals(3, avg, 0.1);
    }

    @Test
    public void avg2Test() {
        ExpressionParser expressionParser = new ExpressionParser("avg(1, 2, 3, 4, 5)");
        double avg = expressionParser.evaluateDouble();
        assertEquals(3, avg, 0.1);
    }

    @Test
    public void foreachTest() {
        ExpressionParser expressionParser = new ExpressionParser("foreach(x * 10, x, d1(1, 2, 3, 4, 5))");
        VectorD1 vectorD1 = (VectorD1) expressionParser.getResultSupplier().get();
        assertEquals(10, vectorD1.get(0), 0.1);
        assertEquals(20, vectorD1.get(1), 0.1);
        assertEquals(30, vectorD1.get(2), 0.1);
        assertEquals(40, vectorD1.get(3), 0.1);
        assertEquals(50, vectorD1.get(4), 0.1);
    }

    @Test
    public void foreachAggD2Test() {
        ExpressionParser expressionParser = new ExpressionParser("foreachAgg(avg(x), x, d2(d1(1, 2), d1(3, 4)))");
        VectorD1 vectorD1 = (VectorD1) expressionParser.getResultSupplier().get();
        assertEquals(1.5, vectorD1.get(0), 0.1);
        assertEquals(3.5, vectorD1.get(1), 0.1);
    }
}
