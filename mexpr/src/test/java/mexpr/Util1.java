package mexpr;

public class Util1 {
    public static Double twice(Double val) {
        return val * 2;
    }

    public static Double twice2(Double... val) {
        double sum = 0;
        for (Double double1 : val) {
            sum += double1 * 2;
        }
        return sum;
    }

    public static Double twice3(Double val1, Double... val) {
        double sum = val1 * 2;
        for (Double double1 : val) {
            sum += double1 * 2;
        }
        return sum;
    }
}
