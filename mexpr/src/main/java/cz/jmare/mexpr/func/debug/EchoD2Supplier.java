package cz.jmare.mexpr.func.debug;

import java.util.StringJoiner;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "echo", description = "Output 2d vector to stdout and return as value.")
public class EchoD2Supplier implements Supplier<VectorD2> {
    private Supplier<VectorD2> argSup;

    public EchoD2Supplier(Supplier<VectorD2> arg1) {
        this.argSup = arg1;
    }

    @Override
    public VectorD2 get() {
        VectorD2 vec = argSup.get();
        int size = vec.size();
        StringJoiner stringJoiner = new StringJoiner(", ", "[", "]");
        for (int i = 0; i < size; i++) {
            StringJoiner stringJoiner2 = new StringJoiner(", ", "[", "]");
            VectorD1 vectorD1 = vec.get(i);
            int size2 = vectorD1.size();
            for (int j = 0; j < size2; j++) {
                double d = vectorD1.get(j);
                stringJoiner2.add(String.valueOf(d));
            }
            stringJoiner.add(stringJoiner2.toString());
        }

        System.out.println(stringJoiner);

        return vec;
    }
}
