package cz.jmare.mexpr.parse;

import java.lang.reflect.Method;

/**
 * Data to identify method to be called
 */
public class ObjectMethod {
    /**
     * Instance to be called or null for static call
     */
    public Object callObject;

    /**
     * Method to be called (member or static)
     */
    public Method callMethod;

    public ObjectMethod(Object callObject, Method callMethod) {
        super();
        this.callObject = callObject;
        this.callMethod = callMethod;
    }
}
