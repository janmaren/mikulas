package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.Spatial;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "distance", description = "Returns distance of two spatial points")
public class DistanceSpatialSupplier implements Supplier<Double> {
    private Supplier<Spatial> spatial1Supplier;

    private Supplier<Spatial> spatial2Supplier;

    public DistanceSpatialSupplier(@Desc("spatial coordinate 1") Supplier<Spatial> arg1, @Desc("spatial coordinate 2") Supplier<Spatial> arg2) {
        this.spatial1Supplier = arg1;
        this.spatial2Supplier = arg2;
    }

    @Override
    public Double get() {
        Spatial spatial1 = spatial1Supplier.get();
        Spatial spatial2 = spatial2Supplier.get();
        double x = spatial1.x - spatial2.x;
        double y = spatial1.y - spatial2.y;
        double z = spatial1.z - spatial2.z;
        return Math.sqrt(x * x + y * y + z * z);
    }
}
