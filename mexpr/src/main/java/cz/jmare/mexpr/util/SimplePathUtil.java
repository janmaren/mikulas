package cz.jmare.mexpr.util;

public class SimplePathUtil {

    /**
     * @param path absolute(also starting with file:) or relative path, example is src/main/resources/mapping.xml
     * @return example mapping.xml (no slash)
     */
    public static String getLastPartFromPath(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return path;
        }
        String fileNameWithWildCard = path.substring(slashIndex + 1);
        return fileNameWithWildCard;
    }

    public static int lastSepIndex(String path) {
        int lastIndexSlash = path.lastIndexOf("/");
        int lastIndexBackSlash = path.lastIndexOf("\\");
        int slashIndex = lastIndexSlash > lastIndexBackSlash ? lastIndexSlash: lastIndexBackSlash;
        return slashIndex;
    }

    public static String[] getNameAndSuffixOrEmpty(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            throw new IllegalArgumentException("String " + fullPathOrName + " is a directory");
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        int lastIndexOf = fullPathOrName.lastIndexOf(".");
        if (lastIndexOf <= 0) {
            return new String[] {fullPathOrName, ""};
        }
        return new String[] {fullPathOrName.substring(0, lastIndexOf), fullPathOrName.substring(lastIndexOf)};
    }

    public static boolean endsWithSep(String str) {
        char lastChar = str.charAt(str.length() - 1);
        return (lastChar == '/' || lastChar == '\\');
    }
}
