package cz.jmare.mexpr.func.calc;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.TrisectExtreme;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "localMins", description = "Calculate local minimums which are surrounding input values. The interval must be small enough - half of expected period, see also Nyquist theorem")
public class LocalMinsSupplier extends LocalExtremesBase {
    public LocalMinsSupplier(@Desc("expression") @UseInnerVariable  Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("From value") Supplier<Double> fromSupplier, @Desc("To value (excluding)") Supplier<Double> toSupplier, @Desc("Interval") Supplier<Double> stepSupplier) {
        super(supplier, variableSupplier, fromSupplier, toSupplier, stepSupplier, TrisectExtreme.LOWEST_COMP);
    }
}
