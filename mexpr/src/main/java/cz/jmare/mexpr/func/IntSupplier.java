package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "int", description = "Definite integral. Example: int(sin(x), x, 1, 10) or int(sin(x), x, 1, 10, 0.000001)")
public class IntSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;
    private DoubleHolder variableSupplier;
    private Supplier<Double> from;
    private Supplier<Double> to;
    private Supplier<Double> epsilonSupplier;

    public static final double EPSILON = 1.E-6;

    public IntSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("from") Supplier<Double> from, @Desc("to") Supplier<Double> to) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.from = from;
        this.to = to;
    }

    public IntSupplier(@Desc("expression") @UseInnerVariable  Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("from") Supplier<Double> from,
            @Desc("to") Supplier<Double> to, @Desc("epsilon") Supplier<Double> epsilonSupplier) {
        this(supplier, variableSupplier, from, to);
        this.epsilonSupplier = epsilonSupplier;
    }

    @Override
    public Double get() {
        double epsilon = epsilonSupplier != null ? epsilonSupplier.get() : EPSILON;

        Double fromValue = from.get();
        Double toValue = to.get();
        double x = fromValue;
        double sum = 0;
        boolean isNondefined = false;

        while (x < toValue) {
            variableSupplier.accept(x);
            Double double1 = supplier.get();
            if (double1 == null) {
                isNondefined = true;
                break;
            }
            sum += double1 * epsilon;
            x += epsilon;
        }

        if (isNondefined) {
            return Double.NaN;
        }
        return sum;
    }
}
