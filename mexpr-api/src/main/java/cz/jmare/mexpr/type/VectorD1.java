package cz.jmare.mexpr.type;

/**
 * Vector of scalar doubles.<br>
 * Example: [1, 3]
 */
public interface VectorD1 {
    double get(int index);

    int size();
}
