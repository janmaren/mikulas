package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "cot", description = "Returns the trigonometric cotangent of an angle.")
public class CotSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CotSupplier(@Desc("An angle in radians") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return 1.0 / Math.tan(supplier.get());
    }
}
