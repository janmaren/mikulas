package cz.jmare.mexpr.parse;

import cz.jmare.mexpr.exception.syntax.IllegalTokenException;
import cz.jmare.mexpr.exception.syntax.UnexpectedEndOfCodeException;
import cz.jmare.mexpr.util.Chars;
import cz.jmare.mexpr.util.CharsUtil;

/**
 * Return tokens.
 * Whitespaces are omitted (are separators)<br/>
 * Tokens returned as single characters '=', '(', ')', '{', '}', '/', '*', ';', ',' and also '+', '-' but only in case of nextSingle()<br/>
 * Double char tokens are '<=', '>=', ':='<br/>
 * Multiple char tokens (but they can contain only 1 character) are: numbers, also starting byt '+' or '-' (but nextSingle() musn't be used), variables starting by 'a-z' or 'A-Z'
 */
public class FToken implements Token {
    private final static String specialOneChar = "(){}[]+-/*;%^,";

    private final static String specialCompareDoubleChar = "<>=";

    private final static char commentChar = '#';

    private CharSequence str;

    private String token;

    private int[] tokenPos;

    private FToken nextToken;

    private boolean nextTokenPresent;

    private static final boolean SCIENTIFIC_E_LETTER_MUST_BE_FOLLOWED_BY_SIGN = false;

    private static final boolean DOUBLE_CHARS_SUPPORTED = false;

    private FToken() {
        super();
    }

    public FToken(CharSequence str) {
        super();
        this.str = str;
    }

    public FToken next() {
        if (nextTokenPresent) {
            return nextToken;
        }
        FToken fToken = nextInternal();
        if (fToken == null) {
            throw getUnexpectedEndOfCodeException();
        }
        return fToken;
    }

    @Override
    public Chars getPosition() {
        if (str instanceof Chars) {
            Chars chars = (Chars) str;
            return new Chars(chars.getWholeString(),
                    chars.getOffset() + tokenPos[0], tokenPos[1] - tokenPos[0]);
        }
        return null;
    }


    private FToken nextInternal() {
        int[] nextTokenDim = findToken(tokenPos == null ? 0 : tokenPos[1]);
        if (nextTokenDim == null) {
            return null;
        }
        FToken fToken = new FToken();
        fToken.tokenPos = nextTokenDim;
        fToken.token = CharsUtil.toString(str).substring(nextTokenDim[0], nextTokenDim[1]);
        fToken.str = str;

        nextToken = fToken;
        nextTokenPresent = true;
        return fToken;
    }

    public boolean hasNext() {
        return nextInternal() != null;
    }

    /**
     * Find token from position. It can be the fromPos position or greater
     * @param fromPos
     * @return int[2] where int[0] is start and int[1] is end
     */
    private int[] findToken(int fromPos) {
        for (int i = fromPos; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (!Character.isWhitespace(ch)) {
                if (ch == commentChar) {
                    i = findEndOfLine(str, i);
                    continue;
                }
                if (specialOneChar.indexOf(ch) != -1) {
                    return new int[] {i, i + 1};
                }
                if (DOUBLE_CHARS_SUPPORTED && specialCompareDoubleChar.indexOf(ch) != -1) {
                    if (i + 1 >= str.length()) {
                        throw getIllegalTokenException(i, 1);
                    }
                    if (str.charAt(i + 1) == '=') {
                        // :=, ==, <=, >=
                        return new int[] {i, i + 2};
                    } else {
                        // (, ), {, }, [, ], +, -, /, *, <, >
                        return new int[] {i, i + 1};
                    }
                }
                if (ch == '"') {
                    return parseText(i);
                }
                if (!((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9') || ch == '_' || ch == '+' || ch == '-')) {
                    throw getIllegalTokenException(i, 1);
                }
                if (ch >= '0' && ch <= '9') {
                    return parseNumber(i);
                } else {
                    int j = i + 1;
                    for (; j < str.length(); j++) {
                        char chNow = str.charAt(j);
                        if (Character.isWhitespace(chNow)) {
                            break;
                        }
                        if (specialOneChar.indexOf(chNow) != -1) {
                            break;
                        }
                        if (specialCompareDoubleChar.indexOf(chNow) != -1) {
                            break;
                        }
                        if (!((chNow >= 'A' && chNow <= 'Z') || (chNow >= 'a' && chNow <= 'z') || (chNow >= '0' && chNow <= '9') || chNow == '_')) {
                            throw new IllegalTokenException(CharsUtil.toString(str).substring(i, j + 1), i, 1 + j - i);
                        }
                    }
                    return new int[] {i, j};
                }
            }
        }

        return null;
    }

    private IllegalTokenException getIllegalTokenException(int offset, int length) {
        String string = CharsUtil.toString(str);
        IllegalTokenException e = new IllegalTokenException(string.substring(offset, offset + length), offset, length);
        if (str instanceof Chars) {
            Chars chars = (Chars) str;
            e.setPosition(new Chars(chars.getWholeString(), chars.getOffset() + offset, length));
        }
        return e;
    }

    private UnexpectedEndOfCodeException getUnexpectedEndOfCodeException() {
        int lastValidOffset = getOffset();
        int lastValidLength = getLength();
        UnexpectedEndOfCodeException e = new UnexpectedEndOfCodeException(str.length(), lastValidOffset, lastValidLength);
        if (str instanceof Chars) {
            Chars chars = (Chars) str;
            e.setPosition(new Chars(chars.getWholeString(), chars.getOffset() + str.length(), 0));
        }
        return e;
    }



    private int[] parseText(int firstDoubleQuoteCharacterIndex) {
        int indexOf = CharsUtil.indexOf(str, '"', firstDoubleQuoteCharacterIndex + 1);
        if (indexOf < 0) {
            String string = CharsUtil.toString(str);
            String rest = string.substring(firstDoubleQuoteCharacterIndex);
            throw new IllegalTokenException(rest, firstDoubleQuoteCharacterIndex, rest.length(), string);
        }
        return new int[] {firstDoubleQuoteCharacterIndex, indexOf + 1};
    }

    private int[] parseNumber(int firstNumberCharacterIndex) {
        boolean exponentState = false;
        boolean numberInExponent = false;
        int i = firstNumberCharacterIndex + 1;
        for (; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= '0' && ch <= '9') {
                if (exponentState) {
                    numberInExponent = true;
                }
                continue;
            } else if (ch == '.') {
                i++;
                if (i >= str.length()) {
                    throw getIllegalTokenException(firstNumberCharacterIndex, str.length() - firstNumberCharacterIndex);
                }
                ch = str.charAt(i);
                if (ch >= '0' && ch <= '9') {
                    continue;
                } else {
                    throw getIllegalTokenException(firstNumberCharacterIndex, i - firstNumberCharacterIndex);
                }
            } else if (ch == 'E' || ch == 'e') {
                if (exponentState) {
                    throw getIllegalTokenException(firstNumberCharacterIndex, i - firstNumberCharacterIndex);
                }
                i++;
                if (i >= str.length()) {
                    if (SCIENTIFIC_E_LETTER_MUST_BE_FOLLOWED_BY_SIGN) {
                        throw getIllegalTokenException(firstNumberCharacterIndex, str.length() - firstNumberCharacterIndex);
                    }
                    break;
                }
                ch = str.charAt(i);
                if (ch == '+' || ch == '-' || Character.isDigit(ch)) {
                    if (!Character.isDigit(ch)) {
                        i++;
                    }
                    if (i >= str.length()) {
                        throw getIllegalTokenException(firstNumberCharacterIndex, str.length() - firstNumberCharacterIndex);
                    }
                    ch = str.charAt(i);
                    if (ch >= '0' && ch <= '9') {
                        exponentState = true;
                        numberInExponent = true;
                        continue;
                    }
                    if (SCIENTIFIC_E_LETTER_MUST_BE_FOLLOWED_BY_SIGN) {
                        throw getIllegalTokenException(firstNumberCharacterIndex, i - firstNumberCharacterIndex);
                    } else {
                        i -= 2;
                        break;
                    }
                } else {
                    if (SCIENTIFIC_E_LETTER_MUST_BE_FOLLOWED_BY_SIGN) {
                        throw getIllegalTokenException(firstNumberCharacterIndex, i - firstNumberCharacterIndex);
                    } else {
                        break;
                    }
                }
            } else {
                break;
            }
        }
        if (exponentState && !numberInExponent) {
            throw getIllegalTokenException(firstNumberCharacterIndex, i - firstNumberCharacterIndex);
        }
        return new int[] {firstNumberCharacterIndex, i};
    }

    public String getString() {
        return token;
    }

    @Override
    public String toString() {
        return token;
    }

    public int getOffset() {
        return tokenPos[0];
    }

    public int getLength() {
        return tokenPos[1] - tokenPos[0];
    }

    public String getExpressionFromPos() {
        return CharsUtil.toString(str).substring(tokenPos[0]);
    }

    public static boolean isText(String string) {
        return string.startsWith("\"");
    }

    /**
     * Find end of line which is position last of last eol character. When no such exists then
     * return length of string
     */
    private static int findEndOfLine(CharSequence str, int fromPos) {
        for (int i = fromPos; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == '\n') {
                return i;
            }
            if (ch == '\r') {
                if (i + 1 < str.length()) {
                    char ch2 = str.charAt(i + 1);
                    if (ch2 == '\n') {
                        return i + 1;
                    } else {
                        return i;
                    }
                } else {
                    return i;
                }
            }
        }
        return str.length();
    }

    public static void main(String[] args) {
        //String str = "-2.235e+1";
        String str = "+3+24^3+1+xy2+\"aaa\"2.235e+0+b1+b5/d3 * [ [1, 3] #\n ]";
        FToken fToken = new FToken(str);
        while (fToken.hasNext()) {
            Token clone = fToken;
            fToken = fToken.next();

            System.out.println("Token:" + fToken.token);
        }
    }
}
