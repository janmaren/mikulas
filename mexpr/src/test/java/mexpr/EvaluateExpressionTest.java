package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.DoubleHolder;

public class EvaluateExpressionTest {

    @Test
    public void simpleByParserUnitTest() {
        ExpressionParser expressionParser = new ExpressionParser("1.5*x+x", Map.of("y", "35 + z", "x", "y^2", "z", "1.4"));
        assertEquals(3312.3999999999996d, expressionParser.evaluateDouble(), 0.0001);
    }

    @Test
    public void simplestShareXTest() {
        ExpressionParser expressionParser = new ExpressionParser("x + x", Map.of("x", "2"));
        assertEquals(4d, expressionParser.evaluateDouble(), 0.0001);
    }
}
