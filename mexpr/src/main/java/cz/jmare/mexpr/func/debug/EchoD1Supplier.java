package cz.jmare.mexpr.func.debug;

import java.util.StringJoiner;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "echo", description = "Output 1d vector to stdout and return as value.")
public class EchoD1Supplier implements Supplier<VectorD1> {
    private Supplier<VectorD1> argSup;

    public EchoD1Supplier(Supplier<VectorD1> arg1) {
        this.argSup = arg1;
    }

    @Override
    public VectorD1 get() {
        VectorD1 vec = argSup.get();
        StringJoiner stringJoiner = new StringJoiner(", ", "[", "]");
        int size = vec.size();
        for (int i = 0; i < size; i++) {
            stringJoiner.add(String.valueOf(vec.get(i)));
        }
        System.out.println(stringJoiner);
        return vec;
    }
}
