package cz.jmare.mexpr.func.calc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.exception.run.InvalidValueException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.TrisectExtreme;
import cz.jmare.mexpr.util.UseInnerVariable;

public abstract class LocalExtremesBase implements Supplier<VectorD1> {
    private Supplier<Double> supplier;
    private DoubleHolder variableSupplier;
    private Supplier<Double> fromSupplier;
    private Supplier<Double> toSupplier;
    private Supplier<Double> stepSupplier;
    private Comparator<Double> comp;

    public LocalExtremesBase(@Desc("expression") @UseInnerVariable  Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("From value") Supplier<Double> fromSupplier, @Desc("To value (excluding)") Supplier<Double> toSupplier, @Desc("Interval") Supplier<Double> stepSupplier, Comparator<Double> comp) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.fromSupplier = fromSupplier;
        this.toSupplier = toSupplier;
        this.stepSupplier = stepSupplier;
        this.comp = comp;
    }

    @Override
    public VectorD1 get() {
        Thread currentThread = Thread.currentThread();
        List<Double> values = new ArrayList<>();
        
        double from = fromSupplier.get();
        double to = toSupplier.get();
        if (from > to) {
        	double tmp = from;
        	from = to;
        	to = tmp;
        }
        double step = stepSupplier.get();
        double pos = from;
        to += step;
        
        double prevX = pos - step;
        variableSupplier.accept(prevX);
        double prevY = supplier.get();

        double currX = pos;
        variableSupplier.accept(currX);
        double currY = supplier.get();

        while (currX < to) {
        	pos += step;
            double nextX = pos;
            variableSupplier.accept(nextX);
            Double nextY = supplier.get();
            if (nextY == currY) {
                continue;
            }
            boolean passes = extremeBetweenPoints(prevY, currY, nextY);
            if (passes) {
                Double value = TrisectExtreme.extremeOrNull(variableSupplier, supplier, prevX, nextX, comp);
                if (value == null) {
                    throw new InvalidValueException("Unable to find exact value between " + prevX + " and " + nextX + ", is there a discontinuity?");
                }
                values.add(value);
            }
            prevX = currX;
            prevY = currY;
            currX = nextX;
            currY = nextY;
            
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("local extreme interrupted");
            }
        }

        double[] arr = values.stream().mapToDouble(Double::doubleValue).toArray();
        return new VectorD1Impl(arr);
    }

    private boolean extremeBetweenPoints(double prevY, double currY, double nextY) {
        return comp.compare(prevY, currY) > 0 && comp.compare(currY, nextY) < 0;
    }
}
