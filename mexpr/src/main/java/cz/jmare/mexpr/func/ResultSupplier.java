package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.Result;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "result", description = "Factory to build a result")
public class ResultSupplier implements Supplier<Result> {
    private Supplier<Double> xSupplier;

    private Supplier<Object> ySupplier;


    public ResultSupplier(Supplier<Double> xSupplier, Supplier<Object> ySupplier) {
		super();
		this.xSupplier = xSupplier;
		this.ySupplier = ySupplier;
	}

	@Override
    public Result get() {
        return new Result(xSupplier.get(), ySupplier.get());
    }
}
