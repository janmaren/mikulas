package cz.jmare.mexpr.exception;

/**
 * When syntactically parsable but something other is wrong, like missing function
 */
public class SemanticException extends ParseExpressionException {
    private static final long serialVersionUID = -3238966991546363196L;

    public SemanticException(String message) {
        super(message);
    }
}
