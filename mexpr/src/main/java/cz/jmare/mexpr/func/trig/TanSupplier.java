package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "tan", description = "Returns the trigonometric tangent of an angle.")
public class TanSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public TanSupplier(@Desc("An angle in radians") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.tan(supplier.get());
    }
}
