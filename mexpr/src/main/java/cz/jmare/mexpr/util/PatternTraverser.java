package cz.jmare.mexpr.util;

import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternTraverser {
    private Pattern pattern;

    private String subject;

    private int limit = -1;

    private int count;

    private Consumer<Matcher> consumer;

    public PatternTraverser(Pattern pattern, String subject, Consumer<Matcher> consumer) {
        super();
        this.pattern = pattern;
        this.subject = subject;
        this.consumer = consumer;
    }

    public PatternTraverser(String regex, String subject, Consumer<Matcher> consumer) {
        super();
        this.pattern = Pattern.compile(regex);
        this.subject = subject;
        this.consumer = consumer;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void traverse() {
        Matcher matcher = pattern.matcher(subject);
        int i;
        for (i = 0; (limit < 0 || i < limit) && matcher.find(); i++) {
            consumer.accept(matcher);
        }

        count = i;
    }

    public int getCount() {
        return count;
    }

    public static void traverse(String regex, String subject, Consumer<Matcher> consumer) {
        PatternTraverser patternTraverser = new PatternTraverser(regex, subject, consumer);
        patternTraverser.traverse();
    }
}
