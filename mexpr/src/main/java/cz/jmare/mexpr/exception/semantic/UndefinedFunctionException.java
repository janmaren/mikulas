package cz.jmare.mexpr.exception.semantic;

import java.util.List;
import java.util.stream.Collectors;

import cz.jmare.mexpr.exception.SemanticException;

public class UndefinedFunctionException extends SemanticException {
    private static final long serialVersionUID = 2692829249787167517L;

    private String functionName;

    public UndefinedFunctionException(String functionName) {
        super("Function " + functionName + " doesn't exist");
        this.functionName = functionName;
    }

    public UndefinedFunctionException(String functionName, List<String> argTypes) {
        super("Function " + functionName + argTypes.stream().collect(Collectors.joining(", ", "(", ")")) + " doesn't exist");
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }
}
