package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "round", description = "Returns the closest long to the argument, with ties rounding to positive infinity.")
public class RoundSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public RoundSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) Math.round(supplier.get());
    }
}
