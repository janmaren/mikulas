package cz.jmare.mexpr.util.classutil;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.DefsGetter;
import cz.jmare.mexpr.arit.MethodFunction;
import cz.jmare.mexpr.exception.InvalidDefException;
import cz.jmare.mexpr.parse.ObjectMethod;
import cz.jmare.mexpr.util.DefinedFuncBindSupplier;
import cz.jmare.mexpr.util.DefinedFunction;

public class FuncCreatorUtil {
    public static Supplier<?> doFindExpressedFunctionSupplier(String functionName, List<Supplier<?>> suppliers, DefsGetter defsGetter, Config config) {
        List<DefinedFunction> exprFuncs = defsGetter.getExpressedFunction(functionName);
        if (exprFuncs != null) {
            for (DefinedFunction definedFunction : exprFuncs) {
                try {
                    DefinedFuncBindSupplier functionSupplier = new DefinedFuncBindSupplier(definedFunction.getArgNames(),
                            suppliers, definedFunction, defsGetter, config);
                    return functionSupplier;
                } catch (IllegalArgumentException e) {
                    // no handling, try next
                }
            }
        }
        return null;
    }

    public static ObjectMethod findMethod(List<MethodFunction> functions, List<Supplier<?>> argumentSuppliers) {
        List<?> parsClasses = argumentSuppliers.stream().map(as -> SupplierTypeUtil.getGenericTypeOfSupplier(as)).collect(Collectors.toList());
        Class<?>[] parameters = parsClasses.toArray(new Class[parsClasses.size()]);
        ObjectMethod objectMethod = null;
        for (MethodFunction instanceFunction : functions) {
//            if (instanceFunction.getImplementation().getClass().getCanonicalName() != null && instanceFunction.getImplementation().getClass().getCanonicalName().contains("$Lambda$")) {
//                throw new InvalidDefException("Function " + instanceFunction + " musn't be defined as lambda");
//            }
            boolean statCall = false;
            Method method;
            if (instanceFunction.getImplementation() instanceof Class) {
                Class<?> class1 = (Class<?>) instanceFunction.getImplementation();
                method = findMethodOfClass(class1, instanceFunction.getFunctionName(), parameters);
                statCall = true;
            } else {
                method = findMethodOfClass(instanceFunction.getImplementation().getClass(), instanceFunction.getFunctionName(), parameters);
            }
            if (method != null) {
                if (objectMethod != null) {
                    throw new InvalidDefException("Function " + instanceFunction.getFunctionName() + " is ambiguous - multiple definitions exists, class " + objectMethod.callObject.getClass() + " and " + instanceFunction.getImplementation().getClass());
                }
                objectMethod = new ObjectMethod(statCall ? null : instanceFunction.getImplementation(), method);
            }
        }
        return objectMethod;
    }

    public static Method findMethodOfClass(Class<?> clazz, String methodName, Class<?>[] parameters) {
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (!method.getName().equals(methodName)) {
                continue;
            }
            Parameter[] methParameters = method.getParameters();
            if (methParameters.length > parameters.length) {
                continue;
            }
            boolean matches = true;
            for (int i = 0; i < methParameters.length; i++) {
                Parameter methParameter = methParameters[i];
                if (!ClassUtils.isAssignable(methParameter.getType(), parameters[i])) {
                    if (i == methParameters.length - 1) {
                        if (methParameters[i].isVarArgs()) {
                            Class<?> componentType = methParameters[i].getType().getComponentType();
                            boolean varargsMatch = true;
                            for (int j = i; j < parameters.length; j++) {
                                if (!ClassUtils.isAssignable(componentType, parameters[i])) {
                                    varargsMatch = false;
                                    break;
                                }
                            }
                            if (!varargsMatch) {
                                matches = false;
                                break;
                            }
                            if (!ClassUtils.isAssignable(componentType, parameters[i])) {
                                matches = false;
                                break;
                            }
                        } else {
                            matches = false;
                            break;
                        }
                    } else {
                        matches = false;
                        break;
                    }
                }
            }
            if (matches) {
                return method;
            }
        }
        return null;
    }

    public static List<String> getSuppliersSimpleTypes(List<Supplier<?>> argumentSuppliers) {
        return argumentSuppliers.stream().map(s -> {
            try {
                return SupplierTypeUtil.getGenericTypeOfSupplier(s).getSimpleName();
            } catch (Exception e) {
                return "UnknownType";
            }
        }).collect(Collectors.toList());
    }
}
