package cz.jmare.mexpr.arit;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.NotExpressionException;
import cz.jmare.mexpr.util.classutil.SupplierTypeUtil;

public class ExpressionFunction implements Function<Double, Double> {
    private Supplier<Double> rootSupplier;
    private DoubleHolder numberHolder;

    public ExpressionFunction(String expression) {
        this(expression, null, new Config());
    }

    public ExpressionFunction(String expression, DoubleHolder variableSupplier) {
        this(expression, variableSupplier, new Config());
    }

    @SuppressWarnings("unchecked")
    public ExpressionFunction(String expression, DoubleHolder numberHolder, Config config) {
        ExpressionParser expressionParser;
        try {
            expressionParser = new ExpressionParser(expression, config);
        } catch (UndefinedVariableException e) {
            String variableName = e.getVariableName();
            expressionParser = new ExpressionParser(expression, Map.of(variableName, numberHolder), config);
        }
        if (!SupplierTypeUtil.supplierOfType(expressionParser.getResultSupplier(), Double.class)) {
            throw new NotExpressionException("Expression " + expression + " is not a double expression");
        }
        rootSupplier = (Supplier<Double>) expressionParser.getResultSupplier();
        this.numberHolder = numberHolder;
    }

    @Override
    public Double apply(Double value) {
        numberHolder.accept(value);
        return rootSupplier.get();
    }
}
