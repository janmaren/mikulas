package cz.jmare.mexpr.arit;

import java.util.function.Supplier;

/**
 * negation -> ('+' | '-') base | base
 */
public class NegationSupplier implements Supplier<Double> {
    private Supplier<Double> number;

    public NegationSupplier(Supplier<Double> number) {
        this.number = number;
    }

    @Override
    public Double get() {
        return -number.get();
    }
}
