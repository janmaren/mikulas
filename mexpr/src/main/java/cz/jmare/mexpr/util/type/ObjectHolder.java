package cz.jmare.mexpr.util.type;

import java.util.function.Supplier;

/**
 * Wrapper for custom object types implementing Supplier because
 * arguments of functions need a Supplier
 * @param <T>
 */
public class ObjectHolder<T> implements Supplier<T>, ObjectTypeInformer {
	private T object;
	
	public ObjectHolder(T object) {
		super();
		this.object = object;
	}

	@Override
	public T get() {
		return object;
	}
	
	public void set(T object) {
		this.object = object;
	}
	
	@Override
	public Class<?> getObjectClass() {
		return object.getClass();
	}
}
