package cz.jmare.mexpr.util;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Store function like 'f(arg1, arg2): arg1 + arg2'
 * It returns argument names and corresponding variable suppliers for population.
 * By default it is not allowed to use another variable not present in argument list in expression but it's possible to allow it by withAllowUnknownVarFunc
 */
public class DefinedFunction {
    private CharSequence expression;
    private String functionName;
    private List<String> argNames;

    public DefinedFunction(String funcSignature, CharSequence funcBody) {
        FuncDefinitionParser funcDefinitionParser = new FuncDefinitionParser(funcSignature);
        init(funcDefinitionParser.getFunctionName(), funcDefinitionParser.getArguments(), funcBody);
    }

    private void init(String functionName, List<String> argNames, CharSequence expression) {
        this.functionName = functionName;
        this.argNames = argNames;
        this.expression = expression;
    }

    @Override
    public String toString() {
        return CharsUtil.toString(getExpression());
    }

    /**
     * Function name
     * @return
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Argument names present in f(arg1, arg2, ...) argument list
     * @return
     */
    public List<String> getArgNames() {
        return argNames;
    }

    public String getSignature() {
        return argNames.stream().collect(Collectors.joining(", ", functionName + "(", ")"));
    }
    
    public boolean hasSameSignature(String signatureOther) {
    	if (!signatureOther.contains("(")) {
    		return false;
    	}
    	FuncDefinitionParser funcDefinitionParser = new FuncDefinitionParser(signatureOther);
    	String signatureOtherNorm = funcDefinitionParser.getArguments().stream().collect(Collectors.joining(", ", funcDefinitionParser.getFunctionName() + "(", ")"));
    	String signatureThis = getSignature();
    	return signatureThis.equals(signatureOtherNorm);
    }

    public CharSequence getExpression() {
        return expression;
    }

    @Override
    public int hashCode() {
        return Objects.hash(argNames, expression, functionName);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DefinedFunction other = (DefinedFunction) obj;
        return Objects.equals(argNames, other.argNames) && Objects.equals(expression, other.expression)
                && Objects.equals(functionName, other.functionName);
    }
}
