package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.DoubleHolder;

public class FuncExprParserTest {
    @Test
    public void simplePassVarTest() {
        ExpressionParser expressionParser = new ExpressionParser("pow(2 ^ a, 3 * a)", Map.of("a", new DoubleHolder(2)));
        double result = expressionParser.evaluateDouble();
        assertEquals(4096, result, 0.1);
    }

}
