package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "diff", description = "Differentiate expression or function. Default epsilon is 1.E-6. Example: diff(sin(x) ^ 2, x) or diff(sin(x) ^ 2, x, 0.0001)")
public class DiffSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    private DoubleHolder variableSupplier;

    public static final double EPSILON  = 1.E-9;

    private Supplier<Double> epsilonSupplier;

    private Supplier<Double> x;

    public DiffSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("inPoint") Supplier<Double> x) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.x = x;
    }

    public DiffSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("inPoint") Supplier<Double> x, @Desc("epsilon") Supplier<Double> epsilonSupplier) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.x = x;
        this.epsilonSupplier = epsilonSupplier;
    }

    @Override
    public Double get() {
        double epsilon = epsilonSupplier != null ? epsilonSupplier.get() : EPSILON;

        variableSupplier.accept(x.get() + epsilon);
        double fyPlusEps = supplier.get();
        if (Double.isNaN(fyPlusEps)) {
            variableSupplier.accept(x.get());
            double fy = supplier.get();

            variableSupplier.accept(x.get() - epsilon);
            double fyMinusEps = supplier.get();

            return (fy - fyMinusEps) / epsilon;
        }

        variableSupplier.accept(x.get() - epsilon);
        double fyMinusEps = supplier.get();
        if (Double.isNaN(fyMinusEps)) {
            variableSupplier.accept(x.get());
            double fy = supplier.get();

            return (fyPlusEps - fy) / epsilon;
        }

        return (fyPlusEps - fyMinusEps) / (2 * epsilon);
    }
}
