package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import cz.jmare.mexpr.exception.syntax.IllegalTokenException;
import cz.jmare.mexpr.util.Chars;
import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.semantic.IncompatibleOperandException;
import cz.jmare.mexpr.exception.semantic.UndefinedFunctionException;
import cz.jmare.mexpr.exception.syntax.UnexpectedEndOfCodeException;
import cz.jmare.mexpr.type.DoubleHolder;

public class ErrorsTest {
    @Test
    public void noFuncTest() {
        try {
            new ExpressionParser("f(10) + f(a ^ 2)", Map.of("a", new DoubleHolder(2)));
        } catch (UndefinedFunctionException e) {
            return;
        }
        fail("Exception not occured");
    }

    @Test
    public void expTest() {
        try {
            new ExpressionParser("a ^ -", Map.of("a", new DoubleHolder(2)));
        } catch (IllegalTokenException e) {
            assertEquals(4, e.getOffset());
            assertEquals(1, e.getLength());
            return;
        }
        fail("Exception not occured");
    }

    @Test(expected = IncompatibleOperandException.class)
    public void incompatibleSummandsTest() {
        new ExpressionParser("10 * (123 + d1(1, 2, 3))");
    }

    @Test
    public void correctExcepMissingFunc() {
        try {
            new ExpressionParser("func1(123, d1(1, 2, 3))");
        } catch (UndefinedFunctionException e) {
            return;
        }
        fail();
    }

    @Test
    public void correctExcepMissingFunc2() {
        try {
            new ExpressionParser("sin(0.5, 0.4)");
        } catch (UndefinedFunctionException e) {
            assertTrue(e.getMessage().contains("sin(Double, Double)"));
        }
    }

    @Test
    public void illegalTokenTest() {
        try {
            new ExpressionParser(new Chars("12\n1@a + 5", 3, 7));
        } catch (IllegalTokenException e) {
            Chars position = e.getPosition();
            assertNotNull(position);
            assertEquals(4, position.getOffset());
            assertEquals(1, position.length());
            return;
        }
        fail("Exception not occured");
    }

    @Test
    public void parenthWrongTokenTest() {
        try {
            new ExpressionParser(new Chars("12\n(1 + 5)) +", 3, 10));
        } catch (IllegalTokenException e) {
            Chars position = e.getPosition();
            assertNotNull(position);
            assertEquals(10, position.getOffset());
            assertEquals(1, position.length());
            return;
        }
        fail("Exception not occured");
    }

    @Test
    public void eolTokenTest() {
        try {
            new ExpressionParser(new Chars("12\n(1 + 5 +", 3, 7));
        } catch (UnexpectedEndOfCodeException e) {
            Chars position = e.getPosition();
            assertNotNull(position);
            assertEquals(10, position.getOffset());
            assertEquals(0, position.length());
            return;
        }
        fail("Exception not occured");
    }
}
