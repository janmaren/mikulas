package cz.jmare.mexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Holder of 1D data
 */
public class VectorD1Holder implements Supplier<VectorD1>, Consumer<VectorD1> {
    private VectorD1 value;

    public VectorD1Holder(VectorD1 value) {
        this.value = value;
    }

    public VectorD1Holder() {
    }

    @Override
    public VectorD1 get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void accept(VectorD1 value) {
        this.value = value;
    }
}
