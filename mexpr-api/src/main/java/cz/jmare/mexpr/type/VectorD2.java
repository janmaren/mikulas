package cz.jmare.mexpr.type;

/**
 * Vector of vector of scalar doubles.<br>
 * Example: [[1, 3], [2, 4]]
 */
public interface VectorD2 {
    VectorD1 get(int index);

    int size();
}
