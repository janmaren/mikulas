package cz.jmare.mexpr.util.classutil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.Manifest;

public class LibrariesClassesDetector {
    public static Set<String> getClasses() {
        Set<File> classResources = getClassResources();
        Set<String> classes = ClassNamesUtil.getClasses(classResources, "", true);
        return classes;
    }

    public static Set<String> getClassesUseManifest() {
        Set<File> classResources = getClassResources();

        Set<File> resourcesByManifests = new HashSet<>();
        for (File file : classResources) {
            String classPathString = getClassPathFromManifest(file);
            if (classPathString != null) {
                String[] split = classPathString.split(" ");
                for (String filePath : split) {
                    File fileByManifest = new File(filePath);
                    if (file.isDirectory()) {
                        File fileToAdd = new File(file.toString(), fileByManifest.toString());
                        if (fileToAdd.isFile()) {
                            resourcesByManifests.add(fileToAdd);
                        }
                    } else {
                        File fileToAdd = new File(file.getParent() != null ? file.getParent().toString() : ".", fileByManifest.toString());
                        if (fileToAdd.isFile()) {
                            resourcesByManifests.add(fileToAdd);
                        }
                    }
                }
            }
        }

        classResources.addAll(resourcesByManifests);

        Set<String> classes = ClassNamesUtil.getClasses(classResources, "", true);

        return classes;
    }

    private static Set<File> getClassResources() {
        String absoluteOrRelativePath = System.getProperty("java.class.path");
        String[] absoluteOrRelativePaths = absoluteOrRelativePath.split(File.pathSeparator);

        Set<File> files = new HashSet<File>();
        for (String path : absoluteOrRelativePaths) {
            File file = new File(path);
            if (file.exists()) {
                files.add(file);
            }
        }

        Set<File> classesFromClassloader = getClassesFromClassloader();
        files.addAll(classesFromClassloader);

        return files;
    }

    /**
     * only for unit tests has sence. Since java 9 it doesn't work for common run
     * @return
     */
    private static Set<File> getClassesFromClassloader() {
        Set<File> files = new HashSet<File>();
        ClassLoader classLoader = LibrariesClassesDetector.class.getClassLoader();
        if (classLoader instanceof URLClassLoader) {
            @SuppressWarnings("resource")
            URLClassLoader urlClassLoader = (URLClassLoader) classLoader;
            URL[] urLs = urlClassLoader.getURLs();
            for (URL url : urLs) {
                files.add(new File(url.getPath()));
            }
        }
        return files;
    }

    public static String getClassPathFromManifest(File file) {
        if (file.isDirectory()) {
            String manifestPath = "file:" + file.getPath() + "/META-INF/MANIFEST.MF";
            return getClassPathFromManifest(manifestPath);
        } else {
            if (!file.toString().endsWith(".jar")) {
                return null;
            }
            String manifestPath = "jar:file:" + file.getPath() + "!/META-INF/MANIFEST.MF";
            return getClassPathFromManifest(manifestPath);
        }
    }

    private static String getClassPathFromManifest(String manifestPath) {
        try (InputStream is = new URL(manifestPath).openStream()) {
            Manifest manifest = new Manifest(is);
            Attributes mainAttribs = manifest.getMainAttributes();
            return mainAttribs.getValue(Name.CLASS_PATH);
        } catch (IOException e) {
            // not exists is normal
        }
        return null;
    }
}
