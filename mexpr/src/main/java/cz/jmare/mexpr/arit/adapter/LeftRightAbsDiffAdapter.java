package cz.jmare.mexpr.arit.adapter;

import java.util.function.Supplier;

public class LeftRightAbsDiffAdapter implements Supplier<Double> {
    private final Supplier<Double> leftSupplier;
    private final Supplier<Double> rightSupplier;

    public LeftRightAbsDiffAdapter(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
    }

    @Override
    public Double get() {
        return Math.abs(leftSupplier.get() - rightSupplier.get());
    }
}
