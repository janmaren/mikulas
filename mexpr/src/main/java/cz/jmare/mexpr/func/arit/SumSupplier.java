package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sum", description = "Returns sum of terms.")
public class SumSupplier implements Supplier<Double> {
    private Supplier<Double> arg1;

    private Supplier<Double>[] args;

    @SafeVarargs
    public SumSupplier(@Desc("term1") Supplier<Double> arg1, @Desc("terms") Supplier<Double>... args) {
        this.arg1 = arg1;
        this.args = args;
    }

    @SuppressWarnings("unchecked")
    public SumSupplier(@Desc("term1") Supplier<Double> arg1) {
        this.arg1 = arg1;
        this.args = new Supplier[0];
    }

    @Override
    public Double get() {
        double sum = arg1.get();
        for (Supplier<Double> supplier : args) {
            sum += supplier.get();
        }
        return sum;
    }
}
