package mexpr;

import cz.jmare.mexpr.util.LeftRight;
import org.junit.Assert;
import org.junit.Test;

public class LeftRightTest {
    @Test
    public void testParseLeftRight1() {
        LeftRight leftRight = new LeftRight("x=1", 1);
        Assert.assertEquals("x", leftRight.left);
    }
}
