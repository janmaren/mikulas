package cz.jmare.mexpr.util;

public class CharsUtil {
    public static String toString(CharSequence charSequence) {
        if (charSequence instanceof String) {
            return (String) charSequence;
        }
        final StringBuilder sb = new StringBuilder(charSequence.length());
        sb.append(charSequence);
        return sb.toString();
    }

    public static int indexOf(CharSequence charSequence, char ch, int fromPos) {
        for (int i = fromPos; i < charSequence.length(); i++) {
            if (charSequence.charAt(i) == ch) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(CharSequence charSequence, char ch) {
        return indexOf(charSequence, ch, 0);
    }
}
