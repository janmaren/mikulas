package cz.jmare.mexpr.func.oper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.exception.run.InvalidValueException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreach", description = "Do an operation with each item of VectorD1. Example: foreach(x * 10, x, d1(1, 2, 3, 4, 5))")
public class ForeachD1Supplier implements Supplier<VectorD1> {
    private Supplier<Double> supplier;
    private DoubleHolder itemHolder;
    private Supplier<VectorD1> vectorD1Supplier;


    public ForeachD1Supplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder itemHolder, @Desc("values") Supplier<VectorD1> vectorD1Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.vectorD1Supplier = vectorD1Supplier;
    }

    @Override
    public VectorD1 get() {
        Thread currentThread = Thread.currentThread();
        VectorD1 vectorD1 = this.vectorD1Supplier.get();
        int size = vectorD1.size();
        List<Double> resultDoubles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            itemHolder.accept(vectorD1.get(i));
            Object value = supplier.get();
            if (value instanceof Double) {
                resultDoubles.add((Double) value);
            } else {
                throw new InvalidValueException("Expected that expression will return a Double value but returned " + (value == null ? "null" : value.getClass()));
            }
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("foreach interrupted");
            }
        }

        return new VectorD1Impl(resultDoubles.stream().mapToDouble(Double::doubleValue).toArray());
    }
}
