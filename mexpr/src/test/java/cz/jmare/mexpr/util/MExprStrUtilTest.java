package cz.jmare.mexpr.util;

import org.junit.Assert;
import org.junit.Test;

public class MExprStrUtilTest {
    @Test
    public void testFindSeparatorIndex() {
        int separatorIndex = MExprStrUtil.findSeparatorIndex("\"ab=\"=3", '=');
        Assert.assertEquals(5, separatorIndex);
    }

    @Test
    public void testFindSeparatorIndex2() {
        int separatorIndex = MExprStrUtil.findSeparatorIndex("\"ab=\"", '=');
        Assert.assertEquals(-1, separatorIndex);
    }

    @Test
    public void testFindSeparatorIndex3() {
        int separatorIndex = MExprStrUtil.findSeparatorIndex("a=b", '=');
        Assert.assertEquals(1, separatorIndex);
    }

    @Test
    public void testFindSeparatorIndex4() {
        int separatorIndex = MExprStrUtil.findSeparatorIndex("=b", '=');
        Assert.assertEquals(0, separatorIndex);
    }
}