package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "between", description = "Returns 1 when value is between min (including) and max (excluding, unless including parameter is 1) value. Otherwise 0 (never NaN).")
public class BetweenSupplier implements Supplier<Double> {
    private Supplier<Double> valueSupplier;
    private Supplier<Double> fromIncludingSupplier;
    private Supplier<Double> toExcludingSupplier;
    private Supplier<Double> includingSupplier;

    public BetweenSupplier(@Desc("value") Supplier<Double> valueSupplier, @Desc("from") Supplier<Double> fromIncludingSupplier, @Desc("to") Supplier<Double> toExcludingSupplier) {
        this.valueSupplier = valueSupplier;
        this.fromIncludingSupplier = fromIncludingSupplier;
        this.toExcludingSupplier = toExcludingSupplier;
    }

    public BetweenSupplier(@Desc("value") Supplier<Double> valueSupplier, @Desc("from") Supplier<Double> fromIncludingSupplier, @Desc("to") Supplier<Double> toExcludingSupplier, @Desc("When passed 1 the second argument will be also including, otherwise excluding") Supplier<Double> includingSupplier) {
        this.valueSupplier = valueSupplier;
        this.fromIncludingSupplier = fromIncludingSupplier;
        this.toExcludingSupplier = toExcludingSupplier;
        this.includingSupplier = includingSupplier;
    }

    @Override
    public Double get() {
        boolean including = includingSupplier != null && includingSupplier.get() == 1.0;
        Double value = valueSupplier.get();
        if (including) {
            if (value >= fromIncludingSupplier.get() && value <= toExcludingSupplier.get()) {
                return 1.0;
            }
        } else {
            if (value >= fromIncludingSupplier.get() && value < toExcludingSupplier.get()) {
                return 1.0;
            }
        }
        return 0.0;
    }
}
