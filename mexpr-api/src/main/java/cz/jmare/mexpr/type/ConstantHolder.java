package cz.jmare.mexpr.type;

import java.util.function.Supplier;

/**
 * Supplier of double. Such values can't be changed, for such case use {@link DoubleHolder}
 */
public class ConstantHolder implements Supplier<Double> {
    private double value;

    public ConstantHolder(double value) {
        this.value = value;
    }

    @Override
    public Double get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
