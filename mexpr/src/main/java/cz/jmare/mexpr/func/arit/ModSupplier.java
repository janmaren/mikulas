package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "mod", description = "Modulo. Returns the remainder when a value is divided by divisor.")
public class ModSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    private Supplier<Double> divisor;

    public ModSupplier(@Desc("value") Supplier<Double> supplier, @Desc("divisor") Supplier<Double> divisor) {
        this.supplier = supplier;
        this.divisor = divisor;
    }

    @Override
    public Double get() {
        return supplier.get() % divisor.get();
    }
}
