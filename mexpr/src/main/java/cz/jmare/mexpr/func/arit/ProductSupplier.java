package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "prod", description = "Returns product of factors.")
public class ProductSupplier implements Supplier<Double> {
    private Supplier<Double> arg1;

    private Supplier<Double>[] args;

    @SafeVarargs
    public ProductSupplier(@Desc("factor1") Supplier<Double> arg1, @Desc("factors") Supplier<Double>... args) {
        this.arg1 = arg1;
        this.args = args;
    }

    @SuppressWarnings("unchecked")
    public ProductSupplier(@Desc("factor1") Supplier<Double> arg1) {
        this.arg1 = arg1;
        this.args = new Supplier[0];
    }

    @Override
    public Double get() {
        double prod = arg1.get();
        for (Supplier<Double> supplier : args) {
            prod *= supplier.get();
        }
        return prod;
    }
}
