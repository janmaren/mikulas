package cz.jmare.mexpr.util;

public class MExprStrUtil {
    public static int findSeparatorIndex(CharSequence str, char separator) {
        return findSeparatorIndex(str, separator, 0);
    }

    public static int findSeparatorIndex(CharSequence str, char separator, int fromPos) {
        int pos = fromPos;
        while ((pos = CharsUtil.indexOf(str, separator, pos)) != -1) {
            if (isDquotesNumEven(str, pos)) {
                return pos;
            }
            pos++;
        }
        return -1;
    }

    private static boolean isDquotesNumEven(CharSequence str, int end) {
        int number = 0;
        int pos = 0;
        while (pos < end) {
            if (str.charAt(pos) == '"') {
                number++;
            }
            pos++;
        }
        return number % 2 == 0;
    }

    public static void main(String[] args) {

    }
}
