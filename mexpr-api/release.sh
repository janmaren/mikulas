mvn versions:set -DremoveSnapshot -DgenerateBackupPoms=false

git add --all
git commit -m "release"

mvn clean deploy -P release
if [[ $? -ne 0 ]] ; then
    exit 1
fi
mvn versions:set -DnextSnapshot -DgenerateBackupPoms=false

git add --all
git commit -m "set development version"

git push
