package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "coth", description = "Returns the hyperbolic cotangent of an angle.")
public class CothSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CothSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return 1.0 / Math.tanh(supplier.get());
    }
}
