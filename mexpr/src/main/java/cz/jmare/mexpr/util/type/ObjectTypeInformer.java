package cz.jmare.mexpr.util.type;

/**
 * Type of result object for cases when impossible to get it from Supplier&lt;Type&gt;
 */
public interface ObjectTypeInformer {
	public Class<?> getObjectClass();
}
