package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "log10", description = "Returns the base 10 logarithm of a double value.")
public class Log10Supplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public Log10Supplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) Math.log10(supplier.get());
    }
}
