package cz.jmare.mexpr;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import cz.jmare.mexpr.util.SuppliersClassesProvider;
import cz.jmare.mexpr.util.SuppliersLibraryManager;
import cz.jmare.mexpr.util.classutil.ConstantSupplierClassInfo;
import cz.jmare.mexpr.util.classutil.FunctionalSupplierClassInfo;
import cz.jmare.mexpr.util.classutil.SupplierInfos;

/**
 * Configuration for parsing.<br>
 * Fields with defaults:<br>
 * <li> overrideConstant: true - variable overrides a constant when used the same name
 * <li> predefinedClasses: true - load predefined functions like sin, cos, log and constants like e, pi
 * <li> defaultMetricPrefixes: false - when true the metric prefixes may be used behind a number, like k for 1000times, m for 0.001times...
 * <li> externalLibraries: NaN - load also functions and classes from external jar files or directory with classes
 * <br><br>
 * Metric prefixes:<br>
 * T: 1000000000000<br>
 * G: 1000000000<br>
 * M: 1000000<br>
 * k: 1000<br>
 * m: 0.001<br>
 * u: 0.000001<br>
 * n: 0.000000001<br>
 * p: 0.000000000001<br>
 */
public class Config {
    public static final Map<String, Double> DEFAULT_METRIC_PREFIXES = new HashMap<>();

    static {
        DEFAULT_METRIC_PREFIXES.put("T", 1000000000000.0);
        DEFAULT_METRIC_PREFIXES.put("G", 1000000000.0);
        DEFAULT_METRIC_PREFIXES.put("M", 1000000.0);
        DEFAULT_METRIC_PREFIXES.put("k", 1000.0);
        DEFAULT_METRIC_PREFIXES.put("m", 0.001);
        DEFAULT_METRIC_PREFIXES.put("u", 0.000001);
        DEFAULT_METRIC_PREFIXES.put("n", 0.000000001);
        DEFAULT_METRIC_PREFIXES.put("p", 0.000000000001);
    }

    /**
     * True when required to use default metric prefixes, like T, G, M, k,...
     */
    private boolean defaultMetricPrefixes;

    /**
     * True (default) when required to use predefined functions and classes starting with cz.jmare.mexpr.
     */
    private boolean predefinedClasses = true;

    /**
     * Prepared for future use...
     */
    private Map<String, Double> customMetricPrefixes = new HashMap<>();

    /**
     * Prepared for future use...
     */
    private Map<String, Supplier<Double>> customConstants = new HashMap<>();

    /**
     * Prepared for future use...
     */
    private Map<String, List<Class<?>>> customFunctions = new HashMap<>();

    /**
     * True (default) when overriding constants is allowed
     */
    private boolean overrideConstant = true;
    
    /**
     * All classpath based functions and constants used for parsing and evaluation
     */
    private SupplierInfos supplierInfos;

    /**
     * Collection of jar files or directories which contain functions and constants to be loaded 
     */
    private Collection<File> externalLibraries;
    
    /**
     * When true a letter behind a number maybe used and it's a factor like k=1000, m=0.001 
     * @param defaultMetricPrefixes
     * @return
     */
    public Config withDefaultMetricPrefixes(boolean defaultMetricPrefixes) {
        this.defaultMetricPrefixes = defaultMetricPrefixes;
        return this;
    }

    /**
     * Add custom metric prefix. For example "da" => 10 for deka
     * @param metricPrefixes
     * @return
     */
    public Config withMetricPrefixes(Map<String, Double> metricPrefixes) {
        this.customMetricPrefixes = metricPrefixes;
        return this;
    }

    /**
     * When true (default) then use all predefined (cz.jmare.mexpr) constants and functions found on classpath.
     * @param predefinedClasses
     * @return
     */
    public Config withPredefinedClasses(boolean predefinedClasses) {
        this.predefinedClasses = predefinedClasses;
        supplierInfos = null;
        return this;
    }

    /**
     * Get all constants usable in expression. It depends if default constants are enabled and also custom constants are encountered
     * @return
     */
    Map<String, Supplier<Double>> getConstants() {
        doSupplierInfos();
        HashMap<String, Supplier<Double>> constants = new HashMap<String, Supplier<Double>>();
        List<ConstantSupplierClassInfo> constantClassInfos = supplierInfos.consInfos;
        constantClassInfos.stream().forEach(i -> {
            constants.put(i.constantName, i.supplier);
        });
        constants.putAll(customConstants);
        return constants;
    }

    private void doSupplierInfos() {
        if (supplierInfos == null) {
            supplierInfos = new SupplierInfos();

            List<SuppliersLibrary> suppliersLibraryList = getSuppliersLibraryList();
            for (SuppliersLibrary suppliersLibrary : suppliersLibraryList) {
                supplierInfos.funcInfos.addAll(suppliersLibrary.supplierInfos.funcInfos);
                supplierInfos.consInfos.addAll(suppliersLibrary.supplierInfos.consInfos);
            }
        }
    }
    
    /**
     * Get all classloaded functions and constants in accordance with setting like predefinedClasses and externalLibraries.
     * The functions added as funcClasses and customConstants are not encountered  
     * @return SupplierInfos instance with functions and constants to be used
     */
    public SupplierInfos getSupplierInfos() {
        doSupplierInfos();
        return supplierInfos;
    }

    public List<SuppliersLibrary> getSuppliersLibraryList() {
        return SuppliersLibraryManager.getInstance().getSuppliersLibraryList(
                predefinedClasses,
                externalLibraries != null ? externalLibraries : List.of());
    }

    /**
     * Get all functions used by parser
     * @return
     */
    Map<String, List<Class<?>>> getFuncs() {
        doSupplierInfos();
        HashMap<String, List<Class<?>>> functions = new HashMap<>();
        List<FunctionalSupplierClassInfo> supplierClassInfos = supplierInfos.funcInfos;
        supplierClassInfos.stream().forEach(i -> {
            List<Class<?>> classesForFunctionName = functions.get(i.functionName);
            if (classesForFunctionName == null) {
                classesForFunctionName = new ArrayList<>();
                functions.put(i.functionName, classesForFunctionName);
            }
            classesForFunctionName.add(i.supplierClass);
        });
        functions.putAll(customFunctions);
        return functions;
    }

    /**
     * Get all metric prefixes used by parser
     * @return
     */
    Map<String, Double> getMetricPrefixes() {
        Map<String, Double> metricPrefixes = new HashMap<String, Double>();
        if (defaultMetricPrefixes) {
            metricPrefixes.putAll(DEFAULT_METRIC_PREFIXES);
        }
        metricPrefixes.putAll(customMetricPrefixes);
        return metricPrefixes;
    }

//    /**
//     * Add custom constants, where key is a constant name as used in expression and value is a Supplier providing
//     * a double value.<br>
//     * Note that constant may not have the @ConstantSupplier annotation.
//     * @param customConstants
//     * @return
//     */
//    public Config withCustomConstants(Map<String, Supplier<Double>> customConstants) {
//        this.customConstants.putAll(customConstants);
//        supplierInfos = null;
//        return this;
//    }

//    /**
//     * Add custom functions, where key is a function name as used in expression and
//     * value is a list of canonical class names with various argument variants, unless
//     * multiple constructors within one class used.<br>
//     * Instances of such functions will be created using constructors with Suppliers as arguments<br>
//     * Note that function may not have the @FunctionalSupplier annotation.
//     * @param customFunctions
//     * @return
//     */
//    public Config withFuncClasses(Map<String, List<Class<?>>> customFunctions) {
//        this.customFunctions.putAll(customFunctions);
//        supplierInfos = null;
//        return this;
//    }

    /**
     * When true (default), the constant can be overriden by a variable, otherwise it fails
     * @param overrideConstant
     * @return
     */
    public Config withOverrideConstant(boolean overrideConstant) {
        this.overrideConstant = overrideConstant;
        return this;
    }

    /**
     * External jar files (or directories with classes) which contained functions and constants to be used
     * @param externalLibraries
     * @return
     */
    public Config withExternalLibraries(Collection<File> externalLibraries) {
        this.externalLibraries = externalLibraries;
        supplierInfos = null;
        return this;
    }

    boolean isOverrideConstant() {
        return overrideConstant;
    }
}
