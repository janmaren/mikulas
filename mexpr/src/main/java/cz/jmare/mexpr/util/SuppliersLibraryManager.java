package cz.jmare.mexpr.util;

import cz.jmare.mexpr.SuppliersLibrary;
import cz.jmare.mexpr.util.classutil.SupplierInfos;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SuppliersLibraryManager {

    private static SuppliersLibraryManager suppliersLibraryManager;

    private SupplierInfos supplierInfosEmbeddedPredCache;

    /**
     * Embedded cached without predefined
     */
    private SupplierInfos supplierInfosEmbeddedWithoutPredCache;

    private Map<File, LibraryInfoCache> libsCaches = new HashMap<>();

    private SuppliersLibraryManager() {
    }

    public synchronized static SuppliersLibraryManager getInstance() {
        if (suppliersLibraryManager == null) {
            suppliersLibraryManager = new SuppliersLibraryManager();
        }
        return suppliersLibraryManager;
    }

    public List<SuppliersLibrary> getSuppliersLibraryList(boolean withPredefined, Collection<File> externalFiles) {
        List<SuppliersLibrary> infos = new ArrayList<>();

        SupplierInfos supplierInfosEmbedded;
        if (!withPredefined) {
            if (supplierInfosEmbeddedWithoutPredCache == null) {
                supplierInfosEmbedded = SuppliersClassesProvider.getSupplierInfosEmbedded(false);
                supplierInfosEmbeddedWithoutPredCache = supplierInfosEmbedded;
            } else {
                supplierInfosEmbedded = supplierInfosEmbeddedWithoutPredCache;
            }
        } else {
            if (supplierInfosEmbeddedPredCache == null) {
                supplierInfosEmbedded = SuppliersClassesProvider.getSupplierInfosEmbedded(true);
                supplierInfosEmbeddedPredCache = supplierInfosEmbedded;
            } else {
                supplierInfosEmbedded = supplierInfosEmbeddedPredCache;
            }
        }
        SuppliersLibrary infoEmb = new SuppliersLibrary();
        infoEmb.embedded = true;
        infoEmb.name = "Basic";
        infoEmb.supplierInfos = supplierInfosEmbedded;
        infos.add(infoEmb);

        for (File file : externalFiles) {
            LibraryInfoCache libraryInfoCache = libsCaches.get(file);
            if (libraryInfoCache != null) {
                if (file.length() != libraryInfoCache.fileLenght || file.lastModified() != libraryInfoCache.fileLastModified) {
                    libraryInfoCache = null; // invalidate changed file
                }
            }
            SupplierInfos supplierInfos;
            if (libraryInfoCache == null) {
                supplierInfos = SuppliersClassesProvider.getSupplierInfosForFiles(List.of(file));
                libraryInfoCache = new LibraryInfoCache();
                libraryInfoCache.fileLenght = file.length();
                libraryInfoCache.fileLastModified = file.lastModified();
                libraryInfoCache.file = file;
                libraryInfoCache.supplierInfos = supplierInfos;
                libsCaches.put(file, libraryInfoCache);
            } else {
                supplierInfos = libraryInfoCache.supplierInfos;
            }

            SuppliersLibrary info = new SuppliersLibrary();
            info.embedded = false;
            String path = SimplePathUtil.getLastPartFromPath(file.toString());
            String[] nameAndSuffix = SimplePathUtil.getNameAndSuffixOrEmpty(path);
            info.name = nameAndSuffix[0];
            info.supplierInfos = supplierInfos;
            info.externalFile = file;
            infos.add(info);
        }
        return infos;
    }
}
