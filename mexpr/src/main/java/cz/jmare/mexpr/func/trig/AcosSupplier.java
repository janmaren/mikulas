package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "acos", description = "Returns the arc cosine of a value; the returned angle is in the range 0.0 through pi.")
public class AcosSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public AcosSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.acos(supplier.get());
    }
}
