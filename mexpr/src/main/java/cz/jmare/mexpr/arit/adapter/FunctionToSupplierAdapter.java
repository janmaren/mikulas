package cz.jmare.mexpr.arit.adapter;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Convert a function to supplier which can be bound to NumberHolder by NumberSupplierAdapter. Then such NumberHolder can
 * accept functional argument and pass it to function which acts as a Supplier providing calculated result.
 * Example of use:
 * <pre>
 *        FunctionToSupplierAdapter quadrSupplier = new FunctionToSupplierAdapter((x) -> x * 2);
 *        NumberSupplierAdapter numberSupplier = new NumberSupplierAdapter(quadrSupplier);
 * </pre>
 */
public class FunctionToSupplierAdapter implements Consumer<Double>, Supplier<Double> {
    private Function<Double, Double> function;

    private Double lastY;

    public FunctionToSupplierAdapter(Function<Double, Double> function) {
        super();
        this.function = function;
    }

    @Override
    public void accept(Double xValue) {
        lastY = function.apply(xValue);
    }

    @Override
    public Double get() {
        return lastY;
    }
}
