package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "asin", description = "Returns the arc sine of a value; the returned angle is in the range -pi/2 through pi/2.")
public class AsinSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public AsinSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.asin(supplier.get());
    }
}
