package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "toDeg", description = "Converts an angle measured in radians to an approximately equivalent angle measured in degrees.")
public class DegSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    public DegSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.toDegrees(supplier.get());
    }
}
