package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.type.VectorD3;
import cz.jmare.mexpr.type.VectorD3Impl;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "d3", description = "Factory for VectorD3 type. VectorD3 is one dimensional array of VectorD2 which is in fact a three dimensional array of doubles.")
public class D3Supplier implements Supplier<VectorD3> {
    private Supplier<VectorD2> supplier;
    private Supplier<VectorD2>[] suppliers;

    @SuppressWarnings("unchecked")
    public D3Supplier() {
        this.supplier = null;
        this.suppliers = new Supplier[0];
    }

    @SafeVarargs
    public D3Supplier(Supplier<VectorD2> supplier, Supplier<VectorD2>... suppliers) {
        this.supplier = supplier;
        this.suppliers = suppliers;
    }

    @SuppressWarnings("unchecked")
    public D3Supplier(Supplier<VectorD2> supplier) {
        this.supplier = supplier;
        this.suppliers = new Supplier[0];
    }

    @Override
    public VectorD3 get() {
        if (supplier == null) {
            return new VectorD3Impl(new VectorD2[0]);
        }
        VectorD2[] values = new VectorD2[suppliers.length + 1];
        values[0] = supplier.get();
        for (int i = 0; i < suppliers.length; i++) {
            values[i + 1] = suppliers[i].get();
        }
        return new VectorD3Impl(values);
    }
}
