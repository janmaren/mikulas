package cz.jmare.mexpr.util;

public class NotExpressionException extends RuntimeException {

    private static final long serialVersionUID = -233375385192226556L;

    public NotExpressionException(String message) {
        super(message);
    }

}
