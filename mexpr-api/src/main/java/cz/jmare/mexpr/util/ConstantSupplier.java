package cz.jmare.mexpr.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark a class with a constant to be classloaded  
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConstantSupplier {
    /**
     * Name of constant to be used in expression, like pi, e, ...
     * @return
     */
    String name();

    /**
     * Description of the constant
     * @return
     */
    String description() default "";
}
