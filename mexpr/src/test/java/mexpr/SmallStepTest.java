package mexpr;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SmallStepTest {
    @Test
    public void distingLargeTest() {
        double x, x2;
        x = 1;
        x2 = x + 1;
        while (!equalsExactly(x, x2)) {
            x *= 10;
            x2 = x + 1;
        }
        assertEquals(1.0E16, x, 0.00000000001);
    }

    @Test
    public void smallStepTest() {
        double x, x2;
        x = 1;
        double step = 1;
        x2 = x + step;
        while (!equalsExactly(x, x2)) {
            step /= 10;
            x2 = x + step;
        }
        assertEquals(1.0000000000000001E-16, step, 0.00000000001);
    }

    public static boolean equalsExactly(double x1, double x2) {
        return x1 == x2;
    }
}
