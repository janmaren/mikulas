package cz.jmare.mexpr.arit;

import java.util.function.Supplier;

/**
 * factor -> negation [ '^' negation ]*
 */
public class ExponentSupplier implements Supplier<Double> {
    private Supplier<Double> initialNumber;

    private Supplier<Double>[] exponents;

    public final static int OPERATOR_PLUS = 1;

    public final static int OPERATOR_MINUS = -1;


    public ExponentSupplier(Supplier<Double> initialNumber, Supplier<Double>[] exponents) {
        super();
        this.initialNumber = initialNumber;
        this.exponents = exponents;
    }

    @Override
    public Double get() {
        if (exponents.length == 0) {
            return initialNumber.get();
        }

        double expSum = exponents[exponents.length - 1].get();
        for (int i = exponents.length - 2; i >= 0; i--) {
            expSum = Math.pow(exponents[i].get(), expSum);
        }

        return Math.pow(initialNumber.get(), expSum);
    }
}
