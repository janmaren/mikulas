package cz.jmare.mexpr.type;

/**
 * Vector of vector of vector of scalar doubles.<br>
 * Example: [[1, 3], [2, 4]], [[5, 6], [7, 8]]
 */
public interface VectorD3 {
    VectorD2 get(int index);

    int size();
}
