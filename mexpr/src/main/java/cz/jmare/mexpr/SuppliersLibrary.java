package cz.jmare.mexpr;

import java.io.File;

import cz.jmare.mexpr.util.classutil.SupplierInfos;

public class SuppliersLibrary {
    /**
     * Name - for external jars it's usually jar name
     */
    public String name;

    /**
     * Supplier classes with functions and constants
     */
    public SupplierInfos supplierInfos;

    /**
     * Embedded classes directly on clsspath together with running application (not external jars)
     */
    public boolean embedded;

    /**
     * External file when embedded is false otherwise this is null
     */
    public File externalFile;
}
