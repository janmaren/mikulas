package cz.jmare.mexpr.parse;

import cz.jmare.mexpr.exception.syntax.UnexpectedEndOfCodeException;
import cz.jmare.mexpr.util.Chars;

public interface Token {
    /**
     * Find next token which contains exactly given string<br/>
     * Useful for recovery after pasring error
     * @param string
     * @return
     */
//    Token findNext(String string);

    /**
     * Length of token
     * @return
     */
    int getLength();

    /**
     * Position of token - start
     * @return
     */
    int getOffset();

    /**
     * Token
     * @return
     */
    String getString();

    /**
     * True when there is next token behind this one (single or not single)
     * @return
     */
    boolean hasNext();

    /**
     * Return next token behind this one. Returned token will contain all meanful characters
     * @return
     * @throws UnexpectedEndOfCodeException when end of source string reached and token can't be returned
     */
    Token next();

    Chars getPosition();
}
