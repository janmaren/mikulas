package cz.jmare.mexpr.func.vector;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD3;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "count", description = "Returns count of items of vector.")
public class CountD3Supplier implements Supplier<Double> {
    private Supplier<VectorD3> supplier;

    public CountD3Supplier(Supplier<VectorD3> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) supplier.get().size();
    }
}
