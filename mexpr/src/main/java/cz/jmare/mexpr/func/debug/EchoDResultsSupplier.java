package cz.jmare.mexpr.func.debug;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.Result;
import cz.jmare.mexpr.type.VectorResults;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "echo", description = "Output results vector to stdout and return as value.")
public class EchoDResultsSupplier implements Supplier<VectorResults> {
    private Supplier<VectorResults> argSup;

    public EchoDResultsSupplier(Supplier<VectorResults> arg1) {
        this.argSup = arg1;
    }

    @Override
    public VectorResults get() {
    	VectorResults vec = argSup.get();
        int size = vec.size();
        for (int i = 0; i < size; i++) {
            Result obj = vec.get(i);
            System.out.println(obj.x + "\t" + obj.y);
        }
        return vec;
    }
}
