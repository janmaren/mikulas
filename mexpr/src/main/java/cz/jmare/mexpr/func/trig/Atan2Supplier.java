package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "atan2", description = "Returns the angle theta from the conversion of rectangularcoordinates (x, y) to polarcoordinates (r, theta).")
public class Atan2Supplier implements Supplier<Double> {
    private Supplier<Double> ordinate;
    private Supplier<Double> abscissa;

    public Atan2Supplier(@Desc("ordindate") Supplier<Double> ordinate, @Desc("abscissa") Supplier<Double> abscissa) {
        super();
        this.ordinate = ordinate;
        this.abscissa = abscissa;
    }

    @Override
    public Double get() {
        return Math.atan2(ordinate.get(), abscissa.get());
    }
}
