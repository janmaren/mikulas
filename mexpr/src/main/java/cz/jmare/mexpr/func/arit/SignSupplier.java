package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sign", description = "Returns the signum function of the argument; zero if the argument is zero, 1.0 if the argument is greater than zero, -1.0 if the argument is less than zero, NaN if the argument is NaN.")
public class SignSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public SignSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.signum(supplier.get());
    }
}
