package cz.jmare.mexpr.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark a class with a function to be classloaded  
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FunctionalSupplier {
    /**
     * Name of function to be used in expression
     * @return
     */
    String name();

    /**
     * Description of use of the function
     * @return
     */
    String description() default "";
}
