package cz.jmare.mexpr.exception.syntax;

import cz.jmare.mexpr.exception.SyntaxException;
import cz.jmare.mexpr.parse.Token;

public class IllegalTokenException extends SyntaxException {
    private static final long serialVersionUID = -8621796980731351640L;

    public IllegalTokenException(String illegalToken, int offset, int length) {
        super("Illegal text '" + illegalToken + "'", offset, length);
    }

    public IllegalTokenException(String illegalToken, int offset, int length, String string) {
        super("Illegal text '" + illegalToken + "' in '" + string + "'", offset, length);
    }

    public IllegalTokenException(Token token) {
        this(token.getString(), token.getOffset(), token.getLength());
        setPosition(token.getPosition());
    }
}
