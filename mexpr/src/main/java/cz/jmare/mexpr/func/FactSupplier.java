package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "fact", description = "Returns the factorial value of an integer.")
public class FactSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    public FactSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return factorial((int) (double) supplier.get());
    }

    public static double factorial(int number) {
        double result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }
}
