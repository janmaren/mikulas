package cz.jmare.mexpr.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import cz.jmare.mexpr.util.classutil.ClassNamesUtil;
import cz.jmare.mexpr.util.classutil.ClassUtils;
import cz.jmare.mexpr.util.classutil.ConstantSupplierClassInfo;
import cz.jmare.mexpr.util.classutil.ConstructorInfo;
import cz.jmare.mexpr.util.classutil.FunctionalSupplierClassInfo;
import cz.jmare.mexpr.util.classutil.LibrariesClassesDetector;
import cz.jmare.mexpr.util.classutil.SupplierInfos;
import cz.jmare.mexpr.util.classutil.SupplierTypeUtil;

/**
 * Main class to get classes infos about functions and constants from classpath
 */
public class SuppliersClassesProvider {
	/**
	 * Get SupplierInfos which contain classes found on classpath
	 * @param includingPredefinedClasses true means to include classes whose package starts with 'cz.jmare.mexpr' 
	 * @return SupplierInfos with functions and constants
	 */
    public static SupplierInfos getSupplierInfosEmbedded(boolean includingPredefinedClasses) {
        Set<String> allClasses = LibrariesClassesDetector.getClassesUseManifest();
        Set<String> classes = LibrariesClassesDetector.getClasses();
        allClasses.addAll(classes);
        if (!includingPredefinedClasses) {
            allClasses = allClasses.stream().filter(s -> !s.startsWith(Consts.mexprPackage)).collect(Collectors.toSet());
        }
        SupplierInfos supplierInfos = SuppliersClassesProvider.getSupplierClassInfosInternal(allClasses, classCanon -> ClassUtils.getClassByName(classCanon), true);
        return supplierInfos;
    }

	/**
	 * Get SupplierInfos which contain classes present in jar files or directory with classes
	 * @param files jar files or directory with classes
	 * @return SupplierInfos with functions and constants
	 */
    public static SupplierInfos getSupplierInfosForFiles(Collection<File> files) {
        Set<String> classes = ClassNamesUtil.getClasses(files, "", true);
        URL[] urls = new URL[files.size()];
        int index = 0;
        for (File file : files) {
            try {
                urls[index++] = file.toURI().toURL();
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("Unable convert " + file + " to URL");
            }
        }
        try (URLClassLoader urlClassLoader = new URLClassLoader(urls)) {
            SupplierInfos supplierInfos = SuppliersClassesProvider.getSupplierClassInfosInternal(classes, classCanon -> {
                try {
                    return urlClassLoader.loadClass(classCanon);
                } catch (ClassNotFoundException e) {
                    throw new IllegalStateException("Unable to find " + classCanon + " among " + files);
                }
            }, false);
            return supplierInfos;
        } catch (IOException e1) {
            throw new IllegalStateException("Unable to close classloader with " + files);
        }
    }

    @SuppressWarnings("unchecked")
	private static SupplierInfos getSupplierClassInfosInternal(Set<String> allClasses, Function<String, Class<?>> classProvider, boolean classpathClass) {
        ArrayList<FunctionalSupplierClassInfo> resultFunctionalSupplierClassInfos = new ArrayList<>();
        ArrayList<ConstantSupplierClassInfo> resultConstantSupplierClassInfos = new ArrayList<>();
        for (String classCanon : allClasses) {
            Class<?> clazz;
            try {
                clazz = classProvider.apply(classCanon);
                if (clazz == null) {
                    continue;
                }
            } catch (Exception e) {
                continue;
            }
            FunctionalSupplier functionalSupplier = clazz.getAnnotation(FunctionalSupplier.class);
            if (functionalSupplier != null) {
                FunctionalSupplierClassInfo supplierClass = new FunctionalSupplierClassInfo();
                supplierClass.supplierClass = clazz;
                supplierClass.functionName = functionalSupplier.name();
                supplierClass.functionDescription = functionalSupplier.description();
                Class<?> returnGnerClass = SupplierTypeUtil.getGenericTypeOfSupplierClass(clazz);
                supplierClass.returnType = returnGnerClass.getSimpleName();
                supplierClass.classpathClass = classpathClass;
                resultFunctionalSupplierClassInfos.add(supplierClass);

                Constructor<?>[] constructors = clazz.getConstructors();
                List<ConstructorInfo> constructorInfos = new ArrayList<>();
                for (Constructor<?> constructor : constructors) {
                    List<String> args = new ArrayList<>();
                    List<String> descs = new ArrayList<>();
                    Parameter[] parameters = constructor.getParameters();
                    for (Parameter parameter : parameters) {
                        if (parameter.isVarArgs()) {
                            Class<?> type = parameter.getType();
                            type = type.getComponentType(); // strip array to get one item
                            java.lang.reflect.ParameterizedType parameterizedType = (java.lang.reflect.ParameterizedType)((java.lang.reflect.GenericArrayType)parameter.getParameterizedType()).getGenericComponentType();
                            Class<?> firstClassFromGeneric = SupplierTypeUtil.getFirstClassFromGeneric(parameterizedType);
                            args.add(firstClassFromGeneric.getSimpleName() + "...");
                            descs.add(getDescsOrNull(parameter));
                        } else {
                            try {
                                if (parameter.getAnnotationsByType(InnerVariable.class).length > 0) {
                                    args.add("<inner>");
                                    descs.add(getDescsOrNull(parameter));
                                } else {
                                    Class<?> genericTypeOfSupplier = SupplierTypeUtil.getParameterSupplierGenType(parameter);
                                    args.add(genericTypeOfSupplier.getSimpleName());
                                    descs.add(getDescsOrNull(parameter));
                                }
                            } catch (Exception e) {
                                args.add("<unknown>");
                                descs.add(null);
                            }
                        }
                    }
                    ConstructorInfo constructorInfo = new ConstructorInfo(args, descs);
                    constructorInfos.add(constructorInfo);
                }
                supplierClass.constructorInfos = constructorInfos;
                continue;
            }
            ConstantSupplier constantSupplier = clazz.getAnnotation(ConstantSupplier.class);
            if (constantSupplier != null) {
                ConstantSupplierClassInfo supplierClass = new ConstantSupplierClassInfo();
                supplierClass.supplierClass = clazz;
                supplierClass.constantName = constantSupplier.name();
                supplierClass.constantDescription = constantSupplier.description();
                supplierClass.classpathClass = classpathClass;
                try {
                    supplierClass.supplier = (Supplier<Double>) clazz.getDeclaredConstructor().newInstance();
                } catch (Exception e) {
                    continue;
                }
                resultConstantSupplierClassInfos.add(supplierClass);
            }
        }

        SupplierInfos evid = new SupplierInfos();
        evid.funcInfos = resultFunctionalSupplierClassInfos;
        evid.consInfos = resultConstantSupplierClassInfos;
        return evid;
    }

    private static String getDescsOrNull(Parameter parameter) {
        Desc[] annotationsByType = parameter.getAnnotationsByType(Desc.class);
        if (annotationsByType.length > 0) {
            Desc desc = annotationsByType[0];
            return desc.value();
        }
        return null;
    }
}
