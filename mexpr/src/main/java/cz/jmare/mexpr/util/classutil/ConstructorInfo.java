package cz.jmare.mexpr.util.classutil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

public class ConstructorInfo {
    public List<String> argTypes;
    public List<String> descs;

    public ConstructorInfo(List<String> types) {
        this.argTypes = types;
        descs = new ArrayList<String>(Collections.nCopies(types.size(), null));
    }

    public ConstructorInfo(List<String> types, List<String> descs) {
        this.argTypes = types;
        this.descs = descs;
    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(", ", "(", ")");
        for (int i = 0; i < argTypes.size(); i++) {
            String argType = argTypes.get(i);
            String desc = descs.get(i);
            if (desc != null && !"".equals(desc.trim())) {
                stringJoiner.add(desc + ": " + argType);
            } else {
                stringJoiner.add(argType);
            }
        }
        return stringJoiner.toString();
    }
}
