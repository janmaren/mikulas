package cz.jmare.mexpr.arit;

import java.util.List;
import java.util.stream.Collectors;

import cz.jmare.mexpr.util.FuncDefinitionParser;

/**
 * Store function like 'f(arg1, arg2)' with implementation by java method (member or static)
 */
public class MethodFunction {
    /**
     * A class for static call or an instance for instance call
     */
    private Object implementation;

    /**
     * Name of method to be called
     */
    private String functionName;

    /**
     * Argument names present in the string
     */
    private List<String> argNames;

    public MethodFunction(String funcDef, Object implementation) {
        FuncDefinitionParser funcDefinitionParser = new FuncDefinitionParser(funcDef);
        init(funcDefinitionParser.getFunctionName(), funcDefinitionParser.getArguments(), implementation);
    }

    private void init(String functionName, List<String> argNames, Object implementation) {
        this.functionName = functionName;
        this.argNames = argNames;
        this.implementation = implementation;
    }

    @Override
    public String toString() {
        return functionName + argNames.stream().collect(Collectors.joining(", ", "(", ")"));
    }

    /**
     * Function name
     * @return
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Argument names present in f(arg1, arg2, ...) argument list
     * @return
     */
    public List<String> getArgNames() {
        return argNames;
    }

    public Object getImplementation() {
        return implementation;
    }
}
