package cz.jmare.mexpr.func.calc;

import cz.jmare.mexpr.arit.adapter.LeftRightAbsDiffAdapter;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.*;

import java.util.function.Supplier;

@FunctionalSupplier(name = "solve", description = "Calculate equation in interval -1000000 to 1000000 using step 1. Note: it can't distinguish multiple near values with interval < 1")
public class SolveDefaultsSupplier extends LocalExtremesBase {
    public SolveDefaultsSupplier(@Desc("left side expression") @UseInnerVariable  Supplier<Double> leftSupplier, @Desc("right side expression") @UseInnerVariable  Supplier<Double> rightSupplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier) {
        super(new LeftRightAbsDiffAdapter(leftSupplier, rightSupplier), variableSupplier, () -> -1000000d, () -> 1000000d, () -> 1d, TrisectExtreme.LOWEST_COMP);
    }
}
