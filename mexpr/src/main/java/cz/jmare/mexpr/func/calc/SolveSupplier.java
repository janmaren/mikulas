package cz.jmare.mexpr.func.calc;

import cz.jmare.mexpr.arit.adapter.LeftRightAbsDiffAdapter;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.*;

import java.util.function.Supplier;

@FunctionalSupplier(name = "solve", description = "Calculate equation. The interval must be small enough - half of expected period, see also Nyquist theorem")
public class SolveSupplier extends LocalExtremesBase {
    public SolveSupplier(@Desc("left side expression") @UseInnerVariable  Supplier<Double> leftSupplier, @Desc("right side expression") @UseInnerVariable  Supplier<Double> rightSupplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("From value") Supplier<Double> fromSupplier, @Desc("To value (excluding)") Supplier<Double> toSupplier, @Desc("Interval") Supplier<Double> stepSupplier) {
        super(new LeftRightAbsDiffAdapter(leftSupplier, rightSupplier), variableSupplier, fromSupplier, toSupplier, stepSupplier, TrisectExtreme.LOWEST_COMP);
    }

    public SolveSupplier(@Desc("left side expression") @UseInnerVariable  Supplier<Double> leftSupplier, @Desc("right side expression") @UseInnerVariable  Supplier<Double> rightSupplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier) {
        super(new LeftRightAbsDiffAdapter(leftSupplier, rightSupplier), variableSupplier, () -> -1000000d, () -> 1000000d, () -> 1d, TrisectExtreme.LOWEST_COMP);
    }
}
