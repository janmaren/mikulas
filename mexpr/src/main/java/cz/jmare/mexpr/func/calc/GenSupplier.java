package cz.jmare.mexpr.func.calc;

import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.InvalidValueException;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "genSequence", description = "Generate ")
public class GenSupplier implements Supplier<VectorD1> {
    private Supplier<Double> fromSupplier;
    private Supplier<Double> toSupplier;
    private Supplier<Double> stepSupplier;

    public GenSupplier(@Desc("From value") Supplier<Double> fromSupplier, @Desc("To value") Supplier<Double> toSupplier, @Desc("Step") Supplier<Double> stepSupplier) {
        this.fromSupplier = fromSupplier;
        this.toSupplier = toSupplier;
        this.stepSupplier = stepSupplier;
    }

    @Override
    public VectorD1 get() {
        double from = fromSupplier.get();
        double to = toSupplier.get();
        boolean increasing = from < to;
        if (increasing) {
            double range = toSupplier.get() - fromSupplier.get();
            Double step = stepSupplier.get();
            if ((double)(range / step + 2.0) > Integer.MAX_VALUE) {
                throw new InvalidValueException("Generator generating too much values - decrease interval or increase step");
            }
            int number = (int) (range / step) + 1;
            if (from + number * step <= to) {
                number++;
            }
            double[] arr = new double[number];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = from + i * step;
            }
            return new VectorD1Impl(arr);
        } else {
            double range = fromSupplier.get() - toSupplier.get();
            Double step = stepSupplier.get();
            if ((double)(range / step + 2.0) > Integer.MAX_VALUE) {
                throw new InvalidValueException("Generator generating too much values - decrease interval or increase step");
            }
            int number = (int) (range / step) + 1;
            if (from + -number * step >= to) {
                number++;
            }
            double[] arr = new double[number];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = from + -i * step;
            }
            return new VectorD1Impl(arr);
        }
    }

}
