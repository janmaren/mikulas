package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "ln", description = "Returns the natural logarithm (base e) of a double value.")
public class LnSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public LnSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) Math.log(supplier.get());
    }
}
