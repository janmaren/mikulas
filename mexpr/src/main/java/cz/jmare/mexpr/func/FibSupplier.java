package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "fib", description = "Fibonacci number.")
public class FibSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    public FibSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) fibonacci((int) (double) supplier.get());
    }

    public static long fibonacci(int n) {
        if (n <= 1) return n;
        else
            return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
