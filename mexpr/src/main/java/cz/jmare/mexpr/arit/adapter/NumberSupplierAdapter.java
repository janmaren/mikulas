package cz.jmare.mexpr.arit.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import cz.jmare.mexpr.type.DoubleHolder;

public class NumberSupplierAdapter extends DoubleHolder {
    private List<Consumer<Double>> listeners;

    public NumberSupplierAdapter(double value, List<Consumer<Double>> listeners) {
        super(value);
        this.listeners = listeners;
    }

    public NumberSupplierAdapter(List<Consumer<Double>> listeners) {
        super(0);
        this.listeners = listeners;
    }

    public NumberSupplierAdapter(Consumer<Double> consumer) {
        super(0);
        this.listeners = new ArrayList<>();
        this.listeners.add(consumer);
    }

    public NumberSupplierAdapter(Consumer<Double> consumer1, Consumer<Double> consumer2) {
        super(0);
        this.listeners = new ArrayList<>();
        this.listeners.add(consumer1);
        this.listeners.add(consumer2);
    }

    public NumberSupplierAdapter(DoubleHolder numberHolder, Consumer<Double> consumer) {
        super(0);
        this.listeners = new ArrayList<>();
        this.listeners.add(numberHolder);
        this.listeners.add(consumer);
    }

    @Override
    public void accept(Double value) {
        super.accept(value);
        for (Consumer<Double> consumer : listeners) {
            consumer.accept(value);
        }
    }
}
