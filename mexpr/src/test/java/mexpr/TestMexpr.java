package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.function.Function;

import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.semantic.IncompatibleOperandException;
import cz.jmare.mexpr.exception.semantic.OverridenConstantException;
import cz.jmare.mexpr.exception.semantic.UndefinedFunctionException;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.type.DoubleHolder;

public class TestMexpr {
    @Test
    public void simpleUnitTest() {
        ExpressionParser expressionParser = new ExpressionParser("100k * 25u + 1000m", new Config().withDefaultMetricPrefixes(true));
        double result = expressionParser.evaluateDouble();
        assertEquals(3.5d, result, 0.00001);
    }

    @Test
    public void simpleVarTest() {
        ExpressionParser expressionParser = new ExpressionParser("100 * x + 25", Map.of("x", new DoubleHolder(3)));
        double result = expressionParser.evaluateDouble();
        assertEquals(325d, result, 0.00001);
    }

    @Test
    public void divideByZeroTest() {
        ExpressionParser expressionParser = new ExpressionParser("100 / 0");
        double result = expressionParser.evaluateDouble();
        assertEquals("Not equals", Double.POSITIVE_INFINITY, result, 0.000001);
    }

    @Test
    public void variableTest() {
        ExpressionParser expressionParser = new ExpressionParser("x", Map.of("x", new DoubleHolder(3)));
        double result = expressionParser.evaluateDouble();
        assertEquals("Variable value not fit", 3.0, result, 0.001);
    }

    @Test
    public void variableEfficientTest() {
        DoubleHolder variableConsumer = new DoubleHolder();
        ExpressionParser expressionParser = new ExpressionParser("x * e", Map.of("x", variableConsumer));

        variableConsumer.accept(2.0);
        double result = expressionParser.evaluateDouble();
        assertTrue(result > 5 && result < 6);

        variableConsumer.accept(10.0);
        result = expressionParser.evaluateDouble();
        assertTrue(result > 27 && result < 28);
    }

    @Test
    public void simpleConstantTest() {
        ExpressionParser expressionParser = new ExpressionParser("pi * 2");
        double result = expressionParser.evaluateDouble();
        assertTrue(result > 6 && result < 7);
    }

    @Test
    public void varAndConstantTest() {
        ExpressionParser expressionParser = new ExpressionParser("pi * 2 + x", Map.of("x", new DoubleHolder(0.1)));
        double result = expressionParser.evaluateDouble();
        assertTrue(result > 6 && result < 7);
    }

    @Test
    public void sumTest() {
        ExpressionParser expressionParser = new ExpressionParser("summ(2, x, 1, 1000)");
        double result = expressionParser.evaluateDouble();
        assertEquals(2000, result, 0.1);
    }

    @Test
    public void scientificTest() {
        ExpressionParser expressionParser = new ExpressionParser("1.2e3");
        double value = expressionParser.evaluateDouble();
        assertEquals(1.2e3, value, 0.0001);
    }

    @Test
    public void passNumberTest() {
        ExpressionParser expressionParser = new ExpressionParser("25 + a", Map.of("a", 10));
        double value = expressionParser.evaluateDouble();
        assertEquals(35, value, 0.0001);
    }

    @Test
    public void passFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("f(10)", Map.of("f(x)", "x * x"));
        double value = expressionParser.evaluateDouble();
        assertEquals(100, value, 0.0001);
    }

    @Test
    public void stringConstantTest() {
        ExpressionParser expressionParser = new ExpressionParser("10 + e", Map.of("e", "20"), new Config().withPredefinedClasses(false));
        double value = expressionParser.evaluateDouble();
        assertEquals(30, value, 0.0001);
    }

    @Test(expected = UndefinedVariableException.class)
    public void notUsePredefConstantTest() {
        new ExpressionParser("10 + e", new Config().withPredefinedClasses(false));
    }

    @Test(expected = UndefinedFunctionException.class)
    public void notUsePredefFuncTest() {
        new ExpressionParser("sin(0.5)", new Config().withPredefinedClasses(false));
    }

    @Test
    public void overrideConstantTest() {
        boolean occured = false;
        try {
            new ExpressionParser("10 + e", Map.of("e", "20"), new Config().withOverrideConstant(false));
        } catch (OverridenConstantException e) {
            occured = true;
        }
        assertTrue(occured);
    }

    @Test
    public void metricForVariableTest() {
        ExpressionParser expressionParser = new ExpressionParser("10k + a", Map.of("a", "20k"), new Config().withDefaultMetricPrefixes(true));
        double value = expressionParser.evaluateDouble();
        assertEquals(30000, value, 0.0001);
    }

    @Test(expected = UndefinedVariableException.class)
    public void solveVariableTest() {
        new ExpressionParser("a + b", Map.of("a", "20"));
    }

    @Test
    public void changeVarTest() {
        DoubleHolder variable = new DoubleHolder();
        ExpressionParser expressionParser = new ExpressionParser("a + 10", Map.of("a", variable));
        variable.accept(20.0);
        assertEquals(30, expressionParser.evaluateDouble(), 0.01);
    }

    @Test(expected = Exception.class)
    public void invalidUseFunc1Test() {
        new ExpressionParser("f(10) + 10", Map.of("f(x)", "c * x"));
    }

    @Test
    public void varInFuncFromOutsideTest() {
        new ExpressionParser("f(11)", Map.of("f(x)", "x + a", "a", 20));
    }

    @Test
    public void overlapVarTest() {
        ExpressionParser expressionParser = new ExpressionParser("f(10, 20)", Map.of("f(x, a)", "x + a", "a", 30));
        assertEquals(30, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void funcInFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("f(r)", Map.of("f(x)", "x * x + g(x)", "g(x)", "x - 1", "r", 10.0));
        assertEquals(109.0, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void validFuncTest() {
        new ExpressionParser("f(11, a)", Map.of("f(x, y)", "x + y", "a", 20));
    }

    @Test
    public void func2Test() {
        ExpressionParser expressionParser = new ExpressionParser("f(10) + f(20, 30)", Map.of("f(x)", "x * x", "f(x, y)", "x + y", "r", 10.0));
        assertEquals(150.0, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void funcInterfTest() {
        ExpressionParser expressionParser = new ExpressionParser("apply(10) + apply(20)", Map.of("apply(x)", new Function<Double, Double>() {
            @Override
            public Double apply(Double val) {
                return 2 * val;
            }
        }));
        assertEquals(60, expressionParser.evaluateDouble(), 0.01);
    }

    @Test(expected = IncompatibleOperandException.class)
    public void funcInterfLambdaNotSupportedTest() {
        new ExpressionParser("apply(10) + apply(20)", Map.of("apply(x)", (Function<Double, Double>) val -> 2 * val));
    }

    @Test
    public void funcStaticTest() {
        ExpressionParser expressionParser = new ExpressionParser("twice(10)", Map.of("twice(x)", Util1.class));
        assertEquals(20, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void funcStaticVarargTest() {
        ExpressionParser expressionParser = new ExpressionParser("twice2(10, 30, 40)", Map.of("twice2(x, y, z)", Util1.class));
        assertEquals(160, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void funcStaticVararg3Test() {
        ExpressionParser expressionParser = new ExpressionParser("twice3(10, 30, 40)", Map.of("twice3(x, y, z)", Util1.class));
        assertEquals(160, expressionParser.evaluateDouble(), 0.01);
    }

    @Test(expected = UndefinedFunctionException.class)
    public void overcyclingFuncTest() {
        new ExpressionParser("f(10)", Map.of("f(x)", "x + f(x)"));
    }

    @Test
    public void negationTermTest() {
        ExpressionParser expressionParser = new ExpressionParser("-5 ^ 2");
        assertEquals(-25.0, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negationTerm2Test() {
        ExpressionParser expressionParser = new ExpressionParser("5 ^ (-2)");
        assertEquals(0.04d, expressionParser.evaluateDouble(), 0.01);
    }


    @Test
    public void negationTerm3Test() {
        ExpressionParser expressionParser = new ExpressionParser("-5 - (-3)");
        assertEquals(-2d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negationTerm4Test() {
        ExpressionParser expressionParser = new ExpressionParser("5 ^ (-2) + 3");
        assertEquals(3.04d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negationTerm5Test() {
        ExpressionParser expressionParser = new ExpressionParser("(10 / 3) ^ 1 * 3");
        assertEquals(10d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void orderOfMultiplicantsTest() {
        ExpressionParser expressionParser = new ExpressionParser("10 / 3 * 3");
        assertEquals(10d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negateFirstMemberSimpleTest() {
        ExpressionParser expressionParser = new ExpressionParser("-10");
        assertEquals(-10d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negateFirstMemberMultiplyTest() {
        ExpressionParser expressionParser = new ExpressionParser("-10 / 3 * 3");
        assertEquals(-10d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negateFirstMemberPowerTest() {
        ExpressionParser expressionParser = new ExpressionParser("-10 ^ 2");
        assertEquals(-100d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negateFirstMemberPowerPositiveTest() {
        ExpressionParser expressionParser = new ExpressionParser("+10 ^ 2");
        assertEquals(100d, expressionParser.evaluateDouble(), 0.01);
    }

    @Test
    public void negatePowerTest() {
        ExpressionParser expressionParser = new ExpressionParser("10 ^ (-2)");
        assertEquals(0.01d, expressionParser.evaluateDouble(), 0.0001);
    }

    @Test
    public void multiplyNegationsTest() {
        ExpressionParser expressionParser = new ExpressionParser("-1 * (-1)");
        assertEquals(1d, expressionParser.evaluateDouble(), 0.0001);
    }
}
