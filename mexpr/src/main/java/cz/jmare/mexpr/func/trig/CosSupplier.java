package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "cos", description = "Returns the trigonometric cosine of an angle.")
public class CosSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CosSupplier(@Desc("An angle in radians") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.cos(supplier.get());
    }
}
