package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "summ", description = "Summation. Example: sum(1 / (n ^ 2 - 1), n, 1, N).")
public class SummationSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;
    private DoubleHolder variableSupplier;
    private Supplier<Double> from;
    private Supplier<Double> to;

    public SummationSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("from") Supplier<Double> from, @Desc("to") Supplier<Double> to) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.from = from;
        this.to = to;
    }

    @Override
    public Double get() {
        long fromValue = Math.round(from.get());
        long toValue = Math.round(to.get());
        double sum = 0;
        boolean isNondefined = false;

        for (long i = fromValue; i <= toValue; i++) {
            variableSupplier.accept((double) i);
            Double double1 = supplier.get();
            if (double1 == null) {
                isNondefined = true;
                break;
            }
            sum += double1;
        }

        if (isNondefined) {
            return Double.NaN;
        }
        return sum;
    }
}
