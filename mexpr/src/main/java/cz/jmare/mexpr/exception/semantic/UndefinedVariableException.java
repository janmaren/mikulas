package cz.jmare.mexpr.exception.semantic;

import cz.jmare.mexpr.exception.SemanticException;
import cz.jmare.mexpr.parse.Token;

public class UndefinedVariableException extends SemanticException {
    private static final long serialVersionUID = 2692829249787167517L;

    private String variableName;

    public UndefinedVariableException(String variableName) {
        super("Variable '" + variableName + "' doesn't exist");
        this.variableName = variableName;
    }

    public UndefinedVariableException(Token token) {
        this(token.getString());
        setPosition(token.getPosition());
    }

    public String getVariableName() {
        return variableName;
    }
}
