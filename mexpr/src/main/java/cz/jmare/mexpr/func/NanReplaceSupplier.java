package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "nanrep", description = "Replace NaN value with other value. Example: nanrep(f(x), 10) or nanrep(f(x), g(x))")
public class NanReplaceSupplier implements Supplier<Double> {
    private Supplier<Double> mainSupplier;
    private Supplier<Double> replacementSupplier;


    public NanReplaceSupplier(@Desc("value") Supplier<Double> mainSupplier, @Desc("replacementExpression") Supplier<Double> replacementSupplier) {
        this.mainSupplier = mainSupplier;
        this.replacementSupplier = replacementSupplier;
    }

    @Override
    public Double get() {
        Double value = mainSupplier.get();
        if (value.isNaN()) {
            return replacementSupplier.get();
        }
        return value;
    }
}
