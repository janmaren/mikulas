package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1Holder;
import cz.jmare.mexpr.type.VectorD1Impl;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.semantic.FunctionExistsArgsNotMatchException;
import cz.jmare.mexpr.exception.semantic.UndefinedFunctionException;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.VectorD2;

public class FuncTest {
    @Test
    public void simpleFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("f(10) + f(a ^ 2)", Map.of("a", new DoubleHolder(2), "f(x)", "x ^ 2"));
        double result = expressionParser.evaluateDouble();
        assertEquals(116, result, 0.1);
    }

    @Test
    public void simpleOverrideFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("sin(10) + sin(a ^ 2)", Map.of("a", new DoubleHolder(2), "sin(x)", "x ^ 2"));
        double result = expressionParser.evaluateDouble();
        assertEquals(116, result, 0.1);
    }

    @Test
    public void simpleDifferentiateFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("diff(f(x), x, y)", Map.of("y", new DoubleHolder(Math.PI / 2), "f(x)", "cos(x)"));
        double result = expressionParser.evaluateDouble();
        assertEquals(-1, result, 0.1);
    }

    @Test
    public void simpleDifferentiateTest() {
        ExpressionParser expressionParser = new ExpressionParser("diff(cos(x), x, y)", Map.of("y", new DoubleHolder(Math.PI / 2)));
        double result = expressionParser.evaluateDouble();
        assertEquals(-1, result, 0.1);
    }

    @Test
    public void sqrtDifferentiateFuncTest() {
        ExpressionParser expressionParser = new ExpressionParser("diff(f(x), x, x)", Map.of("x", new DoubleHolder(0.0000001), "f(x)", "sqrt(x)"));
        double result = expressionParser.evaluateDouble();
        assertEquals(1581.15, result, 0.1);
    }


    @Test
    public void multiArgVariantsIntTest() {
        String expression = "int(x ^ 2, x, 2, 4)";
        ExpressionParser expressionParser = new ExpressionParser(expression);
        double evaluate = expressionParser.evaluateDouble();
        assertEquals(18.66, evaluate, 0.01);
    }

    @Test(expected = FunctionExistsArgsNotMatchException.class)
    public void multiArgVariantsInt2Test() {
        String expression = "int(x + y, x, 2, 4)";
        new ExpressionParser(expression);
    }

    @Test(expected = UndefinedVariableException.class)
    public void sinXUnsetVarTest() {
        new ExpressionParser("sin(x)");
    }

    @Test
    public void unknownFuncTest() {
        try {
            new ExpressionParser("unknown(x)", Map.of("x", 1));
            assertFalse(true);
        } catch (UndefinedFunctionException e) {
            assertEquals("unknown", e.getFunctionName());
        }
    }

    @Test
    public void strTest() {
        String expression = "\"ahoj\"";
        ExpressionParser expressionParser = new ExpressionParser(expression);
        assertEquals("ahoj", expressionParser.getResultSupplier().get());
    }

    @Test
    public void strEmptyTest() {
        String expression = "\"\"";
        ExpressionParser expressionParser = new ExpressionParser(expression);
        assertEquals("", expressionParser.getResultSupplier().get());
    }

    @Test
    public void echoStrTest() {
        String expression = "echo(\"test output to stdout\")";
        ExpressionParser expressionParser = new ExpressionParser(expression);
        assertEquals("test output to stdout", expressionParser.getResultSupplier().get());
    }

    @Test
    public void echoD2Test() {
        String expression = "echo(d2(d1(1, 2), d1(3, 4)))";
        ExpressionParser expressionParser = new ExpressionParser(expression);
        assertTrue(expressionParser.evaluate() instanceof VectorD2);
    }

    @Test
    public void foreachFailTest() {
        String expression = "foreachToResults(x + 1, x, xx)";
        try {
            new ExpressionParser(expression, Map.of("xx", new VectorD1Holder(new VectorD1Impl(new double[]{1, 2, 3}))), new Config().withPredefinedClasses(false));
        } catch (UndefinedFunctionException e) {
            assertEquals("foreachToResults", e.getFunctionName());
            return;
        }
        fail("Not throwed UndefinedFunctionException");
    }
}
