package cz.jmare.mexpr.func.calc;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "lowest", description = "Use input values as X value and return the X which gives the lowest Y")
public class LowestSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;
    private DoubleHolder variableSupplier;
    private Supplier<VectorD1> inputD1Supplier;

    public LowestSupplier(@Desc("expression") @UseInnerVariable  Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder variableSupplier, @Desc("Input values") Supplier<VectorD1> inputD1Supplier) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.inputD1Supplier = inputD1Supplier;
    }

    @Override
    public Double get() {
        double lowestY = Double.NaN;
        double lowestX = Double.NaN;
        VectorD1 vectorD1 = inputD1Supplier.get();
        int size = vectorD1.size();
        for (int i = 0; i < size; i++) {
            double x = vectorD1.get(i);
            variableSupplier.accept(x);
            Double y = supplier.get();
            if (Double.isNaN(lowestY) || y < lowestY) {
                lowestY = y;
                lowestX = x;
            }
        }
        return lowestX;
    }
}
