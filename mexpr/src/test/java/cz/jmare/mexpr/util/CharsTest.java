package cz.jmare.mexpr.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CharsTest {

    @Test
    public void basicTest() {
        String wholeString = "this is a whole string";
        Chars chars = new Chars(wholeString, 5, 4);
        assertEquals(4, chars.length());
        assertEquals('i', chars.charAt(0));
        assertEquals('s', chars.charAt(1));
        assertEquals(' ', chars.charAt(2));
        assertEquals('a', chars.charAt(3));

        assertEquals("is a", chars.toString());

        assertEquals("s ", chars.subSequence(1, 3).toString());
        assertEquals("s a", chars.subSequence(1, 4).toString());
        assertEquals("is a", chars.subSequence(0, 4).toString());

        Chars chars1 = new Chars(chars.getWholeString(), 6, 1);
        assertEquals("s", chars1.toString());
        assertEquals(wholeString, chars.getWholeString());
    }
}
