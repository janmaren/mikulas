package cz.jmare.mexpr.type;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Collection of points
 */
public class VectorPoints {
    private List<Point> points;

    public VectorPoints(List<Point> points) {
        this.points = points;
    }

    public Point get(int index) {
        return points.get(index);
    }

    public int size() {
        return points.size();
    }

    @Override
    public String toString() {
        return points.stream().map(d -> d.toString()).collect(Collectors.joining(", ", "points(", ")"));
    }
}
