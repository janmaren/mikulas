package cz.jmare.mexpr.type;

/**
 * Point in 3D with x, y and z
 */
public class Spatial {
    public double x;

    public double y;

    public double z;

    public Spatial(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "spatial(" + x + ", " + y + ", " + z + ")";
    }
}
