package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "floor", description = "Returns the largest (closest to positive infinity) double value that is less than or equal to the argument and is equal to a mathematical integer.")
public class FloorSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public FloorSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) Math.floor(supplier.get());
    }
}
