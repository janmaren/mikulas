package cz.jmare.mexpr.util.classutil;

public class TypeHelper {
    public static boolean isIdentifier(String str) {
        char ch = str.charAt(0);
        return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || ch == '_';
    }

    public static boolean isNumber(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isAlpha(String name) {
        char[] chars = name.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isVar(String name) {
        if (name == null || name.length() == 0) {
            return false;
        }
        char[] chars = name.toCharArray();
        if (!Character.isLetter(chars[0])) {
            return false;
        }
        for (char c : chars) {
            if (!Character.isLetter(c) && !Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }
}
