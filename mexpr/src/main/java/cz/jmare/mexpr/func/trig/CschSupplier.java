package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "csch", description = "Returns the hyperbolic cosecant of an angle.")
public class CschSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CschSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return 1.0 / Math.sinh(supplier.get());
    }
}
