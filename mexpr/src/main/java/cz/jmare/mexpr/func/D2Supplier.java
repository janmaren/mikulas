package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.type.VectorD2Impl;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "d2", description = "Factory for VectorD2 type. VectorD2 is one dimensinal array of VectorD1 which is in fact a two dimensional array of doubles.")
public class D2Supplier implements Supplier<VectorD2> {
    private Supplier<VectorD1> supplier;
    private Supplier<VectorD1>[] suppliers;

    @SuppressWarnings("unchecked")
    public D2Supplier() {
        this.supplier = null;
        this.suppliers = new Supplier[0];
    }

    @SafeVarargs
    public D2Supplier(Supplier<VectorD1> supplier, Supplier<VectorD1>... suppliers) {
        this.supplier = supplier;
        this.suppliers = suppliers;
    }

    @SuppressWarnings("unchecked")
    public D2Supplier(Supplier<VectorD1> supplier) {
        this.supplier = supplier;
        this.suppliers = new Supplier[0];
    }

    @Override
    public VectorD2 get() {
        if (supplier == null) {
            return new VectorD2Impl(new VectorD1[0]);
        }
        VectorD1[] values = new VectorD1[suppliers.length + 1];
        values[0] = supplier.get();
        for (int i = 0; i < suppliers.length; i++) {
            values[i + 1] = suppliers[i].get();
        }
        return new VectorD2Impl(values);
    }
}
