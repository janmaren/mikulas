package cz.jmare.mexpr.func.vector;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "count", description = "Returns count of items of vector.")
public class CountD1Supplier implements Supplier<Double> {
    private Supplier<VectorD1> supplier;

    public CountD1Supplier(Supplier<VectorD1> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) supplier.get().size();
    }
}
