package cz.jmare.mexpr.exception.semantic;

import cz.jmare.mexpr.exception.SemanticException;

public class FunctionExistsArgsNotMatchException extends SemanticException {
	private static final long serialVersionUID = -588650334535813722L;
	private String functionName;

    public FunctionExistsArgsNotMatchException(String functionName) {
        super("Function " + functionName + " exists but arguments do not match");
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }
}
