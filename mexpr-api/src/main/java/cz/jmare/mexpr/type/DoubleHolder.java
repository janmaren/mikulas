package cz.jmare.mexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Supplier of Double. Besides providing a double value the value may be also changed.
 */
public class DoubleHolder implements Supplier<Double>, Consumer<Double> {
    private double value;

    public DoubleHolder(double value) {
        this.value = value;
    }

    public DoubleHolder() {
    }

    @Override
    public Double get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void accept(Double value) {
        this.value = value;
    }
}
