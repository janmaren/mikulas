package cz.jmare.mexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Holder of 3D data
 */
public class VectorD3Holder implements Supplier<VectorD3>, Consumer<VectorD3> {
    private VectorD3 value;

    public VectorD3Holder(VectorD3 value) {
        this.value = value;
    }

    public VectorD3Holder() {
    }

    @Override
    public VectorD3 get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void accept(VectorD3 value) {
        this.value = value;
    }
}
