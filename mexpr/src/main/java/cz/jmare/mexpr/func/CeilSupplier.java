package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "ceil", description = "the smallest (closest to negative infinity) floating-point value that is greater than or equal to the argument and is equal to a mathematical integer.")
public class CeilSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public CeilSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) Math.ceil(supplier.get());
    }
}
