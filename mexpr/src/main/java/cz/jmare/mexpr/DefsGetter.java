package cz.jmare.mexpr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import cz.jmare.mexpr.arit.MethodFunction;
import cz.jmare.mexpr.exception.semantic.OverridenConstantException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.CharsUtil;
import cz.jmare.mexpr.util.DefinedFunction;
import cz.jmare.mexpr.util.type.ObjectHolder;

public class DefsGetter {
	private Map<String, Object> defsMap;

	private Config config;

    DefsGetter(Map<String, Object> defsMap, Config config) {
        this.defsMap = defsMap;
		this.config = config;
    }

    public Supplier<?> getVariableSupplier(String variableName) {
        Map<String, Supplier<Double>> constants = config.getConstants();
        for (Entry<String, Object> entry : defsMap.entrySet()) {
            String keyStr = entry.getKey();
            if (keyStr.contains("(")) {
            	continue;
            }
            if (!keyStr.equals(variableName)) {
            	continue;
            }
            if (!config.isOverrideConstant() && constants.containsKey(keyStr)) {
                throw new OverridenConstantException(keyStr);
            }
            Object value = entry.getValue();
            if (value instanceof Supplier<?>) {
                return (Supplier<?>) value;
            } else if (value instanceof Number) {
                Number number = (Number) value;
                return new DoubleHolder(number.doubleValue());
            } else if (value instanceof CharSequence) {
                Map<String, Object> innerMap = new HashMap<>(defsMap);
                innerMap.remove(keyStr);
                ExpressionParser expressionParser = new ExpressionParser((CharSequence) value, innerMap, config);
                Map<String, Object> defsSubMap = expressionParser.getDefsMap();
                defsMap.putAll(defsSubMap);
                Supplier<?> resultSupplier = expressionParser.getResultSupplier();
                defsMap.put(keyStr, resultSupplier);
                return resultSupplier;
            } else {
            	@SuppressWarnings({ "rawtypes", "unchecked" })
				ObjectHolder objectHolder = new ObjectHolder(value);
            	Supplier<?> resultSupplier = (Supplier<?>) objectHolder;
            	defsMap.put(keyStr, resultSupplier);
            	return resultSupplier;
            }
        }
        
        return null;
    }
    
    public List<DefinedFunction> getExpressedFunction(String functionName) {
    	List<DefinedFunction> funs = new ArrayList<DefinedFunction>();
        for (Entry<String, Object> entry : defsMap.entrySet()) {
            String keyStr = entry.getKey();
            Object value = entry.getValue();
            if (!keyStr.contains("(") || !(value instanceof CharSequence)) {
            	continue;
            }
            String funcName = keyStr.substring(0, keyStr.indexOf("("));
            if (!funcName.equals(functionName)) {
            	continue;
            }
            DefinedFunction definedFunction = new DefinedFunction(keyStr, (CharSequence) value);
            funs.add(definedFunction);
        }
        return funs;
    }

	public List<MethodFunction> getFunctionsAsMethods(String functionName) {
		List<MethodFunction> funs = new ArrayList<MethodFunction>();
		for (Entry<String, Object> entry : defsMap.entrySet()) {
			String keyStr = CharsUtil.toString(entry.getKey());
			Object value = entry.getValue();
			if (!keyStr.contains("(")) {
				continue;
			}
			String funcName = keyStr.substring(0, keyStr.indexOf("("));
			if (!funcName.equals(functionName)) {
				continue;
			}
			MethodFunction instanceFunction = new MethodFunction(keyStr, value);
			funs.add(instanceFunction);
		}
		return funs;
	}

	public Map<String, Object> getDefsMap() {
		return defsMap;
	}
}
