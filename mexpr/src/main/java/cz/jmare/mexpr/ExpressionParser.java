package cz.jmare.mexpr;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import cz.jmare.mexpr.arit.ExponentSupplier;
import cz.jmare.mexpr.arit.MethodFunction;
import cz.jmare.mexpr.arit.MultiDivSupplier;
import cz.jmare.mexpr.arit.NegationSupplier;
import cz.jmare.mexpr.arit.SumSupplier;
import cz.jmare.mexpr.exception.run.EvaluateException;
import cz.jmare.mexpr.exception.semantic.FunctionExistsArgsNotMatchException;
import cz.jmare.mexpr.exception.semantic.IncompatibleOperandException;
import cz.jmare.mexpr.exception.semantic.UndefinedFunctionException;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.exception.syntax.IllegalTokenException;
import cz.jmare.mexpr.parse.FToken;
import cz.jmare.mexpr.parse.ObjectMethod;
import cz.jmare.mexpr.parse.Token;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.StringHolder;
import cz.jmare.mexpr.util.ArgsSplitter;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.InstanceFuncBindSupplier;
import cz.jmare.mexpr.util.UnknownTypeSupplier;
import cz.jmare.mexpr.util.UseInnerVariable;
import cz.jmare.mexpr.util.classutil.ClassUtils;
import cz.jmare.mexpr.util.classutil.FuncCreatorUtil;
import cz.jmare.mexpr.util.classutil.SupplierTypeUtil;
import cz.jmare.mexpr.util.classutil.TypeHelper;

/**
 * Main class to be used for parsing. It can parse expression, accept some variables and evaluate expression.<br>
 * At least an expression to parse must be passed. In this case the default configuration present in {@link Config}
 * is used. Also a defsMap may be passed to pass variables and expressed functions.<br>
 * <br>
 * defsMap:<br>
 * <li> variable: key as an identifier and value with an expression, but simplest use is variable and number, like Map.of("var1", 123) or variable and string, like Map.of("var2", "\"12:23:34\""). Since value is an expression it could be Map.of("var", "10 ^ 2 + 5") 
 * <li> variable: key as an identifier and value with an implementation of a Supplier&lt;Type&gt;. Such supplier is directly used without need to parse an expression. Example: DoubleHolder dh = new DoubleHolder(9.8); Map.of("g", dh);   
 * <li> function: key as an function signature and value with an expression. The key contains idenfier followed with parentheses and arguments inside. Example: Map.of("power(x)", "x * x") or using other function, like Map.of("pythag(x, y)", "sqrt(x ^ 2 + y ^ 2)"). Note that variables not present among arguments may be used too (global variables).   
 */
public class ExpressionParser {
    /**
     * Root result supplier
     */
    private Supplier<?> resultSupplier;

    /**
     * Parsed expression
     */
    private CharSequence expression;

    /**
     * Map where key is a name of function as written in expression and list of strings are canonical
     * class names of corresponding function. It means that for such functions a Java implementation exists
     */
    private Map<String, List<Class<?>>> classFunctions = new HashMap<>();

    /**
     * Pairs name of constant and value instance which is an Supplier returning double value
     */
    private Map<String, Supplier<Double>> classConstants = new HashMap<>();

    /**
     * Key is name of metric prefix (which is behind a number) and value is a multiplicator, e.g. 'k' is 1000 as kilo
     */
    private Map<String, Double> metricPrefixes = new HashMap<>();

    private Config config;

    private HashMap<String, Object> defsMap = new HashMap<>();
    
    /**
     * When true then no exception will be thrown when function or variable not found
     */
    private boolean dryRun;
    
    /**
     * Parse expression
     * @param expression mathematical expression to parse
     */
    public ExpressionParser(CharSequence expression) {
        this.expression = expression;
        build(new Config());
    }

    /**
     * Parse expression and use defsMap
     * @param expression mathematical expression to parse
     * @param defsMap the defsMap, see {@link ExpressionParser}
     */
    public ExpressionParser(CharSequence expression, Map<String, Object> defsMap) {
        this.expression = expression;
        initByDefsMap(defsMap, new Config());
        build(new Config());
    }

    /**
     * Parse expression and use defsMap and configuration
     * @param expression mathematical expression to parse
     * @param defsMap the defsMap, see {@link ExpressionParser}
     * @param config {@link Config} with configuration
     */
    public ExpressionParser(CharSequence expression, Map<String, Object> defsMap, Config config) {
        this.expression = expression;
        initByDefsMap(defsMap, config);
        build(config);
    }

    /**
     * Parse expression and use a configuration 
     * @param expression expression
     * @param config {@link Config} with configuration
     **/
    public ExpressionParser(CharSequence expression, Config config) {
        this.expression = expression;
        build(config);
    }

    private void initByDefsMap(Map<String, Object> defsMap, Config config) {
        this.defsMap.putAll(defsMap);
    }

    /**
     * Method to pickup the numeric (double) result of the expression evaluation
     * @return
     */
    @SuppressWarnings("unchecked")
    public double evaluateDouble() {
        if (!SupplierTypeUtil.supplierOfType(resultSupplier, Double.class)) {
            if (resultSupplier instanceof UnknownTypeSupplier) {
                throw new EvaluateException("Unable to evaluate expression - unresoved variables exist");
            }
            throw new EvaluateException("Expression " + expression + " is not a double expression");
        }
        return ((Supplier<Double>) resultSupplier).get();
    }

    /**
     * Method to pickup the result of the expression evaluation
     * @return
     */
    public Object evaluate() {
        return resultSupplier.get();
    }
    
    /**
     * Get result type after parsing before evaluation<br>
     * It's the same like <pre>evaluate().getClass()</pre> but this is done without evaluation and that's why it's quicker<br>
     * Usefull to detect whether user entered the right expression for which the expected result will be returned
     * @return result type which would be a class of evaluated object
     */
    public Class<?> getResultType() {
        return SupplierTypeUtil.getGenericTypeOfSupplier(resultSupplier);
    }

    /**
     * Get root supplier.
     * @return
     */
    public Supplier<?> getResultSupplier() {
        return resultSupplier;
    }

    private void build(Config config) {
        this.config = config;

        classFunctions = config.getFuncs();

        classConstants = config.getConstants();

        metricPrefixes = config.getMetricPrefixes();

        Token token = new FToken(expression);
        if (!token.hasNext()) {
            throw new IllegalTokenException("Empty", 0, 0);
        }
        token = token.next();
        ParseResult parseResult = parseExpr(token);
        if (parseResult.lastConsumedToken.hasNext()) {
            throw new IllegalTokenException(parseResult.lastConsumedToken.next());
        }
        resultSupplier = parseResult.resultSupplier;
    }

    @SuppressWarnings("unchecked")
    private ParseResult parseExpr(Token token) {
        String firstString = token.getString();
        boolean negateFirstMember = false;
        if ("+".equals(firstString) || "-".equals(firstString)) {
            if ("-".equals(firstString)) {
                negateFirstMember = true;
            }
            token = token.next();
        }

        ParseResult termResult = parseTerm(token);
        token = termResult.lastConsumedToken;

        if (token.hasNext()) {
            ArrayList<Integer> operators = new ArrayList<Integer>();
            ArrayList<Supplier<?>> suppliers = new ArrayList<Supplier<?>>();
            while (token.hasNext()) {
                Token operatorToken = token.next();
                if ("+".equals(operatorToken.getString()) || "-".equals(operatorToken.getString())) {
                    Integer operator = null;
                    if ("+".equals(operatorToken.getString())) {
                        operator = SumSupplier.OPERATOR_PLUS;
                    } else if ("-".equals(operatorToken.getString())) {
                        operator = SumSupplier.OPERATOR_MINUS;
                    } else {
                        throw new IllegalTokenException(operatorToken);
                    }
                    ParseResult termResultNext = parseTerm(operatorToken.next());
                    suppliers.add(termResultNext.resultSupplier);
                    operators.add(operator);
                    token = termResultNext.lastConsumedToken;
                } else {
                    break;
                }
            }

            if (operators.size() > 0) {
                if (SupplierTypeUtil.allSuppliersOfType(termResult.resultSupplier, suppliers, Double.class)) {
                    int[] operatorsArray = operators.stream().mapToInt(i -> i).toArray();
                    return new ParseResult(token, new SumSupplier((Supplier<Double>) termResult.resultSupplier, toDoubleSupArray(suppliers), operatorsArray, negateFirstMember));
                } else {
                    // TODO: revise following
                    if (SupplierTypeUtil.existsUnknownTypeSupplier(termResult.resultSupplier, suppliers)) {
                        return new ParseResult(token, new UnknownTypeSupplier());
                    }
                    List<Supplier<?>> allSuppliers = new ArrayList<>();
                    allSuppliers.add(termResult.resultSupplier);
                    allSuppliers.addAll(suppliers);
                    String collect = allSuppliers.stream().map(s -> SupplierTypeUtil.getGenericTypeOfSupplier(s).getSimpleName()).collect(Collectors.joining(", ", "Unsupported combination of operands ", " for '+' and '-' operators"));
                    throw new IncompatibleOperandException(collect);
                }
            } else {
                // for example members of functions...
                if (negateFirstMember) {
                    return new ParseResult(token, new NegationSupplier((Supplier<Double>) termResult.resultSupplier));
                }
                return new ParseResult(token, termResult.resultSupplier); // should be same as termResult
            }
        } else {
            if (negateFirstMember) {
                return new ParseResult(token, new NegationSupplier((Supplier<Double>) termResult.resultSupplier));
            }
            return new ParseResult(token, termResult.resultSupplier);
        }
    }

    @SuppressWarnings("unchecked")
    private ParseResult parseTerm(Token token) {
        ParseResult factorResult = parseFactor(token);
        token = factorResult.lastConsumedToken;

        if (token.hasNext()) {
            ArrayList<Integer> operators = new ArrayList<Integer>();
            ArrayList<Supplier<?>> suppliers = new ArrayList<Supplier<?>>();
            while (token.hasNext()) {
                Token operatorToken = token.next();
                if ("*".equals(operatorToken.getString()) || "/".equals(operatorToken.getString())) {
                    Integer operator = null;
                    if ("*".equals(operatorToken.getString())) {
                        operator = MultiDivSupplier.OPERATOR_MULTIPLY;
                    } else if ("/".equals(operatorToken.getString())) {
                        operator = MultiDivSupplier.OPERATOR_DIVIDE;
                    } else {
                        throw new IllegalTokenException(operatorToken);
                    }
                    ParseResult termResultNext = parseFactor(operatorToken.next());
                    suppliers.add(termResultNext.resultSupplier);
                    operators.add(operator);
                    token = termResultNext.lastConsumedToken;
                } else {
                    break;
                }
            }

            if (operators.size() > 0) {
                if (SupplierTypeUtil.allSuppliersOfType(factorResult.resultSupplier, suppliers, Double.class)) {
                    int[] operatorsArray = operators.stream().mapToInt(i -> i).toArray();
                    return new ParseResult(token,
                            new MultiDivSupplier((Supplier<Double>) factorResult.resultSupplier, toDoubleSupArray(suppliers), operatorsArray));
                } else {
                    if (SupplierTypeUtil.existsUnknownTypeSupplier(factorResult.resultSupplier, suppliers)) {
                        return new ParseResult(token, new UnknownTypeSupplier());
                    }
                    List<Supplier<?>> allSuppliers = new ArrayList<>();
                    allSuppliers.add(factorResult.resultSupplier);
                    allSuppliers.addAll(suppliers);
                    String collect = allSuppliers.stream().map(s -> SupplierTypeUtil.getGenericTypeOfSupplier(s).getSimpleName()).collect(Collectors.joining(", ", "Unsupported combination of operands ", " for '*' and '/' operators"));
                    throw new IncompatibleOperandException(collect);
                }
             } else {
                return new ParseResult(token, factorResult.resultSupplier);
            }
        } else {
            return new ParseResult(token, factorResult.resultSupplier);
        }
    }

    @SuppressWarnings("unchecked")
    private ParseResult parseFactor(Token token) {
        ParseResult baseResult = parseBase(token);
        token = baseResult.lastConsumedToken;

        if (token.hasNext()) {
            ArrayList<Supplier<?>> suppliers = new ArrayList<Supplier<?>>();
            if (token.hasNext()) {
                Token operatorToken = token.next();
                if ("^".equals(operatorToken.getString())) {
                    ParseResult termResultNext = parseBase(operatorToken.next());
                    suppliers.add(termResultNext.resultSupplier);
                    token = termResultNext.lastConsumedToken;
                }
            }

            if (suppliers.size() > 0) {
                if (SupplierTypeUtil.allSuppliersOfType(baseResult.resultSupplier, suppliers, Double.class)) {
                    return new ParseResult(token, new ExponentSupplier((Supplier<Double>) baseResult.resultSupplier, toDoubleSupArray(suppliers)));
                } else {
                    // TODO: review following
                    if (SupplierTypeUtil.existsUnknownTypeSupplier(baseResult.resultSupplier, suppliers)) {
                        return new ParseResult(token, new UnknownTypeSupplier());
                    }
                    List<Supplier<?>> allSuppliers = new ArrayList<>();
                    allSuppliers.add(baseResult.resultSupplier);
                    allSuppliers.addAll(suppliers);
                    String collect = allSuppliers.stream().map(s -> SupplierTypeUtil.getGenericTypeOfSupplier(s).getSimpleName()).collect(Collectors.joining(", ", "Unsupported combination of operands ", " for '^' operator"));
                    throw new IncompatibleOperandException(collect);
                }
            } else {
                return new ParseResult(token, baseResult.resultSupplier);
            }
        } else {
            return new ParseResult(token, baseResult.resultSupplier);
        }
    }

    private ParseResult parseBase(Token token) {
        if (FToken.isText(token.getString())) {
            String quotedStr = token.getString();
            String unquotedStr = quotedStr.substring(1, quotedStr.length() - 1);
            StringHolder stringHolder = new StringHolder(unquotedStr);
            return new ParseResult(token, stringHolder);
        } else if (TypeHelper.isIdentifier(token.getString())) {
            if (token.hasNext() && token.next().getString().equals("(")) { // function
                String functionName = token.getString();
                Token funcToken = token;
                Token nextToken = token.next();
                token = nextToken.next();
                ParseResult parseFunction = parseFunction(functionName, token, funcToken);
                return parseFunction;
            } else { // variable
                String variableName = token.getString();
                Supplier<Double> constantSupplier = classConstants.get(variableName);
                DefsGetter defsGetter = new DefsGetter(defsMap, config);
                Supplier<?> variableSupplier = defsGetter.getVariableSupplier(variableName);
                if (variableSupplier != null) {
                    return new ParseResult(token, variableSupplier);
                } else if (constantSupplier != null) {
                    return new ParseResult(token, constantSupplier);
                } else {
                    variableSupplier = new UnknownTypeSupplier();
                    if (dryRun) {
                        return new ParseResult(token, variableSupplier);
                    } else {
                        throw new UndefinedVariableException(token);
                    }
                }
            }
        } else if ("(".equals(token.getString())) {
            token = token.next();
            ParseResult parseExpr = parseExpr(token);
            token = parseExpr.lastConsumedToken;
            token = token.next();
            if (!")".equals(token.getString())) {
                throw new IllegalTokenException(token);
            }
            return new ParseResult(token, parseExpr.resultSupplier);
        } else if (TypeHelper.isNumber(token.getString())) {
            String number = token.getString();
            double multiplier = 1;
            if (token.hasNext()) {
                Token possibleUnit = token.next();
                String possibleUnitValue = possibleUnit.getString();
                if (TypeHelper.isAlpha(possibleUnitValue)) {
                    Double unitMultiplier = metricPrefixes.get(possibleUnitValue);
                    if (unitMultiplier != null) {
                        multiplier = unitMultiplier;
                        token = possibleUnit;
                    } else {
                        throw new IllegalTokenException(possibleUnit);
                    }
                }
            }
            DoubleHolder constantSupplier = new DoubleHolder(Double.valueOf(number) * multiplier);
            return new ParseResult(token, constantSupplier);
        } else {
            throw new IllegalTokenException(token);
        }
    }

    /**
     * Parse arguments of functions and populate suppliers
     * @param token
     * @return new token behind functional arguments
     */
    private Token doFunctionSuppliers(Token token, List<Supplier<?>> resArgSuppliers) {
        int pos = 0;
        while (!")".equals(token.getString())) {
            if (pos > 0) {
                if (!",".equals(token.getString())) {
                    throw new IllegalTokenException(token);
                }
                token = token.next();
            }
            ParseResult parseExpr = parseExpr(token);
            resArgSuppliers.add(parseExpr.resultSupplier);
            token = parseExpr.lastConsumedToken.next();
            pos++;
        }
        return token;
    }

    static class ParseResult {
        /**
         * Last token which corresponds with resultSupplier. The token.next() must be usually called after this to process following token
         */
        private Token lastConsumedToken;

        private Supplier<?> resultSupplier;

        public ParseResult(Token lastConsumedToken, Supplier<?> resultSupplier) {
            super();
            this.lastConsumedToken = lastConsumedToken;
            this.resultSupplier = resultSupplier;
        }

        @Override
        public String toString() {
            return resultSupplier.toString();
        }
    }

    @SuppressWarnings({"unchecked" })
    private final static Supplier<Double>[] toDoubleSupArray(List<Supplier<?>> suppliers) {
        return suppliers.toArray(new Supplier[suppliers.size()]);
    }

    private final static Object[] toArrayNoGen(List<?> suppliers) {
        return suppliers.toArray(new Supplier[suppliers.size()]);
    }

    Map<String, Object> getDefsMap() {
        return defsMap;
    }

    private ParseResult parseFunction(String functionName, Token token, Token funcToken) {
        List<Supplier<?>> argumentSuppliers = new ArrayList<Supplier<?>>();
        boolean origDryRun = dryRun;
        Token resultToken;
        try {
            dryRun = true;
            resultToken = doFunctionSuppliers(token, argumentSuppliers);
        } finally {
            dryRun = origDryRun;
        }

        if (!SupplierTypeUtil.existsUnknownTypeSupplier(argumentSuppliers)) {
            DefsGetter defsGetter = new DefsGetter(defsMap, config);

            // try expressed function
            Supplier<?> functionSupplier = FuncCreatorUtil.doFindExpressedFunctionSupplier(functionName, argumentSuppliers, defsGetter, config);
            if (functionSupplier != null) {
                return new ParseResult(resultToken, functionSupplier);
            }

            // try instanced function
            List<MethodFunction> functions = defsGetter.getFunctionsAsMethods(functionName);
            if (functions != null) {
                ObjectMethod objectMethod = FuncCreatorUtil.findMethod(functions, argumentSuppliers);
                if (objectMethod != null) {
                    functionSupplier = new InstanceFuncBindSupplier(objectMethod.callObject, objectMethod.callMethod, argumentSuppliers);
                    return new ParseResult(resultToken, functionSupplier);
                }
            }
        }

        // try classpath function which can process also UnknownType when it's inner variable of function
        for (Entry<String, List<Class<?>>> entry : classFunctions.entrySet()) {
            String funcName = entry.getKey();
            List<Class<?>> functionClasses = entry.getValue();
            if (!funcName.equals(functionName)) {
                continue;
            }
            for (Class<?> classDefinition : functionClasses) {
                Constructor<?>[] constructors = classDefinition.getConstructors();
                for (Constructor<?> constructor : constructors) {
                    Supplier<?> resultSupplier = supplierForConstructor(constructor, token, functionName);
                    if (resultSupplier != null) {
                        return new ParseResult(resultToken, resultSupplier);
                    }
                }
            }
        }
        if (!classFunctions.containsKey(functionName)) {
            if (dryRun) {
                return new ParseResult(resultToken, new UnknownTypeSupplier());
            }
            UndefinedFunctionException undefinedFunctionException = new UndefinedFunctionException(functionName);
            undefinedFunctionException.setPosition(funcToken.getPosition());
            throw undefinedFunctionException;
        }
        if (SupplierTypeUtil.existsUnknownTypeSupplier(argumentSuppliers)) {
            if (isInnerVarConstructor(functionName)) {
                throw new FunctionExistsArgsNotMatchException(functionName);
            }
            // let it throw appropriate exception for unresolved argument than not found function
            doFunctionSuppliers(token, argumentSuppliers);
        }
        if (dryRun) {
            return new ParseResult(resultToken, new UnknownTypeSupplier());
        } else {
            UndefinedFunctionException undefinedFunctionException = new UndefinedFunctionException(functionName, FuncCreatorUtil.getSuppliersSimpleTypes(argumentSuppliers));
            undefinedFunctionException.setPosition(funcToken.getPosition());
            throw undefinedFunctionException;
        }
    }
    
    private boolean isInnerVarConstructor(String functionName) {
        for (Entry<String, List<Class<?>>> entry : classFunctions.entrySet()) {
            String funcName = entry.getKey();
            List<Class<?>> functionClasses = entry.getValue();
            if (!funcName.equals(functionName)) {
                continue;
            }
            for (Class<?> classDefinition : functionClasses) {
                Constructor<?>[] constructors = classDefinition.getConstructors();
                for (Constructor<?> constructor : constructors) {
                    Parameter[] parameters = constructor.getParameters();
                    for (Parameter parameter : parameters) {
                        if (parameter.getAnnotationsByType(InnerVariable.class).length > 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    private Supplier<?> supplierForConstructor(Constructor<?> constructor, Token tokenArg, String functionName) {
        Parameter[] parameters = constructor.getParameters();
        Map<Integer, Supplier<?>> innerSuppliers = new HashMap<Integer, Supplier<?>>();
        Map<String, Integer> innerVarIndex = new HashMap<>();

        String expressionFromPos = ((FToken)tokenArg).getExpressionFromPos();
        List<String> argumentsTokens = ArgsSplitter.split(functionName, expressionFromPos);
        for (int pos = 0; pos < argumentsTokens.size(); pos++) {
            String token = argumentsTokens.get(pos);
            if (parameters.length > pos) {
                Parameter parameter = parameters[pos];
                if (parameter.getAnnotationsByType(InnerVariable.class).length > 0) {
                    if (!TypeHelper.isVar(token)) {
                        return null;
                    }
                    Class<?> parameterSupplierGenType = parameter.getType();
                    try {
                        Object varInstance = parameterSupplierGenType.getDeclaredConstructor().newInstance();
                        innerSuppliers.put(pos, (Supplier<?>) varInstance);
                        innerVarIndex.put(token, pos);
                    } catch (Exception e) {
                        return null;
                    }
                }
            } else {
                break;
            }
        }

        HashMap<String, Object> origDefsMap = new HashMap<>(defsMap);
        defsMap = origDefsMap;
        List<Supplier<?>> argumentSuppliers = new ArrayList<>();
        Token token = tokenArg;
        int pos = 0;
        while (!")".equals(token.getString())) {
            if (pos > 0) {
                if (!",".equals(token.getString())) {
                    throw new IllegalTokenException(token);
                }
                token = token.next();
            }
            Parameter parameter;
            if (parameters.length > pos) {
                parameter = parameters[pos];
            } else {
                if (parameters.length == 0) {
                    return null;
                }
                parameter = parameters[parameters.length - 1];
                if (!parameter.isVarArgs()) {
                    return null;
                }
            }

            if (parameter.getAnnotationsByType(UseInnerVariable.class).length > 0) {
                Map<String, Object> defsMapBackup = new HashMap<String, Object>();
                Set<String> removing = new HashSet<>();
                for (Entry<String, Integer> entry : innerVarIndex.entrySet()) {
                    String variableName = entry.getKey();
                    Integer index = entry.getValue();
                    Supplier<?> supplier = innerSuppliers.get(index);
                    Object object = defsMap.get(variableName);
                    if (object != null) {
                        defsMapBackup.put(variableName, object);
                    } else {
                        removing.add(variableName);
                    }
                    defsMap.put(variableName, supplier);
                }
                ParseResult parseExpr;
                try {
                    parseExpr = parseExpr(token);
                    argumentSuppliers.add(parseExpr.resultSupplier);
                    token = parseExpr.lastConsumedToken.next();
                } catch (Exception e) {
                    return null;
                } finally {
                    for (String variableName : removing) {
                        defsMap.remove(variableName);
                    }
                    for (Entry<String, Object> entry : defsMapBackup.entrySet()) {
                        String variableName = entry.getKey();
                        Object value = entry.getValue();
                        defsMap.put(variableName, value);
                    }
                }
            } else {
                Supplier<?> supplier;
                if (innerSuppliers.containsKey(pos)) {
                    supplier = innerSuppliers.get(pos);
                    argumentSuppliers.add(supplier);
                    token = token.next();
                } else {
                    ParseResult parseExpr = parseExpr(token);
                    token = parseExpr.lastConsumedToken.next();
                    supplier = parseExpr.resultSupplier;
                    argumentSuppliers.add(supplier);
                }
                Class<?> usedFuncSupplier = SupplierTypeUtil.getGenericTypeOfSupplier(supplier);
                if (parameter.isVarArgs()) {
                    // parameter.getType().getComponentType().getGenericInterfaces() not works, unable to check parameter
                } else {
                    if (!innerSuppliers.containsKey(pos)) { // Inner variable supplier already precreated to match
                        Class<?> parameterSupplier = SupplierTypeUtil.getParameterSupplierGenType(parameter);
                        if (!ClassUtils.isAssignable(parameterSupplier, usedFuncSupplier)) {
                            return null;
                        }
                    }
                }
            }
            pos++;
        }

        Supplier<?> functionSupplier;
        Parameter lastPar = constructor.getParameters().length > 0 ? constructor.getParameters()[constructor.getParameters().length - 1] : null;
        if (lastPar != null && lastPar.isVarArgs()) {
            if (argumentSuppliers.size() < constructor.getParameterCount()) {
                return null;
            }
            Object[] args = new Object[constructor.getParameters().length];
            for (int i = 0; i < constructor.getParameters().length - 1; i++) {
                args[i] = argumentSuppliers.get(i);
            }
            Supplier<?>[] varargSuppliers = new Supplier[argumentSuppliers.size() - constructor.getParameters().length + 1];
            for (int i = 0; i < varargSuppliers.length; i++) {
                varargSuppliers[i] = argumentSuppliers.get(i + constructor.getParameters().length - 1);
            }
            args[constructor.getParameters().length - 1] = varargSuppliers;
            try {
                functionSupplier = (Supplier<?>) constructor.newInstance(args);
            } catch (Exception e) {
                return null;
            }
        } else {
                try {
                    functionSupplier = (Supplier<?>) constructor.newInstance(toArrayNoGen(argumentSuppliers));
                } catch (InstantiationException | IllegalAccessException
                         | InvocationTargetException e) {
                    throw new RuntimeException("Unable to create instance of " + constructor, e);
                } catch (IllegalArgumentException e) {
                    if (e.getMessage() != null && e.getMessage().contains("wrong number")) {
                        return null;
                    }
                    throw e;
                }
        }

        return functionSupplier;
    }
}
