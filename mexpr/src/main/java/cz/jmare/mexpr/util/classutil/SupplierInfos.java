package cz.jmare.mexpr.util.classutil;

import cz.jmare.mexpr.util.Consts;

import java.util.ArrayList;
import java.util.List;


public class SupplierInfos {

    public List<FunctionalSupplierClassInfo> funcInfos = new ArrayList<>();
    public List<ConstantSupplierClassInfo> consInfos = new ArrayList<>();

    public void removePredefined() {
        funcInfos.removeIf(i -> i.classpathClass
                        && i.supplierClass.getCanonicalName().startsWith(Consts.funcPackage));
        consInfos.removeIf(i -> i.classpathClass && i.supplierClass.getCanonicalName().startsWith(Consts.consPackage));
    }
}
