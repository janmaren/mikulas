package cz.jmare.mexpr.util;

import java.util.List;

import cz.jmare.mexpr.exception.syntax.UnexpectedEndOfCodeException;

public class ArgsSplitter {
    /**
     *
     * @param expression expression which points to beginning of first argument, e.g. "x + 1, (y + 3) * 5, y) * sin(x) should return {"x + 1", "(y + 3) * 5", "y"}
     * @return
     */
    public static List<String> split(String function, String expression) {
        ArgsConsumer argsConsumer = new ArgsConsumer(expression);
        PatternTraverser.traverse("\\(|\\)|\"|'|,", expression, argsConsumer);
        if (!argsConsumer.isFinished()) {
            throw new UnexpectedEndOfCodeException("Function '" + function + "(" + expression + "' is cut before all arguments parsed", 0, (function.length() + expression.length() + 1), (function.length() + expression.length() + 1));
        }
        List<String> args = argsConsumer.getArgs();
        return args;
    }

    public static void main(String[] args) {
        List<String> split = ArgsSplitter.split("func1", "x * 10, x, d1(\"a,b,c()))\", 2, 3, 4, 5), 1)");
        for (String string : split) {
            System.out.println(string);
        }
    }
}
