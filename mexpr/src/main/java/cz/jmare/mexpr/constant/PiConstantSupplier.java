package cz.jmare.mexpr.constant;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.ConstantSupplier;

@ConstantSupplier(name = "pi", description = "Ludolph's number pi")
public class PiConstantSupplier implements Supplier<Double> {
    @Override
    public Double get() {
        return Math.PI;
    }
}
