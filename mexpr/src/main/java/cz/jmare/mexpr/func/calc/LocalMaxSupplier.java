package cz.jmare.mexpr.func.calc;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.TrisectExtreme;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "localMax", description = "Return local maximum in given range or NaN when no such exists")
public class LocalMaxSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;
    private DoubleHolder variableSupplier;
    private Supplier<Double> fromSupplier;
    private Supplier<Double> toSupplier;

    public LocalMaxSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier,
            @Desc("Inner variable for the X to be searched in range") @InnerVariable DoubleHolder variableSupplier,
            @Desc("Range for X from") Supplier<Double> fromSupplier, @Desc("Range for X to") Supplier<Double> toSupplier) {
        this.supplier = supplier;
        this.variableSupplier = variableSupplier;
        this.fromSupplier = fromSupplier;
        this.toSupplier = toSupplier;
    }

    @Override
    public Double get() {
        Double value = TrisectExtreme.extremeOrNull(variableSupplier, supplier, fromSupplier.get(), toSupplier.get(), TrisectExtreme.HIGHEST_COMP);
        if (value == null) {
            return Double.NaN;
        }
        return value;
    }
}
