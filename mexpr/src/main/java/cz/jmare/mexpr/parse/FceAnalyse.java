package cz.jmare.mexpr.parse;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

public class FceAnalyse {
    /**
     * constructor when creating instance ({@link #method} is null
     */
    public Constructor<?> constructor;

    /**
     * passing method for existing instance of Supplier function, ({@link #constructor} is null
     */
    public Method method;

    public List<ParType> parTypes;

    public List<Class<?>> suppliersOfClass;

    public enum ParType {
        INNER_VAR, USE_INNER_VAR, NORMAL
    }
}
