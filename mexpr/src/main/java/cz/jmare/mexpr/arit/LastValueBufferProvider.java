package cz.jmare.mexpr.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;

public class LastValueBufferProvider  {

    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberHolder;
    private Double lastX;
    private double leftValue;
    private double rightValue;

    public LastValueBufferProvider(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberHolder) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
    }

    public void setXValue(double x) {
        if (x != lastX) {
            this.lastX = x;
            numberHolder.accept(lastX);
            leftValue = leftSupplier.get();
            rightValue = rightSupplier.get();
        }
    }

    public double getLeftValue() {
        return leftValue;
    }

    public double getRightValue() {
        return rightValue;
    }
}
