package mexpr;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;

public class FuncVarArgsTest {
    @Test
    public void avgTest() {
        ExpressionParser expressionParser = new ExpressionParser("avg(1, 2, 3, 4, 5)");
        assertEquals(3, expressionParser.evaluateDouble(), 0.1);
    }
}
