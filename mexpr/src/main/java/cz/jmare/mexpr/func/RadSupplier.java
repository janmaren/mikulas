package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "toRad", description = "Converts an angle measured in degrees to an approximately equivalent angle measured in radians.")
public class RadSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;

    public RadSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.toRadians(supplier.get());
    }
}
