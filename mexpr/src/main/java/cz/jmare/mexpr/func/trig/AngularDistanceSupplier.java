package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "angDist", description = "Returns angular distance between 2 angles measured in degrees. Result is range 0-180deg")
public class AngularDistanceSupplier implements Supplier<Double> {
    private Supplier<Double> angle1Supplier;

    private Supplier<Double> angle2Supplier;

    public AngularDistanceSupplier(@Desc("Angle 1") Supplier<Double> arg1, @Desc("Angle 2") Supplier<Double> arg2) {
        this.angle1Supplier = arg1;
        this.angle2Supplier = arg2;
    }

    @Override
    public Double get() {
        Double angle1 = angle1Supplier.get();
        Double angle2 = angle2Supplier.get();
        double dist = angle1 - angle2;
        dist %= 360.0;
        dist = (dist + 360.0) % 360.0;
        if (dist >= 180) {
            dist = 360.0 - dist;
        }
        return dist;
    }
}
