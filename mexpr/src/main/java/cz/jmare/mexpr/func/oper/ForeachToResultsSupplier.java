package cz.jmare.mexpr.func.oper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.Result;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorResults;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreachToResults", description = "Do an operation with each item of VectorD1 and store result with input value (for each x value return also y value). Example: foreachToResults(calcCoord(x), x, d1(2, 3, 4))")
public class ForeachToResultsSupplier implements Supplier<VectorResults> {
    private Supplier<Object> supplier;
    private DoubleHolder itemHolder;
    private Supplier<VectorD1> vectorD1Supplier;


    public ForeachToResultsSupplier(@Desc("expression producing a result of any type") @UseInnerVariable Supplier<Object> supplier, @Desc("inner variable for which the x value will be set sequentially") @InnerVariable DoubleHolder itemHolder, @Desc("the x values for which to calculate a result") Supplier<VectorD1> vectorD1Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.vectorD1Supplier = vectorD1Supplier;
    }

    @Override
    public VectorResults get() {
        Thread currentThread = Thread.currentThread();
        VectorD1 vectorD1 = this.vectorD1Supplier.get();
        int size = vectorD1.size();
        List<Result> resultList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            double x = vectorD1.get(i);
            itemHolder.accept(x);
            Object y = supplier.get();
            resultList.add(new Result(x, y));
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("foreachToResults interrupted");
            }
        }
        return new VectorResults(resultList);
    }
}
