package cz.jmare.mexpr.exception;

public class SyntaxException extends ParseExpressionException {

    private static final long serialVersionUID = 6126398480356350606L;

    private int offset;

    private int length;

    public SyntaxException(String message, int offset, int length) {
        super(message);
        this.offset = offset;
        this.length = length;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }
}
