package cz.jmare.mexpr.exception;

/**
 * For example when object passed as value in defsmap is not supported 
 */
public class InvalidDefException extends ParseExpressionException {
    private static final long serialVersionUID = 1335750149794430337L;
    
    public InvalidDefException(String message) {
        super(message);
    }
}
