package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sqrt", description = "Returns the correctly rounded positive square root of a double value.")
public class SqrtSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public SqrtSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.sqrt(supplier.get());
    }
}
