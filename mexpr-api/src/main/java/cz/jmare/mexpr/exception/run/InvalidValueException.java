package cz.jmare.mexpr.exception.run;

/**
 * Exception for example when an invalid value passed into a function, input value
 * out of range or unparsable argument when a string passed
 */
public class InvalidValueException extends EvaluateException {
    private static final long serialVersionUID = -5117865467528660594L;

    public InvalidValueException(String message) {
        super(message);
    }
}
