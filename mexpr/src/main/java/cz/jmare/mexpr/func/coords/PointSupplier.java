package cz.jmare.mexpr.func.coords;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.Point;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "point", description = "Factory for the point type with x and y values")
public class PointSupplier implements Supplier<Point> {
	
	private Supplier<Double> xSuplier;
	
	private Supplier<Double> ySuplier;

	
	public PointSupplier(@Desc("The x value") Supplier<Double> xSuplier, @Desc("The y value") Supplier<Double> ySuplier) {
		super();
		this.xSuplier = xSuplier;
		this.ySuplier = ySuplier;
	}

	@Override
	public Point get() {
		return new Point(xSuplier.get(), ySuplier.get());
	}
}
