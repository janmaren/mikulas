package cz.jmare.mexpr.parse;

public enum ResultType {
    /**
     * Double value
     */
    SCALAR,
    /**
     * Collection of SCALAR
     */
    VECTOR_1D,
    /**
     * Collection of collections of SCALAR
     */
    VECTOR_2D,
    /**
     * Collection of collections of collections of SCALAR
     */
    VECTOR_3D
}
