package cz.jmare.mexpr.arit;

import java.util.Map;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.NotExpressionException;
import cz.jmare.mexpr.util.classutil.SupplierTypeUtil;

public class ExpressionSupplier implements Supplier<Double> {
    private Supplier<Double> rootSupplier;
    private DoubleHolder numberHolder;

    /** ??? **/
    public ExpressionSupplier(String expression) {
        this(expression, new DoubleHolder(), new Config());
    }

    public ExpressionSupplier(String expression, DoubleHolder variableSupplier) {
        this(expression, variableSupplier, new Config());
    }

    @SuppressWarnings("unchecked")
    public ExpressionSupplier(String expression, DoubleHolder numberHolder, Config config) {
        ExpressionParser expressionParser;
        try {
            expressionParser = new ExpressionParser(expression, config);
        } catch (UndefinedVariableException e) {
            String variableName = e.getVariableName();
            expressionParser = new ExpressionParser(expression, Map.of(variableName, numberHolder), config);
        }
        if (!SupplierTypeUtil.supplierOfType(expressionParser.getResultSupplier(), Double.class)) {
            throw new NotExpressionException("Expression " + expression + " is not a double expression");
        }
        rootSupplier = (Supplier<Double>) expressionParser.getResultSupplier();
        this.numberHolder = numberHolder;
    }

    @Override
    public Double get() {
        return rootSupplier.get();
    }

    public DoubleHolder getNumberSupplier() {
        return numberHolder;
    }
}
