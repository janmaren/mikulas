package cz.jmare.mexpr.func;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "roundpl", description = "Round to n decimal places using nearest neighbor.")
public class RoundPlacesSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;
    private Supplier<Double> places;


    public RoundPlacesSupplier(@Desc("value") Supplier<Double> supplier, @Desc("places") Supplier<Double> places) {
        this.supplier = supplier;
        this.places = places;
    }

    @Override
    public Double get() {
        BigDecimal bd = new BigDecimal(supplier.get());
        bd = bd.setScale((int) (double) places.get(), RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
