package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sech", description = "Returns the hyperbolic secant of an angle.")
public class SechSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public SechSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return 1.0 / Math.cosh(supplier.get());
    }
}
