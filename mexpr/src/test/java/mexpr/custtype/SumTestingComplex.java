package mexpr.custtype;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sum", description = "sum custom types")
public class SumTestingComplex implements Supplier<TestingComplex> {
	private Supplier<TestingComplex> supplier1;
	private Supplier<TestingComplex> supplier2;
	
	public SumTestingComplex(Supplier<TestingComplex> supplier1, Supplier<TestingComplex> supplier2) {
		super();
		this.supplier1 = supplier1;
		this.supplier2 = supplier2;
	}

	@Override
	public TestingComplex get() {
		TestingComplex testingComplex = new TestingComplex(0, 0);
		testingComplex.real = supplier1.get().real + supplier2.get().real;
		testingComplex.complex = supplier1.get().complex + supplier2.get().complex;
		return testingComplex;
	}
}
