package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "pow", description = "Returns the value of the first argument raised to the power of 2")
public class PowToTwoSupplier implements Supplier<Double> {

    private Supplier<Double> base;

    public PowToTwoSupplier(Supplier<Double> base) {
        this.base = base;
    }

    @Override
    public Double get() {
        return Math.pow(base.get(), 2);
    }
}
