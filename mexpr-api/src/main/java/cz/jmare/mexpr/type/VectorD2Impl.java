package cz.jmare.mexpr.type;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Type for 2D
 */
public class VectorD2Impl implements VectorD2 {
    private VectorD1[] values;

    public VectorD2Impl(VectorD1[] values) {
        this.values = values;
    }

    @Override
    public VectorD1 get(int index) {
        return values[index];
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public String toString() {
        return Arrays.asList(values).stream().map(d1 -> d1.toString()).collect(Collectors.joining(", ", "d2(", ")"));
    }
}
