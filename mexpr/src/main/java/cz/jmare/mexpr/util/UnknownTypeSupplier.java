package cz.jmare.mexpr.util;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.type.UnknownType;

public class UnknownTypeSupplier implements Supplier<UnknownType> {
    @Override
    public UnknownType get() {
        throw new IllegalStateException("Not resolved variable or function exists");
    }
}
