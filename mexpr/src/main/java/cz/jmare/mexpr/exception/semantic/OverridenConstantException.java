package cz.jmare.mexpr.exception.semantic;

import cz.jmare.mexpr.exception.SemanticException;

public class OverridenConstantException extends SemanticException {

    private static final long serialVersionUID = 5997014099364831063L;
    private String constantName;

    public OverridenConstantException(String constantName) {
        super("Constant " + constantName + " musn't be overriden");
        this.constantName = constantName;
    }

    public String getVariableName() {
        return constantName;
    }
}
