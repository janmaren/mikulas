package cz.jmare.mexpr.func.oper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Holder;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreachAgg", description = "Do an operation with each item of VectorD2 where result of processing each item is a double value. Result type is VectorD1. Example: foreachAgg(avg(x), x, d2(d1(1, 2), d1(3, 4)))")
public class ForeachAggD2Supplier implements Supplier<VectorD1> {
    private Supplier<Double> supplier;
    private VectorD1Holder itemHolder;
    private Supplier<VectorD2> VectorD2Supplier;


    public ForeachAggD2Supplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var")  @InnerVariable VectorD1Holder itemHolder, @Desc("values") Supplier<VectorD2> VectorD2Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.VectorD2Supplier = VectorD2Supplier;
    }

    @Override
    public VectorD1 get() {
        Thread currentThread = Thread.currentThread();
        VectorD2 VectorD2 = this.VectorD2Supplier.get();
        int size = VectorD2.size();
        List<Double> resultDoubles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            itemHolder.accept(VectorD2.get(i));
            resultDoubles.add(supplier.get());
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("foreachAgg interrupted");
            }
        }

        return new VectorD1Impl(resultDoubles.stream().mapToDouble(Double::doubleValue).toArray());
    }
}
