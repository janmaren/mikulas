package cz.jmare.mexpr.util;

import cz.jmare.mexpr.exception.SyntaxException;

public class StatementAwareParseException extends RuntimeException {
    private static final long serialVersionUID = -6644015299958483229L;
    private SyntaxException parseExpressionException;
    private int definitionIndex;
    private int outerOffset;

    public StatementAwareParseException(SyntaxException parseExpressionException, int statementIndex, int outerOffset) {
        super(parseExpressionException);
        this.parseExpressionException = parseExpressionException;
        this.definitionIndex = statementIndex;
        this.outerOffset = outerOffset;
    }

    public SyntaxException getParseExpressionException() {
        return parseExpressionException;
    }

    public int getDefinitionIndex() {
        return definitionIndex;
    }

    public int getOffset() {
        return parseExpressionException.getOffset() + outerOffset;
    }

    public int getLength() {
        return parseExpressionException.getLength();
    }

    @Override
    public String getMessage() {
        if (parseExpressionException.getMessage() != null) {
            return "Problems on line " + (definitionIndex + 1) + " - " + parseExpressionException.getMessage();
        }
        return super.getMessage();
    }
}
