package cz.jmare.mexpr.func.vector;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "count", description = "Returns count of items of vector.")
public class CountD2Supplier implements Supplier<Double> {
    private Supplier<VectorD2> supplier;

    public CountD2Supplier(Supplier<VectorD2> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return (double) supplier.get().size();
    }
}
