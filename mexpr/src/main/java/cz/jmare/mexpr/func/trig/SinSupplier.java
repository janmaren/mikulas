package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sin", description = "Returns the trigonometric sine of an angle.")
public class SinSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public SinSupplier(@Desc("An angle in radians") Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.sin(supplier.get());
    }
}
