package cz.jmare.mexpr.type;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Type for 3D
 */
public class VectorD3Impl implements VectorD3 {
    private VectorD2[] values;

    public VectorD3Impl(VectorD2[] values) {
        this.values = values;
    }

    @Override
    public VectorD2 get(int index) {
        return values[index];
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public String toString() {
        return Arrays.asList(values).stream().map(d2 -> d2.toString()).collect(Collectors.joining(", ", "d3(", ")"));
    }
}
