package cz.jmare.mexpr.util;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Supplier;

import cz.jmare.mexpr.arit.adapter.FunctionToSupplierAdapter;

/**
 * Calculate exact extreme between points. The points from and to should be surrounding such extreme but when not it can be detected.<br>
 * Extreme may be for both lowest or highest value
 */
public class TrisectExtreme {
    public static final Comparator<Double> LOWEST_COMP = new Comparator<Double>() {
        @Override
        public int compare(Double o1, Double o2) {
            if (o1 < o2) {
                return -1;
            }
            if (o2 < o1) {
                return 1;
            }
            return 0;
        }
    };
    
    public static final Comparator<Double> HIGHEST_COMP = new Comparator<Double>() {
        @Override
        public int compare(Double o1, Double o2) {
            if (o1 < o2) {
                return 1;
            }
            if (o2 < o1) {
                return -1;
            }
            return 0;
        }
    };

    public static Double extremeOrNull(Consumer<Double> variableSupplier, Supplier<Double> supplier, double from, double to, Comparator<Double> comp) {
        return extreme(variableSupplier, supplier, from, to, comp, 1e-9, true);
    }
    
    public static double extreme(Consumer<Double> variableSupplier, Supplier<Double> supplier, double from, double to, Comparator<Double> comp) {
        return extreme(variableSupplier, supplier, from, to, comp, 1e-9, false);
    }
    
    /**
     * Find local extreme
     * @param variableSupplier x variable to be set before evaluation
     * @param supplier supplier returning y value depending on last x value
     * @param from from value. It's excluding unless detectExtreme is false 
     * @param to to value. It's excluding unless detectExtreme is false
     * @param comp comparator depending whether searching highest or lowest value
     * @param precision the precision
     * @param detectSurrounding when true it will detect that between from and to is no extreme and for such case it'll return null
     * @return found value or null when between from and to is no extreme
     */
    public static Double extreme(Consumer<Double> variableSupplier, Supplier<Double> supplier, double from, double to, Comparator<Double> comp, double precision, boolean detectSurrounding) {
        if (from > to) {
            double tmp = from;
            from = to;
            to = tmp;
        }
        double x1 = from;
        double x4 = to;
        double div = (x4 - x1) / 3.0;
        double x2 = x1 + div;
        double x3 = x1 + 2 * div;
        while (x4 - x1 > precision) {
            variableSupplier.accept(x1);
            double y1 = supplier.get();
            variableSupplier.accept(x2);
            double y2 = supplier.get();
            variableSupplier.accept(x3);
            double y3 = supplier.get();
            variableSupplier.accept(x4);
            double y4 = supplier.get();
            double int1 = y1 + y2;
            double int2 = y2 + y3;
            double int3 = y3 + y4;
            if (comp.compare(int1, int2) < 0) {
                x4 = x2;
            } else {
                if (comp.compare(int2, int3) < 0) {
                    x1 = x2;
                    x4 = x3;
                } else {
                    x1 = x3;
                    x2 = x4;
                }
            }
            div = (x4 - x1) / 3.0;
            x2 = x1 + div;
            x3 = x1 + 2 * div;
        }
        
        if (detectSurrounding) {
            if (x1 < from + precision) {
                return null;
            }
            if (x4 > to - precision) {
                return null;
            }
        }
        
        return (x1 + x4) / 2;
    }
    
    public static void main(String[] args) {
        FunctionToSupplierAdapter adapter = new FunctionToSupplierAdapter(x -> Math.cos(x));
        double solve = extreme(adapter, adapter, Math.PI - 0.01, Math.PI + 0.4, LOWEST_COMP);
        System.out.println(solve);
        
        solve = extreme(adapter, adapter, Math.PI - 0.4, Math.PI + 0.01, LOWEST_COMP);
        System.out.println(solve);
        
        solve = extreme(adapter, adapter, Math.PI - 0.01, Math.PI + 0.01, LOWEST_COMP);
        System.out.println(solve);
        
        solve = extreme(adapter, adapter, 0 - 0.01, 0 + 0.5, HIGHEST_COMP);
        System.out.println(solve);
        
        Double solve2 = extremeOrNull(adapter, adapter, Math.PI + 0.0001, Math.PI + 0.01, LOWEST_COMP);
        System.out.println(solve2);
    }
}
