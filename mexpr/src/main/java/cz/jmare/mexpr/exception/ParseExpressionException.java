package cz.jmare.mexpr.exception;

import cz.jmare.mexpr.util.Chars;

public class ParseExpressionException extends RuntimeException {
    private static final long serialVersionUID = 3384670301522757614L;

    private Chars position;

    public ParseExpressionException(String message) {
        super(message);
    }

    public Chars getPosition() {
        return position;
    }

    public void setPosition(Chars position) {
        this.position = position;
    }
}
