package cz.jmare.mexpr.util.classutil;

import java.util.function.Supplier;

public class ConstantSupplierClassInfo implements Comparable<ConstantSupplierClassInfo> {
    public Class<?> supplierClass;

    public String constantName;

    public String constantDescription;

    public Supplier<Double> supplier; // TODO: change to Supplier<?>

    /**
     * True when such class is present in classpath (not in external jar file)
     */
    public boolean classpathClass;

    @Override
    public int compareTo(ConstantSupplierClassInfo o) {
        return constantName.compareTo(o.constantName);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(constantName);
        Class<?> clazz = SupplierTypeUtil.getGenericTypeOfSupplier(supplier);
        sb.append(": ").append(clazz.getSimpleName());
        sb.append(" - ").append(constantDescription);
        return sb.toString();
    }
}
