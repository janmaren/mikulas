package cz.jmare.mexpr.util;

import cz.jmare.mexpr.constant.PiConstantSupplier;
import cz.jmare.mexpr.func.AbsSupplier;

public class Consts {
    public final static String funcPackage = AbsSupplier.class.getPackageName() + ".";
    public final static String consPackage = PiConstantSupplier.class.getPackageName() + ".";

    public static String mexprPackage;

    static {
        int lastIndex = funcPackage.substring(0, funcPackage.length() - 1).lastIndexOf(".");
        mexprPackage = funcPackage.substring(0, lastIndex + 1);
    }
}
