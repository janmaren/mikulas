package org.exam.consta;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.ConstantSupplier;

@ConstantSupplier(name = "gravit", description = "Gravitational constant")
public class Gravit implements Supplier<Double> {
    @Override
    public Double get() {
        return 6.674E-11;
    }
}
