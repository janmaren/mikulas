package cz.jmare.mexpr.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuncDefinitionParser {
    public final static Pattern FCE_PATTERN = Pattern.compile("^\\s*(?<name>[a-zA-Z]?[a-zA-Z0-9]*)\\((?<args>[^\\)]+)\\)\\s*$");

    private final static String nonargChars = "+-!@#$%^&*()[]'\\/\":;";

    public String functionName;

    public List<String> arguments;

    public FuncDefinitionParser(String string) {
        Matcher matcher = FCE_PATTERN.matcher(string);
        if (matcher.find()) {
            functionName = matcher.group("name");
            String args = matcher.group("args");
            String[] split = args.split(",");
            arguments = new ArrayList<String>();
            for (String ar : split) {
                String argument = ar.trim();
                for (int i = 0; i < argument.length(); i++) {
                    char ch = argument.charAt(i);
                    if (Character.isWhitespace(ch)) {
                        throw new IllegalArgumentException("Invalid argument " + argument + " for function " + functionName);
                    }
                    String chStr = String.valueOf(ch);
                    if (nonargChars.contains(chStr)) {
                        throw new IllegalArgumentException("Invalid argument " + argument + " for function " + functionName);
                    }
                }
                arguments.add(argument);
            }
        } else {
            throw new IllegalArgumentException(string + " is not a function definition");
        }
    }

    public String getFunctionName() {
        return functionName;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public static void main(String[] args) {
        FuncDefinitionParser funcDefinitionParser = new FuncDefinitionParser("f(x,y)");
        System.out.println(funcDefinitionParser);
    }

    @Override
    public String toString() {
        return "FuncDefinitionParser [functionName=" + functionName + ", arguments=" + arguments + "]";
    }
}
