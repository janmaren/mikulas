package cz.jmare.mexpr.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;

public class ArgsConsumer implements Consumer<Matcher> {
    private List<String> args = new ArrayList<String>();

    private boolean inDoubleQuotes;

    private boolean inQuotes;

    private int parenths;

    int pos = 0;

    private String expression;

    public ArgsConsumer(String expression) {
        this.expression = expression;
    }

    @Override
    public void accept(Matcher matcher) {
        if (parenths < 0) {
            return;
        }
        if (matcher.group(0).equals("'")) {
            inQuotes = !inQuotes;
            return;
        }
        if (matcher.group(0).equals("\"")) {
            inDoubleQuotes = !inDoubleQuotes;
            return;
        }
        if (matcher.group(0).equals("(") && !inDoubleQuotes && !inQuotes) {
            parenths++;
            return;
        }
        if (matcher.group(0).equals(")") && !inDoubleQuotes && !inQuotes) {
            parenths--;
            if (parenths < 0) {
                int start = matcher.start();
                String arg = expression.substring(pos, start).trim();
                args.add(arg);
                pos = start;
            }
            return;
        }
        if (matcher.group(0).equals(",") && !inDoubleQuotes  && !inQuotes && parenths == 0) {
            int start = matcher.start();
            String arg = expression.substring(pos, start).trim();
            args.add(arg);
            pos = start + 1;
        }
    }

    public boolean isFinished() {
        return parenths == -1;
    }

    public List<String> getArgs() {
        return args;
    }
}
