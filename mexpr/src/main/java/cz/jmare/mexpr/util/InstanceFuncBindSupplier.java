package cz.jmare.mexpr.util;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Supplier;

public class InstanceFuncBindSupplier implements Supplier<Object> {
    private Object callObject;

    private Method callMethod;

    private List<Supplier<?>> argumentSuppliers;

    int varArgSize;

    Class<?> varArgType;

    int fromArgIndex;

    /**
     * Constructor
     * @param callObject object to be called or null for static call
     * @param callMethod method to be called
     * @param argumentSuppliers suppliers to be evaluated in this class and passed to called method
     */
    public InstanceFuncBindSupplier(Object callObject, Method callMethod, List<Supplier<?>> argumentSuppliers) {
        super();
        this.callObject = callObject;
        this.callMethod = callMethod;
        callMethod.setAccessible(true);
        this.argumentSuppliers = argumentSuppliers;
        if (callMethod.getParameterCount() > 0 && callMethod.getParameters()[callMethod.getParameterCount() - 1].isVarArgs()) {
            varArgSize = argumentSuppliers.size() - callMethod.getParameterCount() + 1;
            varArgType = callMethod.getParameters()[callMethod.getParameterCount() - 1].getType().getComponentType();
            fromArgIndex = argumentSuppliers.size() - varArgSize;
        }
    }

    @Override
    public Object get() {
        Object[] pars = null;
        if (varArgSize > 0) {
            Object varargs = Array.newInstance(varArgType, varArgSize);
            int vIndex = 0;
            for (int i = fromArgIndex; i < argumentSuppliers.size(); i++) {
                Array.set(varargs, vIndex++, argumentSuppliers.get(i).get());
            }
            pars = new Object[fromArgIndex + 1];
            pars[pars.length - 1] = varargs;
            int parIndex = 0;
            for (int i = 0; i < fromArgIndex; i++) {
                pars[parIndex++] = argumentSuppliers.get(i).get();
            }
        } else {
            pars = new Object[callMethod.getParameterCount()];
            for (int i = 0; i < pars.length; i++) {
                pars[i] = argumentSuppliers.get(i).get();
            }
        }
        try {
            return callMethod.invoke(callObject, pars);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Unable to call " + callMethod, e);
        }
    }

    public Class<?> getReturnClass() {
        return this.callMethod.getReturnType();
    }
}
