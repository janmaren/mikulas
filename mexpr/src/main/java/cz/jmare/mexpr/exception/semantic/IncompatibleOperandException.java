package cz.jmare.mexpr.exception.semantic;

import cz.jmare.mexpr.exception.SemanticException;

public class IncompatibleOperandException extends SemanticException {
    private static final long serialVersionUID = -4746828518089138718L;

    public IncompatibleOperandException(String message) {
        super(message);
    }
}
