package cz.jmare.mexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Holder of 2D data
 */
public class VectorD2Holder implements Supplier<VectorD2>, Consumer<VectorD2> {
    private VectorD2 value;

    public VectorD2Holder(VectorD2 value) {
        this.value = value;
    }

    public VectorD2Holder() {
    }

    @Override
    public VectorD2 get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void accept(VectorD2 value) {
        this.value = value;
    }
}
