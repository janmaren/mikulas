package cz.jmare.mexpr.func.coords;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.Spatial;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "spatial", description = "Factory for the spatial type with x, y and z values")
public class SpatialSupplier implements Supplier<Spatial> {
	
	private Supplier<Double> xSuplier;
	
	private Supplier<Double> ySuplier;

	private Supplier<Double> zSuplier;

	
	public SpatialSupplier(@Desc("The x value") Supplier<Double> xSuplier, @Desc("The y value") Supplier<Double> ySuplier, @Desc("The z value") Supplier<Double> zSuplier) {
		super();
		this.xSuplier = xSuplier;
		this.ySuplier = ySuplier;
		this.zSuplier = zSuplier;
	}

	@Override
	public Spatial get() {
		return new Spatial(xSuplier.get(), ySuplier.get(), zSuplier.get());
	}
}
