package cz.jmare.mexpr.exception.syntax;

import cz.jmare.mexpr.exception.SyntaxException;

public class UnexpectedEndOfCodeException extends SyntaxException {
    private static final long serialVersionUID = 2848818428445254815L;

    private int lastValidOffset;

    private int lastValidLength;

    public UnexpectedEndOfCodeException(int offset, int lastValidOffset, int lastValidLength) {
        super("Unexpected end - next character expected", offset, 0);
        this.lastValidOffset = lastValidOffset;
        this.lastValidLength = lastValidLength;
    }

    public UnexpectedEndOfCodeException(String message, int offset, int lastValidOffset, int lastValidLength) {
        super(message, offset, 0);
        this.lastValidOffset = lastValidOffset;
        this.lastValidLength = lastValidLength;
    }

    public int getLastValidOffset() {
        return lastValidOffset;
    }

    public int getLastValidLength() {
        return lastValidLength;
    }
}
