package mexpr.custtype;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;

public class CustomTypeTest {
	@Test
	public void customTest1() {
		ExpressionParser expressionParser = new ExpressionParser("sum(v1, v2)", Map.of("v1", new TestingComplex(1, 2), "v2", new TestingComplex(3, 4)));
		TestingComplex evaluate = (TestingComplex) expressionParser.evaluate();
		assertEquals(4, evaluate.real, 0.001);
		assertEquals(6, evaluate.complex, 0.001);
	}
}
