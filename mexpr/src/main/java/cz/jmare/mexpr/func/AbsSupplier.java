package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "abs", description = "Absolute number.")
public class AbsSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public AbsSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        return Math.abs(supplier.get());
    }
}
