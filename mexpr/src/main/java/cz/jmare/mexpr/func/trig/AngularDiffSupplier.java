package cz.jmare.mexpr.func.trig;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "angDiff", description = "Returns angular difference between 2 angles measured in degrees. The subtraction is angle2 minus angle1. Result is in range -180 to 180deg")
public class AngularDiffSupplier implements Supplier<Double> {
    private Supplier<Double> angle1Supplier;

    private Supplier<Double> angle2Supplier;

    public AngularDiffSupplier(@Desc("Angle 1") Supplier<Double> arg1, @Desc("Angle 2") Supplier<Double> arg2) {
        this.angle1Supplier = arg1;
        this.angle2Supplier = arg2;
    }

    @Override
    public Double get() {
        Double angle1 = angle1Supplier.get();
        Double angle2 = angle2Supplier.get();
        double dist = angle2 - angle1;
        return normalizeDegMinus180180(dist);
    }

    public static double normalizeDegMinus180180(double angleDeg) {
        angleDeg %= 360.0;
        angleDeg = (angleDeg + 360.0) % 360.0;
        if (angleDeg > 180.0) {
            angleDeg -= 360.0;
        }
        return angleDeg;
    }
}
