package cz.jmare.mexpr.constant;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.ConstantSupplier;

@ConstantSupplier(name = "e", description = "The Euler Number e")
public class EConstantSupplier implements Supplier<Double> {
    @Override
    public Double get() {
        return Math.E;
    }
}
