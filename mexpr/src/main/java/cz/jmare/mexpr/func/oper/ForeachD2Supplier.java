package cz.jmare.mexpr.func.oper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Holder;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.type.VectorD2Impl;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreach", description = "Do an operation with each item of VectorD2.")
public class ForeachD2Supplier implements Supplier<VectorD2> {
    private Supplier<VectorD1> supplier;
    private VectorD1Holder itemHolder;
    private Supplier<VectorD2> VectorD2Supplier;


    public ForeachD2Supplier(@Desc("expression") @UseInnerVariable Supplier<VectorD1> supplier, @Desc("var")  @InnerVariable VectorD1Holder itemHolder, @Desc("values") Supplier<VectorD2> VectorD2Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.VectorD2Supplier = VectorD2Supplier;
    }

    @Override
    public VectorD2 get() {
        Thread currentThread = Thread.currentThread();
        VectorD2 VectorD2 = this.VectorD2Supplier.get();
        int size = VectorD2.size();
        List<VectorD1> resultDoubles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            itemHolder.accept(VectorD2.get(i));
            resultDoubles.add(supplier.get());
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("foreach interrupted");
            }
        }

        return new VectorD2Impl(resultDoubles.toArray(new VectorD1[resultDoubles.size()]));
    }
}
