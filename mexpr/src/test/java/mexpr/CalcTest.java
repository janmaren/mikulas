package mexpr;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.VectorD1;

public class CalcTest {
    @Test
    public void lowestTest() {
        ExpressionParser expressionParser = new ExpressionParser("lowest(x * x, x, d1(-1, 2, 3))");
        double result = expressionParser.evaluateDouble();
        assertEquals(-1, result, 0.1);
    }

    @Test
    public void highestTest() {
        ExpressionParser expressionParser = new ExpressionParser("highest(x * x, x, d1(-1, 2, 3))");
        double result = expressionParser.evaluateDouble();
        assertEquals(3, result, 0.1);
    }

    @Test
    public void localMinsTest() {
        ExpressionParser expressionParser = new ExpressionParser("localMins(sin(x), x, 4, 6, 0.5)");
        assertEquals(VectorD1.class, expressionParser.getResultType());
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(1, evaluate.size());
        assertEquals(4.712388987745925, evaluate.get(0), 0.00001);
    }
    
    @Test
    public void localMaxsTest() {
        ExpressionParser expressionParser = new ExpressionParser("localMaxs(sin(x), x, 1, 4, 0.5)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(1, evaluate.size());
        assertEquals(1.5707963331044703, evaluate.get(0), 0.00001);
    }

    @Test
    public void genTest() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(1, 3, 0.5)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(5, evaluate.size(), 0.01);
    }

    @Test
    public void genNotPreciseTest() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(1, 3.4999999999999, 0.5)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(5, evaluate.size(), 0.01);
    }

    @Test
    public void genNotPrecise2Test() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(9, 9.2, 0.1)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(3, evaluate.size(), 0.01);
    }

    @Test
    public void genDecrTest() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(3, 1, 0.5)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(5, evaluate.size(), 0.01);
    }

    @Test
    public void genNotPreciseDecrTest() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(3.4999999999999, 1, 0.5)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(5, evaluate.size(), 0.01);
    }

    @Test
    public void genNotPrecise2DecrTest() {
        ExpressionParser expressionParser = new ExpressionParser("genSequence(9.2, 9, 0.1)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(3, evaluate.size(), 0.01);
    }

    @Test
    public void lowestGenTest() {
        ExpressionParser expressionParser = new ExpressionParser("lowest(x * x, x, genSequence(-5.05, 5, 0.1))");
        Double evaluate = (Double) expressionParser.evaluate();
        assertEquals(-0.04999, evaluate, 0.01);
    }

    @Test
    public void maxTest() {
        ExpressionParser expressionParser = new ExpressionParser("max(d1(-1, 2, 6))");
        Double evaluate = (Double) expressionParser.evaluate();
        assertEquals(6, evaluate, 0.01);
    }

    @Test
    public void minTest() {
        ExpressionParser expressionParser = new ExpressionParser("min(d1(-1, 2, 6))");
        Double evaluate = (Double) expressionParser.evaluate();
        assertEquals(-1, evaluate, 0.01);
    }

    @Test
    public void localMaxsMultipleTest() {
        ExpressionParser expressionParser = new ExpressionParser("localMaxs(cos(x), x, 0, 13, 2)");
        VectorD1 evaluate = (VectorD1) expressionParser.evaluate();
        assertEquals(3, evaluate.size());
        assertEquals(0, evaluate.get(0), 0.0000001);
        assertEquals(2 * Math.PI, evaluate.get(1), 0.0000001);
        assertEquals(4 * Math.PI, evaluate.get(2), 0.0000001);
    }
}
