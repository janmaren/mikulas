package cz.jmare.mexpr.func.arit;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "less", description = "Return 1 when value is less than value. Otherwise 0. When the including value populated as 1 it can be also equal.")
public class LessSupplier implements Supplier<Double> {
    private Supplier<Double> valueSupplier;
    private Supplier<Double> thresholdSupplier;
    private Supplier<Double> includingSupplier;

    public LessSupplier(@Desc("value") Supplier<Double> valueSupplier, @Desc("threshold") Supplier<Double> thresholdSupplier) {
        this.valueSupplier = valueSupplier;
        this.thresholdSupplier = thresholdSupplier;
    }

    public LessSupplier(@Desc("value") Supplier<Double> valueSupplier, @Desc("threshold") Supplier<Double> thresholdSupplier, @Desc("When passed 1 the second argument will be also including, otherwise excluding") Supplier<Double> includingSupplier) {
        this.valueSupplier = valueSupplier;
        this.thresholdSupplier = thresholdSupplier;
        this.includingSupplier = includingSupplier;
    }

    @Override
    public Double get() {
        double value = valueSupplier.get();
        double threshold = thresholdSupplier.get();
        boolean including = includingSupplier != null && includingSupplier.get() == 1.0;
        if (including) {
            if (value <= threshold) {
                return 1.0;
            } else {
                return 0.0;
            }
        }
        if (value < threshold) {
            return 1.0;
        } else {
            return 0.0;
        }
    }
}
