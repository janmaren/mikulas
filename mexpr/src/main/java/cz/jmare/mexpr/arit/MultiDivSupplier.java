package cz.jmare.mexpr.arit;

import java.util.function.Supplier;

/**
 * term -> factor [ ('*' | '/') factor ]*
 */
public class MultiDivSupplier implements Supplier<Double> {
    private Supplier<Double> initialFactor;

    private Supplier<Double>[] factors;

    private int[] operators;

    public final static int OPERATOR_MULTIPLY = 1;

    public final static int OPERATOR_DIVIDE = -1;


    public MultiDivSupplier(Supplier<Double> initialFactor, Supplier<Double>[] factors, int[] operators) {
        super();
        if (factors.length != operators.length) {
            throw new IllegalArgumentException("Not same length of summands and operators");
        }
        this.initialFactor = initialFactor;
        this.factors = factors;
        this.operators = operators;
    }

    @Override
    public Double get() {
        double sum = initialFactor.get();
        for (int i = 0; i < factors.length; i++) {
            double value = factors[i].get();
            int operator = operators[i];
            if (operator == OPERATOR_MULTIPLY) {
                sum = sum * value;
            } else if (operator == OPERATOR_DIVIDE) {
                sum = sum / value;
            } else {
                throw new IllegalStateException("Unknown (not factor) operator " + operator + " used");
            }
        }
        return sum;
    }
}
