package cz.jmare.mexpr.func.calc;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "min", description = "Return min value of a vector")
public class MinSupplier implements Supplier<Double> {
    private Supplier<VectorD1> supplier;

    public MinSupplier(@Desc("vector with values") Supplier<VectorD1> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        VectorD1 vectorD1 = supplier.get();
        int size = vectorD1.size();
        if (size == 0) {
            return Double.NaN;
        }
        double min = vectorD1.get(0);
        for (int i = 1; i < size; i++) {
            double item = vectorD1.get(i);
            if (item < min) {
                min = item;
            }
        }
        return min;
    }
}
