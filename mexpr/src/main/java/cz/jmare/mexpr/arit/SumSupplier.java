package cz.jmare.mexpr.arit;

import java.util.function.Supplier;

/**
 * expr -> term [ ('+' | '-') term ]*
 */
public class SumSupplier implements Supplier<Double> {
    private final boolean negateFirstMember;
    private final Supplier<Double> initialSummand;

    private final Supplier<Double>[] summands;

    private final int[] operators;

    public final static int OPERATOR_PLUS = 1;

    public final static int OPERATOR_MINUS = -1;


    public SumSupplier(Supplier<Double> initialSummand, Supplier<Double>[] summands, int[] operators, boolean negateFirstMember) {
        super();
        this.negateFirstMember = negateFirstMember;
        if (summands.length != operators.length) {
            throw new IllegalArgumentException("Not same length of summands and operators");
        }
        this.initialSummand = initialSummand;
        this.summands = summands;
        this.operators = operators;
    }

    @Override
    public Double get() {
        double sum = initialSummand.get();
        if (negateFirstMember) {
            sum = -sum;
        }
        for (int i = 0; i < summands.length; i++) {
            double value = summands[i].get();
            int operator = operators[i];
            if (operator == OPERATOR_PLUS) {
                sum += value;
            } else if (operator == OPERATOR_MINUS) {
                sum -= value;
            } else {
                throw new IllegalStateException("Unknown (not summand) operator " + operator + " used");
            }
        }

        return sum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(initialSummand.toString());
        for (int i = 0; i < operators.length; i++) {
            sb.append(operators[i] == 1 ? " + " : " - ");
            sb.append(summands[i]);
        }
        return sb.toString();
    }


}
