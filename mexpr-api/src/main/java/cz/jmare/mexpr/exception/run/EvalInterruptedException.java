package cz.jmare.mexpr.exception.run;

/**
 * Function can process an interrupt signal and in such case this exception is to be throwed
 */
public class EvalInterruptedException extends EvaluateException {
    private static final long serialVersionUID = 2107092567379765378L;

    public EvalInterruptedException(String message) {
        super(message);
    }
}
