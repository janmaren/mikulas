package cz.jmare.mexpr.type;

/**
 * Structure with input value 'x' and result value 'y' for given x. The 'y' may be another structure like some coordinates.<br>
 * In case of y:double it's better to use {@link Point}
 */
public class Result {
    public Double x;

    public Object y;
    
    public Result(Double x, Object y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "result(" + x + ", " + y + ")";
    }
}
