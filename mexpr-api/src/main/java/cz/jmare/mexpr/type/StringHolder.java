package cz.jmare.mexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Holder for string type with possibility to change the value
 */
public class StringHolder implements Supplier<String>, Consumer<String> {
    private String value;

    public StringHolder(String value) {
        this.value = value;
    }

    public StringHolder() {
    }

    @Override
    public String get() {
        return value;
    }

    @Override
    public String toString() {
        return "\"" + value + "\"";
    }

    @Override
    public void accept(String value) {
        this.value = value;
    }
}
