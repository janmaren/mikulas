package cz.jmare.mexpr.util;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.mexpr.exception.syntax.IllegalTokenException;
import cz.jmare.mexpr.exception.syntax.UnexpectedEndOfCodeException;

import static cz.jmare.mexpr.util.MExprStrUtil.findSeparatorIndex;

/**
 * Preparsed expression or equation
 */
public class LeftRight {
    public CharSequence left;
    public CharSequence right;
    private int statementIndex;
    private int rightOffset;
    private int rightLength;
    private boolean commented;

    private char separator = '=';

    public LeftRight(CharSequence statement, int statementIndex) {
        this.statementIndex = statementIndex;
        processLeftRight(statement);
    }

    public void processLeftRight(CharSequence statement) {
        if (CharsUtil.toString(statement).trim().startsWith("#")) {
            this.commented = true;
            return;
        }
        int indexOfComm = findSeparatorIndex(statement, '#');
        if (indexOfComm > 0) {
            statement = statement.subSequence(0, indexOfComm);
        }
        int indexOf = findSeparatorIndex(statement, separator);
        if (indexOf == -1) {
            left = statement;
            return;
        }
        left = statement.subSequence(0, indexOf);
        rightOffset = indexOf + 1;
        rightLength = statement.length() - rightOffset;
        right = statement.subSequence(rightOffset, statement.length());
        if (CharsUtil.toString(left).isBlank()) {
            IllegalTokenException illegalTokenException = new IllegalTokenException(String.valueOf(separator), 0, 1);
            if (statement instanceof Chars) {
                Chars chars = (Chars) statement;
                illegalTokenException.setPosition(new Chars(chars.getWholeString(), chars.getOffset() + indexOf, 1));
            }
            throw illegalTokenException;
        }
        if (CharsUtil.toString(right).isBlank()) {
            UnexpectedEndOfCodeException e = new UnexpectedEndOfCodeException(indexOf, indexOf - 1, 1);
            if (statement instanceof Chars) {
                Chars chars = (Chars) statement;
                e.setPosition(new Chars(chars.getWholeString(), chars.getOffset() + statement.length(), 0));
            }
            throw e;
        }
        int indexOf2 = findSeparatorIndex(right, separator, indexOf + 1);
        if (indexOf2 != -1) {
            IllegalTokenException illegalTokenException = new IllegalTokenException(String.valueOf(separator), indexOf2, 1);
            if (statement instanceof Chars) {
                Chars chars = (Chars) statement;
                illegalTokenException.setPosition(new Chars(chars.getWholeString(), chars.getOffset() + indexOf2, 1));
            }
            throw illegalTokenException;
        }
    }

    public boolean isExpression() {
        if (commented) {
            return false;
        }
        return right == null;
    }

    public boolean isDef() {
        if (commented) {
            return false;
        }
        if (right == null) {
            return false;
        }
        return true;
    }

    public boolean isVarDef() {
        if (commented) {
            return false;
        }
        if (right != null) {
            return false;
        }
        try {
            new FuncDefinitionParser(CharsUtil.toString(left));
            return false;
        } catch (IllegalArgumentException e) {
            return true;
        }
    }

    public String getVariableName() {
        if (right == null) {
            throw new IllegalStateException("No variable exists for " + toString());
        }
        return CharsUtil.toString(left).trim();
    }

    public CharSequence getExpressionDef() {
        return right;
    }

    public boolean isFuncDef() {
        if (commented) {
            return false;
        }
        if (right == null) {
            return false;
        }
        try {
            new FuncDefinitionParser(CharsUtil.toString(right));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isCommented() {
        return commented;
    }

    @Override
    public String toString() {
        if (right == null) {
            return CharsUtil.toString(left);
        }
        return CharsUtil.toString(left) + "=" + CharsUtil.toString(right);
    }

    public int getStatementIndex() {
        return statementIndex;
    }

    public int getRightOffset() {
        return rightOffset;
    }
    
    public static int numberCharactersBeforeIndex(String str, int before, char ch) {
        int num = 0;
        for (int i = 0; i < before; i++) {
            if (str.charAt(i) == ch) {
                num++;
            }
        }
        return num;
    }
    
    public static boolean between(String str, int pos, char betweenCharacter) {
        return numberCharactersBeforeIndex(str, pos, betweenCharacter) % 2 == 1;
    }
    
    public static List<LeftRight> convertToLeftRights(List<String> statements) {
        List<LeftRight> subEquations = new ArrayList<>();
        for (int i = 0; i < statements.size(); i++) {
            String statement = statements.get(i);
            try {
                LeftRight leftRight = new LeftRight(statement, i);
                subEquations.add(leftRight);
            } catch (IllegalTokenException e) {
                throw new StatementAwareParseException(e, i, 0);
            }
        }
        return subEquations;
    }

    public int getRightLength() {
        return rightLength;
    }
}
