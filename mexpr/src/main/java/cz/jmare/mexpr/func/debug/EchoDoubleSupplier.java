package cz.jmare.mexpr.func.debug;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "echo", description = "Output double to stdout and return as value.")
public class EchoDoubleSupplier implements Supplier<Double> {
    private Supplier<Double> argSup;

    public EchoDoubleSupplier(Supplier<Double> arg1) {
        this.argSup = arg1;
    }

    @Override
    public Double get() {
        Double double1 = argSup.get();
        System.out.println(double1);
        return double1;
    }
}
