package cz.jmare.mexpr.func.oper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.exception.run.EvalInterruptedException;
import cz.jmare.mexpr.exception.run.InvalidValueException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.Point;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorPoints;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreachToPoints", description = "Do an operation with each item of VectorD1 and store result with input value (for each x value return also y value). Example: foreachToPoints(x * 10, x, d1(2, 3, 4)) will return point(2, 20), point(3, 30), point(4, 40)")
public class ForeachToPointsSupplier implements Supplier<VectorPoints> {
    private Supplier<Double> supplier;
    private DoubleHolder itemHolder;
    private Supplier<VectorD1> vectorD1Supplier;


    public ForeachToPointsSupplier(@Desc("expression producing a result of double type") @UseInnerVariable Supplier<Double> supplier, @Desc("inner variable for which the x value will be set sequentially") @InnerVariable DoubleHolder itemHolder, @Desc("the x values for which to calculate a result") Supplier<VectorD1> vectorD1Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.vectorD1Supplier = vectorD1Supplier;
    }

    @Override
    public VectorPoints get() {
        Thread currentThread = Thread.currentThread();
        VectorD1 vectorD1 = this.vectorD1Supplier.get();
        int size = vectorD1.size();
        List<Point> resultList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            double x = vectorD1.get(i);
            itemHolder.accept(x);
            Object y = supplier.get();
            if (y instanceof Double) {
                resultList.add(new Point(x, (Double) y));
            } else {
                throw new InvalidValueException("Expected that expression will return a Double value but returned " + (y == null ? "null" : y.getClass()));
            }
            if (currentThread.isInterrupted()) {
                throw new EvalInterruptedException("foreachToPoints interrupted");
            }
        }
        return new VectorPoints(resultList);
    }
}
