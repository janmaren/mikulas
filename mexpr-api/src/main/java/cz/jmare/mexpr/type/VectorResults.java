package cz.jmare.mexpr.type;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Collection of results
 */
public class VectorResults {
    private List<Result> results;

    public VectorResults(List<Result> results) {
        this.results = results;
    }

    public Result get(int index) {
        return results.get(index);
    }

    public int size() {
        return results.size();
    }

    @Override
    public String toString() {
        return results.stream().map(d -> d.toString()).collect(Collectors.joining(", ", "results(", ")"));
    }
}
