package cz.jmare.mexpr.exception.run;

/**
 * Exceptions which can occure during evaluation, like invalid operand values
 */
public class EvaluateException extends RuntimeException {
	private static final long serialVersionUID = 938915458399969650L;

	public EvaluateException(String message) {
        super(message);
    }
}
