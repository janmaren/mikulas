package org.exam.func;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "square", description = "Returns the square")
public class Square implements Supplier<Double> {
    private Supplier<Double> supplier;

    public Square(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        Double x = supplier.get();
        return x * x;
    }
}
